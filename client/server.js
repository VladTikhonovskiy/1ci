const express = require("express");
const compression = require("compression");

const app = express();

app.use(compression());
app.use(express.static("build"));

app.get("*", (request, response) => {
    response.sendFile("./build/index.html", { root: __dirname });
});

const port = 3001;

app.listen(port, () => {
    console.log(`Server is running and listening on port ${port}`);
});