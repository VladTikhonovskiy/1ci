import React from "react";

import { Button } from "components/controls";
import { notFoundPage } from "images";

import classes from "./NotFoundPage.less";


export default class NotFoundPage extends React.PureComponent {
    get style() {
        return {
            backgroundImage:  `url(${notFoundPage})`
        };
    }

    render() {
        return (
            <div className={classes.notFoundPage} style={this.style}>
                <div className={classes.notFoundPageWrapper}>
                    <div className={classes.notFoundPageTextContainer}>
                        <h1> Page not found </h1>

                        <p>
                            Our apologies... <br/>
                            The document you requested does not exist on this server or <br/>
                            cannot be served.
                        </p>

                        <h3> Thank you! </h3>

                        <Button
                            link={"/"}
                            label={"Go to Homepage"}
                            className={classes.notFoundPageGoToHomepageLink}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
