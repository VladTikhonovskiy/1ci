import React from "react";
import ReactDOM from "react-dom";
import RegisterServiceWorker from "./registerServiceWorker";

import "styles/index.less";
import "styles/blocks.less";
import "styles/client.less";

import Application from "./containers/Application";


ReactDOM.render(
    <div>
        <Application />
    </div>,
    
    document.getElementById("root")
);


RegisterServiceWorker();