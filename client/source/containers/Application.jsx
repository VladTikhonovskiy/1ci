import React from "react";
import axios from "axios";

import blocks from "components/blocks";
import NotFoundPage from "components/NotFoundPage/NotFoundPage";


export default class Application extends React.Component {
    state = {
        blockList: [],
        render404Page: false
    }

    componentDidMount() {
        axios.get("http://api.1ci.dev.kingbird.ru/api/frontend/pages/search?url=" + window.location.pathname)
            .then(response => {
                this.setState({
                    blockList: response.data.version.blocks
                });
            })
            .catch(() => {
                this.setState({
                    render404Page: true
                });
            });
    }

    render() {
        let { blockList, render404Page } = this.state;

        if (render404Page) {
            return <NotFoundPage />;
        }

        return (
            <div>
                {
                    blockList.map(({ name, settings }) => {
                        let BlockTagName = blocks[name];

                        return <BlockTagName {...settings} />;
                    })
                }
            </div>
        );
    }
}