import thunk from "redux-thunk";
import { compose, createStore, combineReducers, applyMiddleware } from "redux";

import api from "../api";
import app from "./app/app.reducer";
import auth from "./auth/auth.reducer";
import pageBuilder from "./page-builder/page-builder.reducer";

import { initialize } from "./app/app.actions";


const rootReducer = combineReducers({ app, auth, pageBuilder });

const store = createStore(rootReducer, undefined, compose(
    applyMiddleware(thunk.withExtraArgument(api)),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

store.dispatch(initialize());


export default store;