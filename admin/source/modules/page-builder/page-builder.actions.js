import shortid from "shortid";
import debounceAction from "debounce-action";
import { cloneDeep } from "lodash";
import { createAction } from "redux-act";

import { normalizePages } from "./page-builder.schema";
import * as applicationActions from "modules/app/app.actions";
import * as pageBuilderSelectors from "./page-builder.selectors";

import blockSettingsMap from "components/blocks/settings";


export const selectTab = createAction("select tab");
export const selectPage = createAction("select page");
export const deselectPage = createAction("deselect page");
export const togglePage = createAction("toggle page");
export const selectBlock = createAction("select block");
export const selectPageTemplate = createAction("select page template");
export const resetBlockSelection = createAction("reset block selection");
export const showPageCodeDialog = createAction("show page code dialog");
export const hidePageCodeDialog = createAction("show page code dialog");

export const showPageBuilderNavigation = createAction("show page builder navigation");
export const hidePageBuilderNavigation = createAction("hide page builder navigation");

export const resetPageTemplateSectionSelection = createAction("reset page template section selection");
export const selectPageTemplateSection = createAction("select page template section");

export const addFooterMenuItem = createAction("add footer menu item");
export const editFooterMenuItem = createAction("edit footer menu item");
export const moveFooterMenuItem = createAction("move footer menu item");
export const removeFooterMenuItem = createAction("remove footer menu item");

export const addFooterSocialLink = createAction("add footer social link");
export const editFooterSocialLink = createAction("edit footer social link");
export const moveFooterSocialLink = createAction("move footer social link");
export const removeFooterSocialLink = createAction("remove footer social link");

export const addFooterSecondaryLink = createAction("add footer secondary link");
export const editFooterSecondaryLink = createAction("edit footer secondary link");
export const moveFooterSecondaryLink = createAction("move footer secondary link");
export const removeFooterSecondaryLink = createAction("remove footer secondary link");

export const addHeaderMenuItem = createAction("add header menu item");
export const editHeaderMenuItem = createAction("edit header menu item");
export const moveHeaderMenuItem = createAction("move header menu item");
export const removeHeaderMenuItem = createAction("remove header menu item");


export const createBlock = block => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { pageList, selectedPageID } = pageBuilder;

    if (selectedPageID === -1)
        return;

    let stateToRestore = cloneDeep({ pageList });
    dispatch(applicationActions.addUndoableAction({
        name: "create-block",
        restoreState: stateToRestore,
        restoreAction: updatePageBlocks,
        restoreActionArguments: [selectedPageID]
    }));

    dispatch(createBlockSuccess({
        id: shortid.generate(),
        name: block.name,
        pageID: selectedPageID,
        settings: blockSettingsMap[block.name]
    }));

    dispatch(updatePageBlocks());
}

export const createBlockSuccess = createAction("create block / success");


export const cloneBlock = blockID => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { pageList, selectedPageID } = pageBuilder;

    let stateToRestore = cloneDeep({ pageList });
    dispatch(applicationActions.addUndoableAction({
        name: "clone-block",
        restoreState: stateToRestore,
        restoreAction: updatePageBlocks,
        restoreActionArguments: [selectedPageID]
    }));

    dispatch(cloneBlockSuccess(blockID));
    dispatch(updatePageBlocks());
}

export const cloneSelectedBlock = () => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { selectedBlockID } = pageBuilder;

    return dispatch(cloneBlock(selectedBlockID));
}

export const cloneBlockSuccess = createAction("clone block / success");


export const moveBlock = (direction, blockID) => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { pageList, selectedPageID } = pageBuilder;

    let stateToRestore = cloneDeep({ pageList });
    dispatch(applicationActions.addUndoableAction({
        name: "move-block",
        restoreState: stateToRestore,
        restoreAction: updatePageBlocks,
        restoreActionArguments: [selectedPageID]
    }));

    dispatch(moveBlockSuccess({ direction, blockID }));
    dispatch(updatePageBlocks());
}

export const moveSelectedBlock = direction => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { selectedBlockID } = pageBuilder;

    return dispatch(moveBlock(direction, selectedBlockID));
}

export const moveBlockSuccess = createAction("move block / success");


export const removeBlock = blockID => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { pageList, selectedPageID } = pageBuilder;

    let stateToRestore = cloneDeep({ pageList });
    dispatch(applicationActions.addUndoableAction({
        name: "remove-block",
        restoreState: stateToRestore,
        restoreAction: updatePageBlocks,
        restoreActionArguments: [selectedPageID]
    }));

    dispatch(removeBlockSuccess(blockID));
    dispatch(updatePageBlocks());
}

export const removeSelectedBlock = () => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { selectedBlockID } = pageBuilder;

    return dispatch(removeBlock(selectedBlockID));
}

export const removeBlockSuccess = createAction("remove block / success");


export const createPage = () => async (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { pageList, selectedPageID } = pageBuilder;

    let stateToRestore = cloneDeep({ pageList });

    let createdPage = await api.pageBuilder.createPage(selectedPageID);
    dispatch(createPageSuccess(createdPage));

    dispatch(applicationActions.addUndoableAction({
        name: "create-page",
        restoreState: stateToRestore,
        restoreAction: removePage,
        restoreActionArguments: [createdPage.id]
    }));
}

export const createPageSuccess = createAction("create page success");


export const removePage = pageID => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { selectedPageID } = pageBuilder;
    let { showMessage } = applicationActions;

    let pageIDToRemove = pageID || selectedPageID;

    return api.pageBuilder.removePage(pageIDToRemove)
        .then(() => {
            dispatch(removePageSuccess(pageIDToRemove));
            dispatch(showMessage("Page has been successfully removed"));
        })
        .catch(() => {
            dispatch(showMessage("An error occured during page remove"));
        });
}

export const removeSelectedPage = () => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { selectedPageID } = pageBuilder;

    return dispatch(removePage(selectedPageID));
}

export const removePageSuccess = createAction("remove page / success");



export const showBlockSettings = createAction("show block settings");
export const hideBlockSettings = createAction("hide block settings");


export const showPageSettings = createAction("show page settings");
export const hidePageSettings = createAction("hide page settings");
export const changePageSettings = createAction("change page settings");

export const showPagePreview = createAction("show page preview");
export const hidePagePreview = createAction("hide page preview");


export const updatePageSettings = updatedPage => (dispatch, getState, api) => {
    dispatch(changePageSettings(updatedPage));
    dispatch(sendPageUpdateRequestDebounced(updatedPage));
}

export const sendPageUpdateRequest = (updatedPage, undoable = true) => async (dispatch, getState, api) => {
    if (undoable) {
        let updatedPageServerVersion = await api.pageBuilder.fetchPage(updatedPage.id);

        let stateToRestore = getStateToRestore(updatedPageServerVersion);
        dispatch(applicationActions.addUndoableAction({
            name: "update-page-settings",
            restoreState: stateToRestore,
            restoreAction: sendPageUpdateRequest,
            restoreActionArguments: [updatedPageServerVersion, false]
        }));
    }

    return api.pageBuilder.updatePage(updatedPage);


    function getStateToRestore(updatedPageServerVersion) {
        let { pageBuilder } = getState();

        let updatedPageList = pageBuilder.pageList.map(page => {
            if (page.id === updatedPageServerVersion.id) {
                return updatedPageServerVersion;
            }

            return page;
        });

        return {
            pageList: updatedPageList
        };
    }
}

export const sendPageUpdateRequestDebounced = debounceAction(sendPageUpdateRequest, 500);


export const changeBlockSettings = settings => (dispatch, getState, api) => {
    dispatch(changeBlockSettingsSuccess(settings));
    dispatch(updatePageBlocksDebounced(null, true));
}

export const updatePageBlocks = (selectedPageID, undoable = false) => async (dispatch, getState, api) => {
    let { pageBuilder } = getState();

    if (undoable) {
        let { selectedPageID } = pageBuilder;

        let updatedPageServerVersion = await api.pageBuilder.fetchPage(selectedPageID);

        let stateToRestore = getStateToRestore(updatedPageServerVersion);
        dispatch(applicationActions.addUndoableAction({
            name: "update-block-settings",
            restoreState: stateToRestore,
            restoreAction: updatePageBlocks,
            restoreActionArguments: [selectedPageID]
        }));
    }

    let selectedPage = pageBuilderSelectors.selectedPage({
        ...pageBuilder,
        selectedPageID: selectedPageID || pageBuilder.selectedPageID
    });

    return api.pageBuilder.updatePageBlocks(selectedPage);


    function getStateToRestore(updatedPageServerVersion) {
        let { pageBuilder } = getState();

        let updatedPageList = pageBuilder.pageList.map(page => {
            if (page.id === updatedPageServerVersion.id) {
                return updatedPageServerVersion;
            }

            return page;
        });

        return {
            pageList: updatedPageList
        };
    }
}

export const updatePageBlocksDebounced = debounceAction(updatePageBlocks, 500);

export const changeBlockSettingsSuccess = createAction("change block settings / success");


export const cloneSelectedBlockInfoSave = createAction("clone selected block info save");
export const cloneSelectedBlockInfoClean = createAction("clone selected block info clean");
export const cloneBlockInsertInPage = createAction("clone block insert in page");


export const publishSelectedPage = () => (dispatch, getState, api) => {
    let { pageBuilder } = getState();
    let { showMessage } = applicationActions;

    let selectedPage = pageBuilderSelectors.selectedPage(pageBuilder);

    return api.pageBuilder.publishPage(selectedPage)
        .then(() => {
            dispatch(showMessage("Page has been successfully published"));
        })
        .catch(error => {
            dispatch(showMessage("An error occured during page publishing"));
        });
}

export const fetchPageList = () => (dispatch, getState, api) => {
    dispatch(fetchPageListStart());

    return api.pageBuilder.fetchPageList()
        .then(fetchedPages => {
            let normalizedPages = normalizePages(fetchedPages);

            dispatch(fetchPageListSuccess(normalizedPages));
        })
        .catch(error => {
            dispatch(fetchPageListFailure(error.message));
        });
}

export const fetchPageListStart = createAction("fetch page list / start");
export const fetchPageListSuccess = createAction("fetch page list / success");
export const fetchPageListFailure = createAction("fetch page list / failure");

export const getUrlList = urlName => (dispatch, getState, api) => {
    dispatch(fetchUrlListStart());

    return api.pageBuilder.fetchUrlList(urlName)
        .then(fetchedUrls => {
            let normalizedFetchedUrls = fetchedUrls.map(item => item.url);

            dispatch(fetchUrlListSuccess(normalizedFetchedUrls));
        })
        .catch(error => {
            dispatch(fetchUrlListFailure(error.message));
        });
}

export const fetchUrlListStart = createAction("fetch url list / start");
export const fetchUrlListSuccess = createAction("fetch url list / success");
export const fetchUrlListFailure = createAction("fetch url list / failure");


export const uploadImage = image => (dispatch, getState, api) => {
    return api.pageBuilder.uploadImage(image);
}