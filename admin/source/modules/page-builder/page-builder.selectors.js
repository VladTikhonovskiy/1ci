import { includes } from "lodash";
import { createSelector } from "reselect";


const pageList = state => state.pageList;
const selectedPageID = state => state.selectedPageID;
const selectedBlockID = state => state.selectedBlockID;
const toggledPagesIDs = state => state.toggledPagesIDs;
const fetchPageListStatus = state => state.fetchPageListStatus;


export const selectedPage = createSelector(pageList, selectedPageID, (pageList, selectedPageID) => {
    if (selectedPageID === -1)
        return null;

    return pageList.find(page => page.id === selectedPageID);
});

export const selectedPageBlocks = createSelector(selectedPage, selectedPage => {
    if (!selectedPage)
        return [];

    return selectedPage.version.blocks || [];
});

export const selectedBlock = createSelector(selectedPage, selectedBlockID, selectedPageBlocks, (selectedPage, selectedBlockID, selectedPageBlocks) => {
    if (selectedBlockID === "" || selectedPage === null)
        return null;

    return selectedPageBlocks.find(block => block.id === selectedBlockID);
});

export const isPageListFetching = createSelector(fetchPageListStatus, status => status === "fetching");

export const pageListTreeView = createSelector(pageList, selectedPageID, toggledPagesIDs, (pageList, selectedPageID, toggledPagesIDs) => {
    if (pageList.length === 0)
        return [];

    let pageListTreeView = [];
    generatePageListTreeView(pageListTreeView, pageList, 0, null);

    return pageListTreeView;


    function generatePageListTreeView(result = [], pageList = [], level = 0, parentPageID) {
        let currentLevelPages = pageList.filter(page => page.parentId === parentPageID);

        currentLevelPages.forEach(page => {
            let treeViewPage = {
                ...page,
                level,
                toggled: includes(toggledPagesIDs, page.id),
                selected: page.id === selectedPageID,
                children: []
            };

            result.push(treeViewPage);

            generatePageListTreeView(treeViewPage.children, pageList, level + 1, page.id);
        });
    }
});