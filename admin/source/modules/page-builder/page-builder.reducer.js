import shortid from "shortid";
import { clone, cloneDeep } from "lodash";
import { createReducer } from "redux-act";

import * as appActions from "../app/app.actions";
import * as pageBuilderActions from "./page-builder.actions";
import * as pageBuilderSelectors from "./page-builder.selectors";


const initialState = {
    pageList: [],
    selectedPageID: -1,
    selectedBlockID: "",
    selectedTabName: "pages",
    selectedPageTemplateSection: "",
    toggledPagesIDs: [],
    pagePreviewShown: false,
    pageSettingsShown: false,
    blockSettingsShown: false,
    pageBuilderNavigationShown: true,
    isPageTemplateSelected: false,
    fetchPagesStatus: "",
    headerMenu: [],
    footerMenu: [],
    footerSocialLinks: [],
    footerSecondaryLinks: [],
    urlList: [],
    fetchUrlListStatus: "",
    pageCodeDialogShown: false,
    cloneBlockInfo: {
        selectedBlockID:"",
        selectedPageID : "",
    }
};

const reducer = {
    [pageBuilderActions.selectPage]: (state, page) => ({
        ...state,
        selectedPageID: page.id,
        isPageTemplateSelected: false
    }),

    [pageBuilderActions.selectPageTemplate]: state => ({
        ...state,
        selectedPageID: -1,
        isPageTemplateSelected: true
    }),

    [pageBuilderActions.selectPageTemplateSection]: (state, section) => ({
        ...state,
        selectedPageTemplateSection: section
    }),

    [pageBuilderActions.deselectPage]: (state) => ({
        ...state,
        selectedPageID: -1
    }),

    [pageBuilderActions.selectBlock]: (state, blockID) => ({
        ...state,
        selectedBlockID: blockID
    }),

    [pageBuilderActions.hidePageBuilderNavigation]: state => ({
        ...state,
        pageBuilderNavigationShown: false
    }),

    [pageBuilderActions.showPageBuilderNavigation]: state => ({
        ...state,
        pageBuilderNavigationShown: true
    }),

    [pageBuilderActions.resetBlockSelection]: state => ({
        ...state,
        selectedBlockID: ""
    }),

    [pageBuilderActions.resetPageTemplateSectionSelection]: state => ({
        ...state,
        selectedPageTemplateSection: ""
    }),

    [pageBuilderActions.showBlockSettings]: state => ({
        ...state,
        blockSettingsShown: true
    }),

    [pageBuilderActions.hideBlockSettings]: state => ({
        ...state,
        blockSettingsShown: false
    }),

    [pageBuilderActions.showPageSettings]: state => ({
        ...state,
        pageSettingsShown: true
    }),

    [pageBuilderActions.showPagePreview]: state => ({
        ...state,
        pagePreviewShown: true
    }),

    [pageBuilderActions.hidePagePreview]: state => ({
        ...state,
        pagePreviewShown: false
    }),

    [pageBuilderActions.hidePageSettings]: state => ({
        ...state,
        pageSettingsShown: false
    }),

    [pageBuilderActions.selectTab]: (state, tabName) => ({
        ...state,
        selectedTabName: tabName
    }),

    [pageBuilderActions.showPageCodeDialog]: state => ({
        ...state,
        pageCodeDialogShown: true
    }),

    [pageBuilderActions.hidePageCodeDialog]: state => ({
        ...state,
        pageCodeDialogShown: false
    }),

    [pageBuilderActions.togglePage]: (state, page) => {
        let { toggledPagesIDs } = state;

        return {
            ...state,
            toggledPagesIDs: getUpdatedToggledPagesIDs()
        };

        function getUpdatedToggledPagesIDs() {
            if (page.toggled)
                return toggledPagesIDs.filter(toggledPageID => toggledPageID !== page.id);

            return [...toggledPagesIDs, page.id];
        }
    },

    [pageBuilderActions.changePageSettings]: (state, updatedPage) => {
        let { pageList, selectedPageID } = state;

        return {
            ...state,
            pageList: getUpdatedPageList()
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    return updatedPage;
                }

                return page;
            });
        }
    },

    [pageBuilderActions.createPageSuccess]: (state, createdPage) => {
        let { pageList, toggledPagesIDs, selectedPageID } = state;

        return {
            ...state,
            pageList: [...pageList, createdPage],
            toggledPagesIDs: [...toggledPagesIDs, selectedPageID]
        };
    },

    [pageBuilderActions.removePageSuccess]: (state, pageIDToRemove) => {
        let { pageList, selectedPageID } = state;

        let updatedPageList = pageList.filter(page => page.id !== pageIDToRemove);

        let updatedSettings = {};
        if (selectedPageID === pageIDToRemove) {
            updatedSettings = {
                selectedPageID: -1,
                pageSettingsShown: false,
                pageBuilderNavigationShown: true
            };
        }

        return {
            ...state,
            ...updatedSettings,
            pageList: updatedPageList
        };
    },

    [pageBuilderActions.createBlockSuccess]: (state, block) => {
        let { pageList, selectedPageID } = state;

        return {
            ...state,
            pageList: getUpdatedPageList()
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    page.version.blocks = [...page.version.blocks || [], block];
                    return clone(page);
                }

                return page;
            });
        }
    },

    [pageBuilderActions.changeBlockSettingsSuccess]: (state, settings) => {
        let { pageList, selectedPageID, selectedBlockID } = state;

        return {
            ...state,
            pageList: getUpdatedPageList()
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    page.version.blocks = getUpdatedBlock(page);
                    return clone(page);
                }

                return page;
            });
        }

        function getUpdatedBlock(page) {
            return page.version.blocks.map(block => {
                if (block.id === selectedBlockID)
                    return { ...block, settings };

                return block;
            });
        }
    },

    [pageBuilderActions.cloneBlockSuccess]: (state, blockID) => {
        let { pageList, selectedPageID } = state;
        let selectedPageBlocks = pageBuilderSelectors.selectedPageBlocks(state);

        return {
            ...state,
            pageList: getUpdatedPageList()
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    let index = page.version.blocks.findIndex(block => block.id === blockID);
                    index += 1;
                    page.version.blocks.splice(index, 0, createBlockClone());
                    page.version.blocks = [...page.version.blocks];
                    return clone(page);
                }

                return page;
            });
        }

        function createBlockClone() {
            let sourceBlock = selectedPageBlocks.find(block => block.id === blockID);
            let clonedBlock = cloneDeep(sourceBlock);
            clonedBlock.id = shortid.generate();

            return clonedBlock;
        }
    },
    [pageBuilderActions.cloneBlockInsertInPage]: state => {
        let { pageList, selectedPageID } = state;
        let selectedPageBlocksIndex = state.pageList.findIndex(block => block.id === state.cloneBlockInfo.selectedPageID);
        let selectedPageBlocks = state.pageList[selectedPageBlocksIndex].version.blocks;

        let returnCloneBlock = createBlockClone();

        return {
            ...state,
            pageList: getUpdatedPageList()
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    page.version.blocks = [returnCloneBlock, ...page.version.blocks];
                    return clone(page);
                }
                return page;
            });
        }

        function createBlockClone() {
            let sourceBlock = selectedPageBlocks.find(block => block.id === state.cloneBlockInfo.selectedBlockID);
            let clonedBlock = cloneDeep(sourceBlock);
            clonedBlock.id = shortid.generate();

            return clonedBlock;
        }
    },

    [pageBuilderActions.cloneSelectedBlockInfoSave]: state => {
        let { selectedBlockID, selectedPageID } = state;

        let cloneBlockInfoUpdate = { selectedBlockID, selectedPageID };

        return {
            ...state,
            cloneBlockInfo: cloneBlockInfoUpdate
        };
    },

    [pageBuilderActions.cloneSelectedBlockInfoClean]: state => {
        let cloneBlockInfoUpdate = {
                selectedBlockID: "",
                selectedPageID: ""
            };

        return {
            ...state,
            cloneBlockInfo: cloneBlockInfoUpdate
        };
    },

    [pageBuilderActions.moveBlockSuccess]: (state, { direction, blockID }) => {
        let { pageList, selectedPageID, selectedBlockID } = state;
        let selectedPageBlocks = pageBuilderSelectors.selectedPageBlocks(state);

        let blockToMove = selectedPageBlocks.find(block => block.id === selectedBlockID);
        let blockToMoveIndex = selectedPageBlocks.findIndex(block => block.id === selectedBlockID);

        return {
            ...state,
            pageList: getUpdatedPageList()
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    let indexOffset = direction === "up" ? -1 : 1;
                    page.version.blocks = getUpdatedPageBlocks(page.version.blocks, blockToMoveIndex + indexOffset)

                    return clone(page);
                }

                return page;
            });
        }

        function getUpdatedPageBlocks(blocks, secondBlockIndex) {
            let secondBlock = selectedPageBlocks[secondBlockIndex];

            blocks[secondBlockIndex] = blockToMove;
            blocks[blockToMoveIndex] = secondBlock;

            return clone(blocks);
        }
    },

    [pageBuilderActions.removeBlockSuccess]: (state, blockID) => {
        let { pageList, selectedPageID } = state;

        let updatedPageList = getUpdatedPageList();

        return {
            ...state,
            selectedBlockID: "",
            pageList: updatedPageList
        };

        function getUpdatedPageList() {
            return pageList.map(page => {
                if (page.id === selectedPageID) {
                    page.version.blocks = page.version.blocks.filter(block => block.id !== blockID)
                    return clone(page);
                }

                return page;
            });
        }
    },

    [pageBuilderActions.fetchPageListStart]: state => ({
        ...state,
        fetchPageListStatus: "fetching"
    }),

    [pageBuilderActions.fetchPageListSuccess]: (state, fetchedPageList) => ({
        ...state,
        pageList: fetchedPageList,
        fetchPageListStatus: "success"
    }),

    [pageBuilderActions.fetchPageListFailure]: state => ({
        ...state,
        pageList: [],
        fetchPageListStatus: "failure"
    }),

    [pageBuilderActions.fetchUrlListStart]: state => ({
        ...state,
        fetchUrlListStatus: "fetching"
    }),

    [pageBuilderActions.fetchUrlListSuccess]: (state, fetchedUrlList) => ({
        ...state,
        urlList: fetchedUrlList,
        fetchUrlListStatus: "success"
    }),

    [pageBuilderActions.fetchUrlListFailure]: state => ({
        ...state,
        urlList: [],
        fetchUrlListStatus: "failure"
    }),

    [pageBuilderActions.addFooterMenuItem]: (state, menuItemToAdd) => {
        menuItemToAdd.id = shortid.generate();

        return {
            ...state,
            footerMenu: [...state.footerMenu, menuItemToAdd]
        };
    },

    [pageBuilderActions.editFooterMenuItem]: (state, menuItemToEdit) => {
        let updatedFooterMenu = state.footerMenu.map(menuItem => {
            if (menuItem.id === menuItemToEdit.id)
                return {
                    ...menuItem,
                    ...menuItemToEdit
                };

            return menuItem;
        });

        return {
            ...state,
            footerMenu: updatedFooterMenu
        };
    },

    [pageBuilderActions.moveFooterMenuItem]: (state, { column, oldIndex, newIndex }) => {
        let updatedFooterMenu = clone(state.footerMenu);

        let columnItems = updatedFooterMenu.filter(item => item.column === column.toString());
        let oldItem = columnItems[oldIndex];
        let newItem = columnItems[newIndex];

        let fadilOldIndex = updatedFooterMenu.findIndex(item => item.id === oldItem.id);
        let fadilNewIndex = updatedFooterMenu.findIndex(item => item.id === newItem.id);

        let temp = updatedFooterMenu[fadilOldIndex];
        updatedFooterMenu[fadilOldIndex] = updatedFooterMenu[fadilNewIndex];
        updatedFooterMenu[fadilNewIndex] = temp;

        return {
            ...state,
            footerMenu: updatedFooterMenu
        };
    },

    [pageBuilderActions.removeFooterMenuItem]: (state, menuItemToRemove) => ({
        ...state,
        footerMenu: state.footerMenu.filter(menuItem => menuItem.id !== menuItemToRemove.id)
    }),

    [pageBuilderActions.addFooterSocialLink]: (state, socialLinkToAdd) => {
        socialLinkToAdd.id = shortid.generate();

        return {
            ...state,
            footerSocialLinks: [...state.footerSocialLinks, socialLinkToAdd]
        };
    },

    [pageBuilderActions.editFooterSocialLink]: (state, socialLinkToEdit) => {
        let updatedFooterSocialLinks = state.footerSocialLinks.map(socialLink => {
            if (socialLink.id === socialLinkToEdit.id)
                return {
                    ...socialLink,
                    ...socialLinkToEdit
                };

            return socialLink;
        });

        return {
            ...state,
            footerSocialLinks: updatedFooterSocialLinks
        };
    },

    [pageBuilderActions.moveFooterSocialLink]: (state, { oldIndex, newIndex }) => {
        let updatedFooterSocialLinks = clone(state.footerSocialLinks);

        let temp = updatedFooterSocialLinks[oldIndex];
        updatedFooterSocialLinks[oldIndex] = updatedFooterSocialLinks[newIndex];
        updatedFooterSocialLinks[newIndex] = temp;

        return {
            ...state,
            footerSocialLinks: updatedFooterSocialLinks
        };
    },

    [pageBuilderActions.removeFooterSocialLink]: (state, socialLinkToRemove) => ({
        ...state,
        footerSocialLinks: state.footerSocialLinks.filter(socialLink => socialLink.id !== socialLinkToRemove.id)
    }),

    [pageBuilderActions.addFooterSecondaryLink]: (state, secondaryLinkToAdd) => {
        secondaryLinkToAdd.id = shortid.generate();

        return {
            ...state,
            footerSecondaryLinks: [...state.footerSecondaryLinks, secondaryLinkToAdd]
        };
    },

    [pageBuilderActions.editFooterSecondaryLink]: (state, secondaryLinkToEdit) => {
        let updatedFooterSecondaryLinks = state.footerSecondaryLinks.map(secondaryLink => {
            if (secondaryLink.id === secondaryLinkToEdit.id)
                return {
                    ...secondaryLink,
                    ...secondaryLinkToEdit
                };

            return secondaryLink;
        });

        return {
            ...state,
            footerSecondaryLinks: updatedFooterSecondaryLinks
        };
    },

    [pageBuilderActions.moveFooterSecondaryLink]: (state, { oldIndex, newIndex }) => {
        let updatedFooterSecondaryLinks = clone(state.footerSecondaryLinks);

        let temp = updatedFooterSecondaryLinks[oldIndex];
        updatedFooterSecondaryLinks[oldIndex] = updatedFooterSecondaryLinks[newIndex];
        updatedFooterSecondaryLinks[newIndex] = temp;

        return {
            ...state,
            footerSecondaryLinks: updatedFooterSecondaryLinks
        };
    },

    [pageBuilderActions.removeFooterSecondaryLink]: (state, secondaryLinkToRemove) => ({
        ...state,
        footerSecondaryLinks: state.footerSecondaryLinks.filter(secondaryLink => secondaryLink.id !== secondaryLinkToRemove.id)
    }),

    [pageBuilderActions.addHeaderMenuItem]: (state, menuItemToAdd) => {
        menuItemToAdd.id = shortid.generate();

        return {
            ...state,
            headerMenu: [...state.headerMenu, menuItemToAdd]
        };
    },

    [pageBuilderActions.editHeaderMenuItem]: (state, menuItemToEdit) => {
        let updatedHeaderMenu = state.headerMenu.map(menuItem => {
            if (menuItem.id === menuItemToEdit.id)
                return {
                    ...menuItem,
                    ...menuItemToEdit
                };

            return menuItem;
        });

        return {
            ...state,
            headerMenu: updatedHeaderMenu
        };
    },

    [pageBuilderActions.moveHeaderMenuItem]: (state, { oldIndex, newIndex }) => {
        let updatedHeaderMenu = clone(state.headerMenu);

        let temp = updatedHeaderMenu[oldIndex];
        updatedHeaderMenu[oldIndex] = updatedHeaderMenu[newIndex];
        updatedHeaderMenu[newIndex] = temp;

        return {
            ...state,
            headerMenu: updatedHeaderMenu
        };
    },

    [pageBuilderActions.removeHeaderMenuItem]: (state, menuItemToRemove) => ({
        ...state,
        headerMenu: state.headerMenu.filter(menuItem => menuItem.id !== menuItemToRemove.id)
    }),

    [appActions.undoSuccess]: (state, restoreState) => ({
        ...state,
        ...restoreState
    })
};


export default createReducer(reducer, initialState);