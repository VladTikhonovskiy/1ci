import { omit } from "lodash";


export function normalizePages(pages) {
    let result = flattenPagesTree(pages);

    return result.map(page => omit(page, ["children"]));
}


function flattenPagesTree(pages, result = []) {
    pages.forEach(page => {
        result.push(page);

        if (page.children) {
            flattenPagesTree(page.children, result);
        }
    });

    return result;
}