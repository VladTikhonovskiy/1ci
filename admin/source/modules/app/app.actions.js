import store from "store";
import { createAction } from "redux-act";

import { setUser } from "modules/auth/auth.actions";


// initialize
export const initialize = () => (dispatch, getState, api) => {
    let user = store.get("user");

    if (user) {
        dispatch(setUser(user));
    }
}

// navigation
export const hideNavigation = createAction("hide navigation");
export const showNavigation = createAction("show navigation");

// messages
export const showMessage = createAction("show message");
export const hideMessage = createAction("hide message");

export const addUndoableAction = createAction("add undoable action");
export const updateUndoableAction = createAction("update undoable action");

export const undo = () => (dispatch, getState, api) => {
    let { app } = getState();
    let { lastUndoableAction } = app;
    let { restoreState, restoreAction, restoreActionArguments } = lastUndoableAction;

    dispatch(undoSuccess(restoreState));
    dispatch(restoreAction(...restoreActionArguments));
}

export const undoSuccess = createAction("undo / success");