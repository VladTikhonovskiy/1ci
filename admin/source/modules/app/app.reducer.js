import { createReducer } from "redux-act";

import * as appActions from "./app.actions";


const initialState = {
    navigationShown: false,
    messageText: "",
    messageShown: false,
    lastUndoableAction: null
};

const reducer = {
    [appActions.hideNavigation]: state => ({
        ...state,
        navigationShown: false
    }),

    [appActions.showNavigation]: state => ({
        ...state,
        navigationShown: true
    }),

    [appActions.showMessage]: (state, messageText) => ({
        ...state,
        messageText,
        messageShown: true
    }),

    [appActions.hideMessage]: state => ({
        ...state,
        messageShown: false
    }),

    [appActions.addUndoableAction]: (state, undoableAction) => ({
        ...state,
        lastUndoableAction: undoableAction
    }),

    [appActions.updateUndoableAction]: (state, updatedUndoableAction) => {
        let { lastUndoableAction } = state;

        return {
            ...state,
            lastUndoableAction: {
                ...lastUndoableAction,
                ...updatedUndoableAction
            }
        };
    },

    [appActions.undoSuccess]: state => ({
        ...state,
        lastUndoableAction: null
    })
};


export default createReducer(reducer, initialState);