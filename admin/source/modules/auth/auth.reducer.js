import { createReducer } from "redux-act";

import * as actions from "./auth.actions";


const initialState = {
    user: null,
    loginStatus: "",
    logoutStatus: "",
    errorMessage: ""
};

const reducer = {
    [actions.setUser]: (state, user) => ({
        ...state,
        user
    }),

    [actions.loginStart]: state => ({
        ...state,
        loginStatus: "pending"
    }),

    [actions.loginSuccess]: (state, user) => ({
        ...state,
        user,
        loginStatus: "success",
        errorMessage: ""
    }),

    [actions.loginFailure]: (state, message) => ({
        ...state,
        user: null,
        loginStatus: "failure",
        errorMessage: message
    }),

    [actions.logoutStart]: state => ({
        ...state,
        logoutStatus: "pending"
    }),

    [actions.logoutSuccess]: state => ({
        ...state,
        user: null,
        logoutStatus: "success"
    }),

    [actions.logoutFailure]: state => ({
        ...state,
        logoutStatus: "failure"
    })
};


export default createReducer(reducer, initialState);