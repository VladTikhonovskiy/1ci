import store from "store";
import { createAction } from "redux-act";


// user
export const setUser = createAction("set user");

// login
export const login = (email, password) => (dispatch, getState, api) => {
    dispatch(loginStart());

    return api.auth.login(email, password)
        .then(() => {
            let user = {
                email
            };

            dispatch(loginSuccess(user));
            dispatch(addToLocalStorage("user", user));
        })
        .catch(error => {
            let errorMessage = error.response.data.message;
            dispatch(loginFailure(errorMessage));
        });
}

export const loginStart = createAction("login / start");
export const loginSuccess = createAction("login / success");
export const loginFailure = createAction("login / failure");


// logout
export const logout = () => (dispatch, getState, api) => {
    dispatch(logoutStart());

    return api.auth.logout()
        .then(() => {
            dispatch(logoutSuccess());
            dispatch(removeFromLocalStorage("user"));
        })
        .catch(error => {
            dispatch(logoutFailure(error.message));
        });
}

export const logoutStart = createAction("logout / start");
export const logoutSuccess = createAction("logout / success");
export const logoutFailure = createAction("logout / failure");


// local storage
export const addToLocalStorage = (key, value) => (dispatch, getState, api) => {
    store.set(key, value);
}

export const removeFromLocalStorage = key => (dispatch, getState, api) => {
    store.remove(key);
}