import { createSelector } from "reselect";


const loginStatus = state => state.loginStatus;


export const isLoginPending = createSelector(loginStatus, status => status === "pending");