// cta
import GetStartedComponent from "./cta/GetStarted/GetStarted";
import ReadyToJoinComponent from "./cta/ReadyToJoin/ReadyToJoin";

// cards
import Cards1Component from "./cards/Cards1/Cards1";
import Cards2Component from "./cards/Cards2/Cards2";
import Cards3Component from "./cards/Cards3/Cards3";

// link with icon
import LinkWithIconComponent from "./buttonsAndLinks/LinkWithIcon/LinkWithIcon";
import LinkWithButtonComponent from "./buttonsAndLinks/LinkWithButton/LinkWithButton";

// heroes
import Hero1Component from "./hero/Hero1/Hero1";
import Hero2Component from "./hero/Hero2/Hero2";
import Hero3Component from "./hero/Hero3/Hero3";
import Hero4Component from "./hero/Hero4/Hero4";
import Hero6Component from "./hero/Hero6/Hero6";

// titles
import Title1Component from "./title/Title1/Title1";
import Title2Component from "./title/Title2/Title2";
import Title3Component from "./title/Title3/Title3";

// common
import BreadcrumbsComponent from "./common/Breadcrumbs/Breadcrumbs";

// benefits
import Benefits1Component from "./benefits/Benefits1/Benefits1";
import Benefits2Component from "./benefits/Benefits2/Benefits2";
import Benefits3Component from "./benefits/Benefits3/Benefits3";
import Benefits4Component from "./benefits/Benefits4/Benefits4";
import PersonsComponent from "./benefits/Persons/Persons";

// partners
import Partners1Component from "./partners/Partners1/Partners1";
import Partners2Component from "./partners/Partners2/Partners2";

// dividers
import SimpleDividerComponent from "./dividers/SimpleDivider/SimpleDivider";
import CountryDividerComponent from "./dividers/CountryDivider/CountryDivider";

// separators
import SimpleSeparatorComponent from "./separators/SimpleSeparator/SimpleSeparator";

// statistics
import Statistics1Component from "./statistics/Statistics1/Statistics1";

// products
import EnterpriseComponent from "./products/Enterprise/Enterprise";
import Products1Component from "./products/Products1/Products1";
import Products2Component from "./products/Products2/Products2";

// features
import Features1Component from "./features/Features1/Features1";
import BigIcons1Component from "./features/BigIcons1/BigIcons1";
import BigIcons2Component from "./features/BigIcons2/BigIcons2";
import BigIcons3Component from "./features/BigIcons3/BigIcons3";
import ImageListComponent from "./features/ImageList/ImageList";
import ImageList2Component from "./features/ImageList2/ImageList2";
import ListImageComponent from "./features/ListImage/ListImage";
import ChecklistComponent from "./features/Checklist/Checklist";
import ContactsComponent from "./features/Contacts/Contacts";
import BrochureComponent from "./features/Brochure/Brochure";

// HtmlEditor
import HtmlEditorComponent from "./htmlEditor/HtmlEditor";

// reviews
import ReviewsComponent from "./reviews/Reviews/Reviews";
import Reviews2Component from "./reviews/Reviews2/Reviews2";

// visual
import MainComponent from "./visual/Main/Main";

// infographic
import InfographicComponent  from "./infographic/Infographic";

// template
import HeaderMenu from "./template/HeaderMenu/HeaderMenu";
import FooterMenu from "./template/FooterMenu/FooterMenu";


export default {
    // cta
    GetStarted: GetStartedComponent,
    ReadyToJoin: ReadyToJoinComponent,

    // link with icon
    LinkWithIcon: LinkWithIconComponent,
    LinkWithButton: LinkWithButtonComponent,

    // cards
    Cards1: Cards1Component,
    Cards2: Cards2Component,
    Cards3: Cards3Component,

    // hero
    Hero1: Hero1Component,
    Hero2: Hero2Component,
    Hero3: Hero3Component,
    Hero4: Hero4Component,
    Hero6: Hero6Component,

    // titles
    Title1: Title1Component,
    Title2: Title2Component,
    Title3: Title3Component,

    // common
    Breadcrumbs: BreadcrumbsComponent,

    // benefits
    Benefits1: Benefits1Component,
    Benefits2: Benefits2Component,
    Benefits3: Benefits3Component,
    Benefits4: Benefits4Component,
    Persons: PersonsComponent,

    // partners
    Partners1: Partners1Component,
    Partners2: Partners2Component,

    // dividers & separators
    SimpleDivider: SimpleDividerComponent,
    CountryDivider: CountryDividerComponent,
    SimpleSeparator: SimpleSeparatorComponent,

    // statistics
    Statistics1: Statistics1Component,

    // products
    Enterprise: EnterpriseComponent,
    Products1: Products1Component,
    Products2: Products2Component,

    // features
    Features1: Features1Component,
    BigIcons1: BigIcons1Component,
    BigIcons2: BigIcons2Component,
    BigIcons3: BigIcons3Component,
    ImageList: ImageListComponent,
    ImageList2: ImageList2Component,
    ListImage: ListImageComponent,
    Checklist: ChecklistComponent,
    Contacts: ContactsComponent,
    Brochure: BrochureComponent,

    // HtmlEditor
    HtmlEditor: HtmlEditorComponent,

    // reviews
    Reviews: ReviewsComponent,
    Reviews2: Reviews2Component,

    // visual
    Main: MainComponent,

    // infographic
    Infographic: InfographicComponent
};

export {
    HeaderMenu,
    FooterMenu
};
