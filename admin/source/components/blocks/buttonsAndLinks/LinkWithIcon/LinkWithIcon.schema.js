export default {
    id: {
        type: "string",
        caption: "ID"
    },

    linkButton: {
        type: "button",
        caption: "First Button"
    },

    iconButton: {
        type: "dropdown",
        caption: "Icon",
        options: [ "Download PDF", "Arrow"]
    }
};