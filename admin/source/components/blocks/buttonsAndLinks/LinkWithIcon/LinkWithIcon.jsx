import React from "react";
import classnames from "classnames";
import { shape, string } from "prop-types";

import { Icon } from "components/common";
import { Link } from "components/controls";

import classes from "./LinkWithIcon.less";
import defaultProps from "./LinkWithIcon.settings";


export default class LinkWithIcon extends React.PureComponent {
    static propTypes = {
        id: string,
        linkButton: shape({
            link: string,
            label: string
        }),
        iconButton: string,
        className: string
    }

    static defaultProps = defaultProps

    get classes() {
        return classnames(classes.readyToJoin, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    render() {
        let { linkButton, iconButton } = this.props;

        return (
            <div className={this.classes} {...this.attributes}>
                <div className={classes.linkWithIconContainer}>
                    <a href={linkButton.url} className={classes.linkWithIconLink}>
                        {
                            iconButton === "Download PDF" ?
                            <React.Fragment>
                                <Icon name="filePdf" className={classes.linkWithIconIcon}/>
                                {linkButton.label}
                            </React.Fragment>
                            :
                            <Link
                                url={linkButton.url}
                                label={linkButton.label}
                                showArrow={true}
                            />
                        }
                    </a>
                </div>
            </div>
        );
    }
}