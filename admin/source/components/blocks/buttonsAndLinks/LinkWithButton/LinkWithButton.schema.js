export default {
    id: {
        type: "string",
        caption: "ID"
    },

    firstButton: {
        type: "button",
        caption: "First Button"
    },

    secondButton: {
        type: "button",
        caption: "Second Button"
    }
};