import React from "react";
import classnames from "classnames";
import { shape, string } from "prop-types";

import { Button } from "components/controls";

import classes from "./LinkWithButton.less";
import defaultProps from "./LinkWithButton.settings";


export default class ReadyToJoin extends React.PureComponent {
    static propTypes = {
        id: string,
        firstButton: shape({
            link: string,
            label: string
        }),
        secondButton: shape({
          link: string,
          label: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    get classes() {
        return classnames(classes.readyToJoin, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    render() {
        let { firstButton, secondButton } = this.props;

        return (
            <div className={this.classes} {...this.attributes}>
                <div className={classes.linkWithButtonContainer}>

                    <div className={classes.linkWithButtonButtonWrapper}>
                        {
                            secondButton.label &&
                            <Button
                                type="alternate"
                                link={secondButton.link}
                                label={secondButton.label}
                                className={classes.linkWithButtonButton}
                            />
                        }

                        {
                            firstButton.label &&
                            <Button
                                link={firstButton.link}
                                label={firstButton.label}
                                className={classes.linkWithButtonButton}
                            />
                        }
                    </div>
                </div>
            </div>
        );
    }
}