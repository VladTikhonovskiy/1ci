import React from "react";
import classnames from "classnames";
import { string, object, arrayOf } from "prop-types";

import { Image, Icon } from "components/common";

import classes from "./HeaderMenu.less";


export default class HeaderMenu extends React.PureComponent {
    static propTypes = {
        menu: arrayOf(object),
        className: string
    }

    get classes() {
        return classnames(classes.headerMenu, this.props.className);
    }

    renderMenuItemContent(menuItem) {
        let { title, linkType, dropdown } = menuItem;

        return (
            <React.Fragment>
                { title }

                {
                    linkType === "external-link" &&
                    <Icon name="link" className={classes.headerMenuMenuItemLinkIcon}/>
                }

                {
                    dropdown &&
                    <Icon name="arrowDown" className={classes.headerMenuMenuItemDropdownIcon} />
                }
            </React.Fragment>
        )
    }

    renderMenuItem(menuItem) {
        let { id, isLink, linkHref, linkTarget } = menuItem;

        if (isLink)
            return (
                <a key={id} href={linkHref} target={linkTarget} className={classes.headerMenuMenuItem}>
                    { this.renderMenuItemContent(menuItem) }
                </a>
            );

        return (
            <span key={id} className={classes.headerMenuMenuItem}>
                { this.renderMenuItemContent(menuItem) }
            </span>
        );
    }

    render() {
        return (
            <div className={this.classes}>
                <div className={classes.headerMenuContainer}>
                    <Image name="logo" className={classes.headerMenuLogo} />

                    <div className={classes.headerMenuMenuItemContainer}>
                        { this.props.menu.map(menuItem => this.renderMenuItem(menuItem)) }
                    </div>
                </div>
            </div>
        );
    }
}