import React from "react";
import classnames from "classnames";
import { string, object, arrayOf } from "prop-types";

import { Image, Icon } from "components/common";

import classes from "./FooterMenu.less";


export default class FooterMenu extends React.PureComponent {
    static propTypes = {
        menu: arrayOf(object),
        socialLinks: arrayOf(object),
        secondaryLinks: arrayOf(object),
        className: string
    }

    get classes() {
        return classnames(classes.footerMenu, this.props.className);
    }

    renderMenuItem(menuItem) {
        let { id, title, type, isLink, linkType } = menuItem;

        let menuItemClasses = classnames(classes.footerMenuMenuItem, {
            [classes.footerMenuMenuItemRegular]: type === "regular",
            [classes.footerMenuMenuItemHeadline]: type === "headline"
        });

        return (
            <span key={id} className={menuItemClasses}>
                { title }

                {
                    isLink && linkType === "external-link" &&
                    <Icon name="link" className={classes.footerMenuMenuItemLinkIcon} />
                }
            </span>
        );
    }

    renderSocialLink(socialLink) {
        let { id, icon } = socialLink;

        return (
            <div key={id} className={classes.footerMenuSocialLink}>
                <img src={icon} alt="" className={classes.footerMenuSocialLinkIcon} />
            </div>
        );
    }

    renderSecondaryLink(secondaryLink) {
        let { id, title } = secondaryLink;

        return (
            <span key={id} className={classes.footerMenuSecondaryLink}>
                { title }
            </span>
        );
    }

    render() {
        let { menu, socialLinks, secondaryLinks } = this.props;

        return (
            <div className={this.classes}>
                <div className={classes.footerMenuContainer}>
                    <Image name="logoWhite" className={classes.footerMenuLogo} />

                    <div className={classes.footerMenuLinksContainer}>
                        <div className={classes.footerMenuLinksContainerColumn}>
                            { menu.filter(item => item.column === "1").map(item => this.renderMenuItem(item)) }
                        </div>

                        <div className={classes.footerMenuLinksContainerColumn}>
                            { menu.filter(item => item.column === "2").map(item => this.renderMenuItem(item)) }
                        </div>

                        <div className={classes.footerMenuLinksContainerColumn}>
                            { menu.filter(item => item.column === "3").map(item => this.renderMenuItem(item)) }
                        </div>
                    </div>
                </div>

                <div className={classes.footerMenuBottomLine}>
                    <div className={classes.footerMenuBottomLineContainer}>
                        <span className={classes.footerMenuCopyright}>
                            © 2018 1C International
                        </span>

                        <div className={classes.footerMenuBottomLineLinksContainer}>
                            <div className={classes.footerMenuSecondaryLinksContainer}>
                                { secondaryLinks.map(secondaryLink => this.renderSecondaryLink(secondaryLink)) }
                            </div>

                            <div className={classes.footerMenuSocialLinks}>
                                { socialLinks.map(socialLink => this.renderSocialLink(socialLink)) }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}