import { defaultBackgroundColor, defaultTextColor } from "../../default";
import { dogImage } from "images";


export default {
    id: "",

    titleColor: defaultTextColor,
    backgroundColor: defaultBackgroundColor,

    descriptionColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    image: {
        url: dogImage
    },

    title: {
        tag: "h2",
        text: "Why AccountingSuite?",
        visible: true
    },

    banners: [
        {
            icon: {
                url: ""
            },
            bannerTitle: "Made with 100% Real People",
            description: {
                text: "TIs your current solution customer service more frustrating than the problem you call with? With AccountingSuite, you’ll always have a direct access to the team able to resolve any issue that may arise."
            }
        },
        {
            icon: {
                url: ""
            },
            bannerTitle: "Adjusting to Your Workflow",
            description: {
                text: "No need to adjust your workflow to the product. Instead, AccountingSuite can adjust to your processes by customizing the product to fit your specific needs."
            }
        },
        {
            icon: {
                url: ""
            },
            bannerTitle: "Integrated  Solution",
            description: {
                text: "Money spent, effort wasted on integrating several standalone add-ons that don’t speak to each other? AccountingSuite provides all the necessary modules integrated for smooth information flow."
            }
        }
    ]
};