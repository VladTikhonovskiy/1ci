import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, arrayOf, oneOf } from "prop-types";

import classes from "./Benefits4.less";
import defaultProps from "./Benefits4.settings";


export default class Benefits4 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        banners: arrayOf(shape({
            icon: shape({
                url: string
            }),
            description: shape({
                text: string
            })
        })),
        image: shape({
            url: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.benefits4, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderBannerIcon(icon) {
        let style = {};

        if (icon.url) {
            style.backgroundColor = "transparent";
            style.backgroundImage = `url(${icon.url})`;
        }

        return <div className={classes.benefits4BannerIcon} style={style}></div>;
    }

    renderTitledImage() {
        let { image, title, titleColor } = this.props;

        let TitleTagName = title.tag;
        let titleStyle = this.getTextColorStyle(titleColor);

        return (
            <div className={classes.benefits4TitledImageContainer}>
                <img src={image.url} alt="" className={classes.benefits4Image} />

                {
                    title.visible &&
                    <TitleTagName style={titleStyle} className={classes.benefits4Title}>
                        { title.text }
                    </TitleTagName>
                }
            </div>
        );
    }

    renderBanner(banner, key) {
        let { titleColor, descriptionColor } = this.props;
        let { icon, description, bannerTitle } = banner;

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyles = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} className={classes.benefits4Banner}>
                { this.renderBannerIcon(icon) }

                <p className={classes.benefits4BannerTitle} style={titleStyle}>
                    { bannerTitle }
                </p>

                <p
                    style={descriptionStyles}
                    className={classes.benefits4BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.benefits4Container}>
                    { this.renderTitledImage() }

                    <div className={classes.benefits4BannerWrapper}>
                        { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                    </div>
                </div>
            </div>
        );
    }
}