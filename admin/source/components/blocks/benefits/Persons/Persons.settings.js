import { point } from "images/icons";
import { defaultColorWhite, defaultTextColor } from "../../default";


export default {
    id: "",

    theme: "With descriptions",
    responsive: "One column",

    titleColor: defaultTextColor,
    descriptionColor: defaultTextColor,
    backgroundColor: defaultColorWhite,

    firstColumnTitle: {
        text: "Maximum Efficiency",
        visible: true
    },

    firstColumnSmallTitle: {
        text: "Professional Accountants",
        visible: true
    },

    firstColumnPoints: [
        {
            icon: {
                url: point
            },
            title: "Increase the value",
            description: "Increase firm’s value with all-in-one cloud accounting."
        },
        {
            icon: {
                url: point
            },
            title: "Boost your practice",
            description: "Grow your practice with product-based clients."
        },
        {
            icon: {
                url: point
            },
            title: "Work on multiple clients",
            description: "Simultaneously via tabs in the same browser."
        }
    ],

    secondColumnTitle: {
        text: "Company’s Performance at a Glance",
        visible: true
    },

    secondColumnSmallTitle: {
        text: "Business Managers",
        visible: true
    },

    secondColumnPoints: [
        {
            icon: {
                url: point
            },
            title: "Real-time control",
            description: "Track your company’s financial position."
        },
        {
            icon: {
                url: point
            },
            title: "Informed decisions",
            description: "Get data for taking informed business decisions."
        },
        {
            icon: {
                url: point
            },
            title: "Complete solution",
            description: "Manage all the key processes at your organization."
        }
    ]
};