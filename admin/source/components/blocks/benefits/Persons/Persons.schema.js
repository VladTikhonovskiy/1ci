import { point } from "images/icons";


export default {
    id: {
        type: "string",
        caption: "ID"
    },

    theme: {
        type: "dropdown",
        caption: "Theme",
        options: ["With descriptions", "Without descriptions"]
    },

    responsive: {
        type: "dropdown",
        caption: "Tablet responsive column quantity",
        options: ["One column", "Two columns"]
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    firstColumnTitle: {
        type: "title",
        caption: "First column title"
    },

    firstColumnSmallTitle: {
        type: "title",
        caption: "First column small title"
    },

    firstColumnPoints: {
        type: "list",
        minCount: 1,
        menuCollapse: true,
        caption: "First column points",
        addElementButtonCaption: "Add point",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            title: {
                type: "string",
                caption: "Title"
            },

            description: {
                type: "string",
                caption: "Description",
                multiLine: true
            }
        },

        newElement: {
            icon: {
                url: point
            },

            title: "Point title",
            description: "Point description"
        }
    },

    secondColumnTitle: {
        type: "title",
        caption: "Second column title"
    },

    secondColumnSmallTitle: {
        type: "title",
        caption: "Second column small title"
    },

    secondColumnPoints: {
        type: "list",
        minCount: 1,
        caption: "Second column points",
        menuCollapse: true,
        addElementButtonCaption: "Add point",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            title: {
                type: "string",
                caption: "Title"
            },

            description: {
                type: "string",
                caption: "Description",
                multiLine: true
            }
        },

        newElement: {
            icon: {
                url: point
            },

            title: "Point title",
            description: "Point description"
        }
    }
};