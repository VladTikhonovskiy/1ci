import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, arrayOf } from "prop-types";

import classes from "./Persons.less";
import defaultProps from "./Persons.settings";


export default class Persons extends React.PureComponent {
    static propTypes = {
        id: string,
        theme: oneOf([
            "With descriptions",
            "Without descriptions"
        ]),
        responsive: oneOf([
            "One column",
            "Two columns"
        ]),
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        firstColumnTitle: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        firstColumnSmallTitle: shape({
            text: string,
            visible: bool
        }),
        secondColumnTitle: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        secondColumnSmallTitle: shape({
            text: string,
            visible: bool
        }),
        firstColumnPoints: arrayOf(shape({
            icon: shape({
                url: string
            }),
            title: string,
            description: string,
        })),
        secondColumnPoints: arrayOf(shape({
            icon: shape({
                url: string
            }),
            title: string,
            descriptions: string
        })),
        className: string
    }

    static defaultProps = defaultProps;

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        let { theme } = this.props;

        return classnames(classes.persons, this.props.className, {
            [classes.personsWithDescriptions]: theme === "With descriptions",
            [classes.personsWithoutDescriptions]: theme === "Without descriptions"
        });
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderPoint(point, key) {
        let { titleColor, descriptionColor, theme, responsive} = this.props;

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        let pointClasses = classnames(classes.personsPoint,  {
            [classes.personsTwoPointInColumn]: responsive === "Two columns"
        });

        return (
            <div key={key} className={pointClasses}>
                <img src={point.icon.url} alt="" className={classes.personsPointIcon} />

                <div>
                    <p className={classes.personsPointTitle} style={titleStyle}>
                        { point.title }
                    </p>

                    {
                        theme === "With descriptions" &&
                        <p
                            style={descriptionStyle}
                            className={classes.personsPointDescription}
                            dangerouslySetInnerHTML={this.getContentHtml(point.description)}>
                        </p>
                    }
                </div>
            </div>
        );
    }

    renderTitle(title, smallTitle) {
        let { titleColor, descriptionColor } = this.props;

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        let titleClasses = classnames(classes.personsTitle, {
            [classes.personsTitleIncreased]: smallTitle.visible === false
        });

        return (
            <React.Fragment>
                {
                    smallTitle.visible &&
                    <p className={classes.personsSmallTitle} style={descriptionStyle}>
                        { smallTitle.text }
                    </p>
                }

                {
                    title.visible &&
                    <p className={titleClasses} style={titleStyle}>
                        { title.text }
                    </p>
                }
            </React.Fragment>
        );
    }

    render() {
        let {
            firstColumnTitle, firstColumnSmallTitle, firstColumnPoints,
            secondColumnTitle, secondColumnSmallTitle, secondColumnPoints
        } = this.props;

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.personsContainer}>
                    <div className={classes.personsColumnContainer}>
                        { this.renderTitle(firstColumnTitle, firstColumnSmallTitle) }
                        { firstColumnPoints.map((point, index) => this.renderPoint(point, index)) }
                    </div>

                    <div className={classes.personsColumnContainer}>
                        { this.renderTitle(secondColumnTitle, secondColumnSmallTitle) }
                        { secondColumnPoints.map((point, index) => this.renderPoint(point, index)) }
                    </div>
                </div>
            </div>
        );
    }
}