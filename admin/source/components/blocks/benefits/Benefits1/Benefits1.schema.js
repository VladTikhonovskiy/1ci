export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },


    banners: {
        type: "list",
        minCount: 1,
        caption: "Banner",
        addElementButtonCaption: "Add banner",

        element: {
            image: {
                type: "image",
                caption: "Image"
            },

            description: {
                type: "description",
                caption: "Description"
            }
        },

        newElement: {
            image: {
                url: ""
            },
            description: {
                text: "Description"
            }
        }
    }
}