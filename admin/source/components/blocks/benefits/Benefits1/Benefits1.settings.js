import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    descriptionColor: defaultTextColor,

    banners: [
        {
            image: {
                url: ""
            },
            description: {
                text: "Do you want to have a real-time visibility of your factory floors?"
            }
        },

        {
            image: {
                url: ""
            },
            description: {
                text: "Do you experience lack of reporting accuracy due to diverse systems integrated into different departments?"
            }
        },

        {
            image: {
                url: ""
            },
            description: {
                text: "Or maybe you have geographically distributed operations which complicate management?"
            }
        }
    ]
}