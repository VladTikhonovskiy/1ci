import { defaultBackgroundColor, defaultTextColor } from "../../default";
import { bicycleImage } from "images";


export default {
    id: "",

    titleColor: defaultTextColor,
    descriptionColor: defaultTextColor,
    backgroundColor: defaultBackgroundColor,

    image: {
        url: bicycleImage
    },

    title: {
        tag: "h2",
        text: "Why 1C:Drive?",
        visible: true
    },

    banners: [
        {
            icon: {
                url: ""
            },
            description: {
                text: "Times faster than you are typically used to."
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Times faster than you are typically used to."
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Complete enterprise software that combines process automation with business analytics, eliminating the need to buy costly add-ons."
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Technical support team is always at your disposal for seamless implementation and maintenance over time."
            }
        }
    ]
};