import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, arrayOf, oneOf } from "prop-types";

import classes from "./Benefits3.less";
import defaultProps from "./Benefits3.settings";


export default class Benefits3 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        banners: arrayOf(shape({
            icon: shape({
                url: string
            }),
            description: shape({
                text: string
            })
        })),
        image: shape({
            url: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.benefits3, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderBannerIcon(icon) {
        let style = {};

        if (icon.url) {
            style.backgroundColor = "transparent";
            style.backgroundImage = `url(${icon.url})`;
        }

        return <div className={classes.benefits3BannerIcon} style={style}></div>;
    }

    renderTitledImage() {
        let { image, title, titleColor } = this.props;

        let TitleTagName = title.tag;
        let titleStyle = this.getTextColorStyle(titleColor);

        return (
            <div className={classes.benefits3TitledImageContainer}>
                <img src={image.url} alt="" className={classes.benefits3Image} />

                {
                    title.visible &&
                    <TitleTagName style={titleStyle} className={classes.benefits3Title}>
                        { title.text }
                    </TitleTagName>
                }
            </div>
        );
    }

    renderBanner(banner, key) {
        let { descriptionColor } = this.props;
        let { icon, description } = banner;

        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} className={classes.benefits3Banner}>
                { this.renderBannerIcon(icon) }

                <p
                    style={descriptionStyle}
                    className={classes.benefits3BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.benefits3Container}>
                    { this.renderTitledImage() }

                    <div className={classes.benefits3BannerWrapper}>
                        { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                    </div>
                </div>
            </div>
        );
    }
}