export default {
    id: {
        type: "string",
        caption: "ID"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    image: {
        type: "image",
        caption: "Image"
    },

    banners: {
        type: "list",
        minCount: 1,
        caption: "Banner",
        addElementButtonCaption: "Add banner",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            description: {
                type: "description",
                caption: "Description"
            }
        },

        newElement: {
            icon: {
                url: ""
            },

            description: {
                text: "Description"
            }
        }
    }
};