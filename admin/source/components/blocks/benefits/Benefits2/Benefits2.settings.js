import { defaultBackgroundColor, defaultTextColor } from "../../default";


export default {
    id: "",

    titleColor: defaultTextColor,
    descriptionColor: defaultTextColor,
    backgroundColor: defaultBackgroundColor,

    banners: [
        {
            image: {
                url: ""
            },
            caption: "Fast development",
            description: {
                text: "Times faster than you are typically used to."
            }
        },

        {
            image: {
                url: ""
            },
            caption: "Cross-platform",
            description: {
                text: "Create Desktop, Cloud and Mobile apps."
            }
        },

        {
            image: {
                url: ""
            },
            caption: "Apps created",
            description: {
                text: "Apps created on 1C:Enterprise easily customizable."
            }
        }
    ]
};