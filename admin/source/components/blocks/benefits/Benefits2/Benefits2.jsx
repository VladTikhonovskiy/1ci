import React from "react";
import classnames from "classnames";
import { shape, string, arrayOf, number } from "prop-types";

import classes from "./Benefits2.less";
import defaultProps from "./Benefits2.settings";


export default class Benefits2 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        banners: arrayOf(shape({
            image: shape({
                url: string
            }),
            caption: string,
            description: shape({
                text: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getBannerStyle(banner, index) {
        let styles = {};

        if (index > 2) {
            styles.marginTop = "32px";
        }

        return styles;
    }

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    getBackgroundIcon(icon) {
        if (icon.url !== "") {
            return {
                backgroundImage: `url(${icon.url})`,
                backgroundColor: "unset"
            };
        }
    }

    get classes() {
        return classnames(classes.benefits2, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderBanner(banner, key) {
        let { descriptionColor, titleColor } = this.props;

        let { image, caption, description } = banner;

        let bannerStyle = this.getBannerStyle(banner, key);
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} style={bannerStyle} className={classes.benefits2Banner}>
                <div
                    style={this.getBackgroundIcon(image)}
                    className={classes.benefits2BannerImage}>
                </div>

                <p className={classes.benefits2BannerCaption} style={titleStyle}>
                    { caption }
                </p>

                <p
                    style={descriptionStyle}
                    className={classes.benefits2BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.benefits2Container}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                </div>
            </div>
        );
    }
}