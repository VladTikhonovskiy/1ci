export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    }
}