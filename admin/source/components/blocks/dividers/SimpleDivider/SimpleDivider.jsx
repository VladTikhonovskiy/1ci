import React from "react";
import classnames from "classnames";
import { string, object } from "prop-types";

import classes from "./SimpleDivider.less";
import defaultProps from "./SimpleDivider.settings";


export default class SimpleDivider extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        className: string
    }

    static defautlProps = defaultProps

    get classes() {
        return classnames(classes.simpleDivider, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <hr className={classes.simpleDividerHr} />
            </div>
        );
    }
}