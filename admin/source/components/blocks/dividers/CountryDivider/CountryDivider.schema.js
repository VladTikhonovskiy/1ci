export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    country: {
        type: "dropdown",
        caption: "Country",
        options: [ "USA", "Romania"]
    }
}