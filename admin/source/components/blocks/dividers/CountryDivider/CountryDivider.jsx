import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, object } from "prop-types";

import { FlagIcon } from "components/common";

import classes from "./CountryDivider.less";
import defaultProps from "./CountryDivider.settings";


export default class CountryDivider extends React.PureComponent {
    static propTypes = {
        id: string,
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        backgroundColor: object,
        titleColor: object,
        country: string,
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle (typeBlock, name) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get classes() {
        return classnames(classes.countryDivider, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    render() {
        let { title, country, titleColor } = this.props;

        let TagName = title.tag;

        let titleStyles = this.getTextColorStyle(titleColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.countryDividerContainer}>
                    <TagName className={classes.countryDividerTitle} style={titleStyles}>
                        <FlagIcon country={country.toLowerCase()} className={classes.countryDividerFlagIcon} />

                        { title.visible && title.text }
                    </TagName>
                </div>
            </div>
        );
    }
}