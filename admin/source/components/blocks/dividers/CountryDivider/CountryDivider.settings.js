import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Limited for use in US",
        visible: true
    },
    country: "USA"
}