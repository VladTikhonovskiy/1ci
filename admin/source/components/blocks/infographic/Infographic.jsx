import React from "react";
import classnames from "classnames";
import { shape, string, arrayOf, number } from "prop-types";

import classes from "./Infographic.less";
import defaultProps from "./Infographic.settings";

const tabletViewMaxWidth = 1199;

export default class Infographic extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        labelColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        infographics: arrayOf(shape({
            label: string,
            percent: string
        })),
        className: string
    }

    static defaultProps = defaultProps

    get labelStyle() {
        let { labelColor: { r, g, b, a } } = this.props;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get classes() {
        return classnames(classes.infographic, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderGraphics(percent, key, label) {
        let coveredGraphicStyle = this.getCoveredGraphicStyle(percent);

        return (
            <div key={key} className={classes.infographicGraphicWrapper}>
                <div className={classes.infographicGraphicMain}>
                    <div className={classes.infographicGraphicCover} style={coveredGraphicStyle}>

                        { percent > 100 && this.getCoveredGraphicPercentText(percent) }
                    </div>

                    <span className={classes.infographicGraphicMainText}> { percent > 0 && "+" } { percent } %</span>
                </div>
                <span className={classes.infographicTitle} style={this.labelStyle}>{ label }</span>
            </div>
        );
    }

    getCoveredGraphicPercentText(percent) {
        return (
            <span className={classes.infographicGraphicCoverText}>
                x{(percent / 100).toFixed(1)}
            </span>
        );
    }

    getCoveredGraphicStyle(percent) {
        let percentFormatted;

        if (percent < -100) {
            percentFormatted = -100;
        } else if (percent > 100){
            percentFormatted = 100;
        } else {
            percentFormatted = percent;
        }

        if (window.innerWidth <= tabletViewMaxWidth) {
            return {
                right: `${percentFormatted < 0 ? 0 : -percentFormatted }px`,
                width: `${Math.abs(percentFormatted)}px`,
            };
        }

        return {
            height: `${Math.abs(percentFormatted)}px`,
            top: `${percentFormatted < 0 ? 0 : -percentFormatted }px`
        };
    }

    render() {
        let { infographics } = this.props;

        return (
            <div className={this.classes} {...this.attributes}>
                <div className={classes.infographicContainer} style={this.style}>
                    {
                        infographics.map((item, index) => {
                            let { percent, label } = item;
                            let key = `${percent}__${index}`;

                            return this.renderGraphics(percent, key, label);
                        })
                    }
                </div>
            </div>
        );
    }
}