import { defaultTextColor } from "../default";


export default {
    id: "",

    backgroundColor: {
        r: 244,
        g: 245,
        b: 247,
        a: 1
    },

    labelColor: defaultTextColor,

    infographics: [
        {
            label: "Some text",

            percent: "10"
        },

        {
            label: "Some text",

            percent: "10"
        },
        
        {
            label: "Some text",

            percent: "10"
        },

        {
            label: "Some text",

            percent: "10"
        },
        
        {
            label: "Some text",

            percent: "10"
        }
    ]
}