export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    labelColor: {
        type: "backgroundColor",
        caption: "Label color"
    },

    infographics: {
        type: "list",
        minCount: 1,
        caption: "Infographic",
        menuCollapse: true,

        element: {
            label: {
                type: "string",
                caption: "Label",
                width: "80"
            },

            percent: {
                type: "number",
                caption: "Percent"
            }
        },

        newElement: {
            label: "Some text",

            percent: "10"
        }
    }
}