import { defaultTextColor, defaultColorWhite } from "../../default";

export default {
    id: "",
    theme: "Light",

    titleColor: defaultColorWhite,
    buttonColor: defaultTextColor,
    descriptionColor: defaultColorWhite,

    backgroundColor: {
        r: 21,
        g: 138,
        b: 255,
        a: 1
    },

    breadcrumbsLinks: [
        {
            button: {
                link: "",
                label: "Home"
            }
        },

        {
            button: {
                link: "",
                label: "Company"
            },
        },

        {
            button: {
                link: "",
                label: "1C:Drive"
            },
        }
    ],

    title: {
        tag: "h2",
        text: "1C:Contabilitate",
        visible: true
    },

    description: {
        text: "Integrated software for professional accountants and business managers that want to stay on top of their company performance."
    },

    button: {
        link: "",
        label: "Request a Demo"
    },

    image: {
        url: ""
    }
};