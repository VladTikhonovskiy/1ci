import { defaultTextColor } from "../../default";

export default {
    id: "",
    theme: "Dark",

    titleColor: defaultTextColor,
    descriptionColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    backgroundColor: {
        r: 244,
        g: 245,
        b: 247,
        a: 1
    },

    breadcrumbsLinks: [
        {
            button: {
                link: "",
                label: "Home"
            }
        },

        {
            button: {
                link: "",
                label: "Company"
            },
        },

        {
            button: {
                link: "",
                label: "1C:Drive"
            },
        }
    ],

    title: {
        tag: "h2",
        text: "Company",
        visible: true
    },

    description: {
        text: "Skyrocket Your Business"
    },

    image: {
        url: ""
    }
};