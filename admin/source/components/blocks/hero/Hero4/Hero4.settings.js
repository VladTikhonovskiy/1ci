import { defaultTextColor, defaultColorWhite } from "../../default";

export default {
    id: "",
    theme: "Light",

    titleColor: defaultColorWhite,
    buttonColor: defaultTextColor,
    descriptionColor: defaultColorWhite,

    backgroundColor: {
        r: 193,
        g: 199,
        b: 208,
        a: 1
    },

    title: {
        tag: "h2",
        text: "1Ci Solutions",
        visible: true
    },

    breadcrumbsLinks: [
        {
            button: {
                link: "",
                label: "Home"
            }
        },

        {
            button: {
                link: "",
                label: "Company"
            },
        },

        {
            button: {
                link: "",
                label: "1C:Drive"
            },
        }
    ],

    description: {
        text: "How a small business can offer big support."
    },

    button: {
        link: "",
        label: "Request a Demo"
    },

    backgroundImage: {
        url: ""
    },

    image: {
        url: ""
    }
};