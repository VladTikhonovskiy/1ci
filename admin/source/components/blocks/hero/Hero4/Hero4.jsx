import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, array } from "prop-types";

import { Button } from "components/controls";
import Breadcrumbs from "components/blocks/common/Breadcrumbs/Breadcrumbs";

import classes from "./Hero4.less";
import defaultProps from "./Hero4.settings";


export default class Hero4 extends React.PureComponent {
    static propTypes = {
        id: string,
        theme: oneOf(["Light", "Dark"]),
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        buttonColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        description: shape({
            text: string,
        }),
        breadcrumbsLinks: array,
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(typeBlock) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get shouldRenderBreadcrumbs() {
        let { breadcrumbsLinks } = this.props;

        return breadcrumbsLinks && breadcrumbsLinks.length > 0;
    }

    get classes() {
        return classnames(classes.hero4, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a }, backgroundImage } = this.props;

        return {
            backgroundImage: `url(${backgroundImage.url})`,
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    render() {
        let { title, description, button, breadcrumbsLinks, theme, image, descriptionColor, titleColor, buttonColor } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);
        let buttonStyle = this.getTextColorStyle(buttonColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                {
                    this.shouldRenderBreadcrumbs &&
                    <Breadcrumbs
                        breadcrumbsLinks={breadcrumbsLinks}
                        theme={theme}
                        className={classes.hero4Breadcrumbs}
                    />
                }

                {
                    title.visible &&
                    <TitleTagName
                        style={titleStyle}
                        className={classes.hero4Title}
                        dangerouslySetInnerHTML={this.getContentHtml(title.text)}>
                    </TitleTagName>
                }

                <p
                    style={descriptionStyle}
                    className={classes.hero4Description}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>

                <Button
                    link={button.link}
                    label={button.label}
                    className={classes.hero4Button}
                    buttonStyles={buttonStyle}
                />

                <img src={image.url} alt="" className={classes.hero4Image} />
            </div>
        );
    }
}