import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, array } from "prop-types";

import { Image } from "components/common";
import { Button } from "components/controls";
import Breadcrumbs from "components/blocks/common/Breadcrumbs/Breadcrumbs";

import classes from "./Hero1.less";
import defaultProps from "./Hero1.settings";


export default class Hero1 extends React.PureComponent {
    static propTypes = {
        id: string,
        theme: oneOf(["Light", "Dark"]),
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        buttonColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        description: shape({
            text: string,
        }),
        button: shape({
            link: string,
            label: string
        }),
        image: shape({
            url: string
        }),
        breadcrumbsLinks: array,
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get shouldRenderBreadcrumbs() {
        let { breadcrumbsLinks } = this.props;

        return breadcrumbsLinks && breadcrumbsLinks.length > 0;
    }

    get classes() {
        return classnames(classes.hero1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    render() {
        let { title, description, button,  theme, image, titleColor, descriptionColor, buttonColor, breadcrumbsLinks } = this.props;
        let { backgroundColor: { r, g, b, a } } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);
        let buttonStyle = this.getTextColorStyle(buttonColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={this.classes} {...this.attributes}>
                <Image name="heroBackground" className={classes.hero1BackgroundImage} style={{fill: `rgba(${r}, ${g}, ${b}, ${a})`}}/>

                <div className={classes.hero1Container}>
                    {
                        this.shouldRenderBreadcrumbs &&
                        <Breadcrumbs breadcrumbsLinks={breadcrumbsLinks} theme={theme} className={classes.hero1Breadcrumbs} />
                    }

                    {
                        title.visible &&
                        <TitleTagName
                            style={titleStyle}
                            className={classes.hero1Title}
                            dangerouslySetInnerHTML={this.getContentHtml(title.text)}>
                        </TitleTagName>
                    }

                    <p
                        style={descriptionStyle}
                        className={classes.hero1Description}
                        dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                    </p>

                    <Button
                        link={button.link}
                        label={button.label}
                        className={classes.hero1Button}
                        buttonStyles={buttonStyle}
                    />

                    <img src={image.url} alt="" className={classes.hero1Image} />
                </div>
            </div>
        );
    }
}