import { defaultTextColor } from "../../default";

export default {
    id: "",
    theme: "Dark",

    titleColor: defaultTextColor,
    buttonColor: defaultTextColor,

    backgroundColor: {
        r: 244,
        g: 245,
        b: 247,
        a: 1
    },

    descriptionColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    breadcrumbsLinks: [
        {
            button: {
                link: "",
                label: "Home"
            }
        },

        {
            button: {
                link: "",
                label: "Company"
            },
        },

        {
            button: {
                link: "",
                label: "1C:Drive"
            },
        }
    ],

    title: {
        tag: "h2",
        text: "1C:Drive",
        visible: true
    },

    description: {
        text: "Complete solution to streamline and manage key processes in SMB, bringing transparency to operations and revealing new opportunities for business growth"
    },

    button: {
        link: "",
        label: "Request a Demo"
    },

    image: {
        url: ""
    }
};