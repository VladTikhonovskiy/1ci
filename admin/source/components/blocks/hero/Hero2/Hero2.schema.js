export default {
    id: {
        type: "string",
        caption: "ID"
    },

    path: {
        type: "string",
        caption: "Breadcrumbs path"
    },

    theme: {
        type: "dropdown",
        caption: "Breadcrumbs theme",
        options: ["Light", "Dark"]
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    breadcrumbsLinks: {
        type: "list",
        minCount: 1,
        menuCollapse: true,
        caption: "Breadcrumbs settings",
        addElementButtonCaption: "Add breadcrumbs",

        element: {
            button: {
                type: "button",
                caption: "Button"
            }
        },

        newElement: {
            button: {
                link: ""
            }
        }
    },

    title: {
        type: "title",
        caption: "Title"
    },

    description: {
        type: "description",
        caption: "Description"
    },

    image: {
        type: "image",
        caption: "Image"
    }
}