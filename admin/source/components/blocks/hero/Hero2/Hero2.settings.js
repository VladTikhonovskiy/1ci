import { defaultColorWhite } from "../../default";

export default {
    id: "",
    theme: "Light",

    titleColor: defaultColorWhite,
    descriptionColor: defaultColorWhite,

    backgroundColor: {
        r: 23,
        g: 43,
        b: 77,
        a: 1
    },

    breadcrumbsLinks: [
        {
            button: {
                link: "",
                label: "Home"
            }
        },

        {
            button: {
                link: "",
                label: "Company"
            },
        },

        {
            button: {
                link: "",
                label: "1C:Drive"
            },
        }
    ],

    title: {
        tag: "h2",
        text: "1C:Enterprise",
        visible: true
    },

    description: {
        text: "A developer platform for fast creation of business automation software. Cloud, Desktop or Mobile. Easily customizable for company’s workflows."
    },

    image: {
        url: ""
    }
};