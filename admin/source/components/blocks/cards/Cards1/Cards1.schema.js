export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    linkColor: {
        type: "backgroundColor",
        caption: "Link color"
    },

    cards: {
        type: "list",
        minCount: 1,
        caption: "Card",

        element: {
            caption: {
                type: "string",
                caption: "Caption"
            },

            description: {
                type: "description",
                caption: "Description"
            },

            link: {
                type: "link",
                caption: "Link"
            }
        },

        newElement: {
            caption: "Caption",
            description: {
                text: "Description"
            },
            link: {
                url: "",
                label: "Link",
            }
        }
    }
}