import React from "react";
import classnames from "classnames";
import { shape, string, arrayOf, object } from "prop-types";

import { Card, Link } from "components/controls";

import classes from "./Cards1.less";
import defaultProps from "./Cards1.settings";


export default class Cards1 extends React.PureComponent {
    static propTypes = {
        id: string,
        cards: arrayOf(shape({
            caption: string,
            description: shape({
                text: string
            }),
            link: shape({
                url: string,
                label: string
            })
        })),
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        linkColor: object,
        className: string
    }

    static defaultProps = defaultProps

    getCardStyle(card, index) {
        let styles = {};

        if (index > 2) {
            styles.marginTop = "32px";
        }

        return styles;
    }

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.cards1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderCard(card, key) {
        let { titleColor, descriptionColor, linkColor } = this.props;
        let { caption, description, link } = card;

        let cardStyle = this.getCardStyle(card, key);
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);
        let linkStyle = this.getTextColorStyle(linkColor, "fill");

        return (
            <Card key={key} style={cardStyle} className={classes.cards1Card}>
                <p className={classes.cards1CardCaption} style={titleStyle}>
                    { caption }
                </p>

                <p
                    style={descriptionStyle}
                    className={classes.cards1CardDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>

                <Link
                    url={link.url}
                    label={link.label}
                    showArrow={true}
                    className={classes.cards1CardLink}
                    linkStyles={linkStyle}
                />
            </Card>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.cards1Container}>
                    { this.props.cards.map((card, index) => this.renderCard(card, index)) }
                </div>
            </div>
        );
    }
}