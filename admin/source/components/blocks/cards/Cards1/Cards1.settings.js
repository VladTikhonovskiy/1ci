import { defaultBackgroundColor, defaultTextColor, defaultLinkColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    linkColor: defaultLinkColor,

    cards: [
        {
            caption: "Knowledge Base",
            description: {
                text: "Access 1C:Enterprise documentation"
            },
            link: {
                url: "",
                label: "Learn more",
            }
        },

        {
            caption: "Partner Chat",
            description: {
                text: "Ask questions and discuss with your fellow developers"
            },
            link: {
                url: "",
                label: "Learn more"
            }
        },

        {
            caption: "Blog",
            description: {
                text: "Stay on top of on the latest 1Ci news and updates"
            },
            link: {
                url: "",
                label: "Learn more"
            }
        }
    ]
}