import React from "react";
import classnames from "classnames";
import { shape, string, arrayOf, object } from "prop-types";

import classes from "./Cards3.less";
import defaultProps from "./Cards3.settings";

import { Card, Link, Button } from "components/controls";


export default class Cards3 extends React.PureComponent {
    static propTypes = {
        id: string,
        cards: arrayOf(shape({
            icon: shape({
                url: string
            }),
            caption: string,
            description: shape({
                text: string
            }),
            button: shape({
                link: string,
                label: string
            }),
            link: shape({
                url: string,
                label: string
            })
        })),
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        linkColor: object,
        buttonColor: object,
        className: string
    }

    static defaultProps = defaultProps

    getCardStyle(card, index) {
        let styles = {};

        if (index > 1) {
            styles.marginTop = "32px";
        }

        return styles;
    }

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.cards3, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderCard(card, key) {
        let { descriptionColor, titleColor, linkColor, buttonColor } = this.props;
        let { icon, caption, description, link, button } = card;

        let cardStyle = this.getCardStyle(card, key);
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);
        let buttonStyle = this.getTextColorStyle(buttonColor);
        let linkStyle = this.getTextColorStyle(linkColor, "fill");

        return (
            <Card key={key} style={cardStyle} className={classes.cards3Card}>
                <div className={classes.cards3CardIcon} style={{backgroundImage: `url(${icon.url})`}}>
                </div>

                <p className={classes.cards3CardCaption} style={titleStyle}>
                    { caption }
                </p>

                <p
                    style={descriptionStyle}
                    className={classes.cards3CardDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>

                <Button
                    type="secondary"
                    link={button.link}
                    label={button.label}
                    className={classes.cards3CardButton}
                    buttonStyles={buttonStyle}
                />

                <Link
                    url={link.url}
                    label={link.label}
                    showArrow={true}
                    className={classes.cards3CardLink}
                    linkStyles={linkStyle}
                />
            </Card>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.cards3Container}>
                    { this.props.cards.map((card, index) => this.renderCard(card, index)) }
                </div>
            </div>
        );
    }
}