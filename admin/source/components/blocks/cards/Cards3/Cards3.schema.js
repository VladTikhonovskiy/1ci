export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    linkColor: {
        type: "backgroundColor",
        caption: "Link color"
    },

    buttonColor: {
        type: "backgroundColor",
        caption: "Button text color"
    },

    cards: {
        type: "list",
        minCount: 1,
        caption: "Card",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            caption: {
                type: "string",
                caption: "Caption"
            },

            description: {
                type: "description",
                caption: "Description"
            },

            button: {
                type: "button",
                caption: "Button"
            },

            link: {
                type: "link",
                caption: "Link"
            }
        },

        newElement: {
            icon: {
                url: ""
            },
            caption: "Caption",
            description: {
                text: "Description"
            },
            button: {
                link: "",
                label: "Request a Demo"
            },
            link: {
                url: "",
                label: "Link",
            }
        }
    }
}