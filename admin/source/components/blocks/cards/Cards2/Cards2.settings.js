import { defaultBackgroundColor, defaultTextColor, defaultLinkColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    linkColor: defaultLinkColor,

    cards: [
        {
            icon: {
                url: ""
            },
            caption: "Small and Medium Business",
            description: {
                text: "Add trasnparency to your business operations - identify bottlenecks and redundancies to keep full control."
            },
            link: {
                url: "",
                label: "Learn more",
            }
        },

        {
            icon: {
                url: ""
            },
            caption: "Accounting Professionals",
            description: {
                text: "Grow your professional value by mastering advanced accounting tools (e.g. inventory, project management, and financial reporting)."
            },
            link: {
                url: "",
                label: "Learn more"
            }
        },

        {
            icon: {
                url: ""
            },
            caption: "Smart Enterprises",
            description: {
                text: "Improve the flow of data across your organization and adapt quickly to the dynamics of digital transformation."
            },
            link: {
                url: "",
                label: "Learn more"
            }
        }
    ]
}