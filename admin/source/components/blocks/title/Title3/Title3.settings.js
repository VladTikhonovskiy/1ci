import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Features",
        visible: true,
        position: "Center"
    },

    description: {
        text: "Create Business Applications that connect company’s business units such as Sales, Purchase, Inventory, Production, HR or Accounting into a secure and customizable system."
    }
}