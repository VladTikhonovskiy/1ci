import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, object } from "prop-types";

import classes from "./Title3.less";
import defaultProps from "./Title3.settings";


export default class Title2 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool,
            position: oneOf(["Left", "Center", "Right"])
        }),
        description: shape({
            text: string,
        }),
        className: string
    }

    static defaultProps = defaultProps

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.title3, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleStyle() {
        let { title: { position }, titleColor: { r, g, b, a } } = this.props;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            textAlign: position
        };
    }

    get descriptionStyle() {
        let { title: { position }, descriptionColor: { r, g, b, a } } = this.props;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            textAlign: position
        };
    }

    render() {
        let { title, description } = this.props;

        let TitleTagName = title.tag;

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                {
                    title.visible &&
                    <TitleTagName
                        style={this.titleStyle}
                        className={classes.title3Title}
                        dangerouslySetInnerHTML={this.getContentHtml(title.text)}>
                    </TitleTagName>
                }

                <p
                    style={this.descriptionStyle}
                    className={classes.title3Description}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }
}