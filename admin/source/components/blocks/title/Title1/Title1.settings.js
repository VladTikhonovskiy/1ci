import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Enterprise-level solution for SMB",
        visible: true,
        position: "Center"
    },

    theme: "Dark"
}