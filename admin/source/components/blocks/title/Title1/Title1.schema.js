export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    theme: {
        type: "dropdown",
        caption: "Theme",
        options: ["Dark", "Light"]
    }
}