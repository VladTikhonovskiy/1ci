import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, object } from "prop-types";

import classes from "./Title1.less";
import defaultProps from "./Title1.settings";


export default class Title1 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        titleColor: object,
        theme: oneOf(["Dark", "Light"]),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool,
            position: oneOf(["Left", "Center", "Right"])
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle (typeBlock, name) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get classes() {
        let { theme, className } = this.props;

        return classnames(classes.title1, className, {
            [classes.title1Dark]: theme === "Dark",
            [classes.title1Light]: theme === "Light"
        });
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleStyle() {
        let { title: { position }, titleColor: { r, g, b, a } } = this.props;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            textAlign: position
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    render() {
        let { title: { tag, visible } } = this.props;

        let TitleTagName = tag;

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                {
                    visible &&
                    <TitleTagName
                        style={this.titleStyle}
                        className={classes.title1Title}
                        dangerouslySetInnerHTML={this.titleHtml}>
                    </TitleTagName>
                }
            </div>
        );
    }
}