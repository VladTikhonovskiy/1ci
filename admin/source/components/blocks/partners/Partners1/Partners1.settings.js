import { range } from "lodash";
import { defaultBackgroundColor, defaultTextColor } from "../../default";


export default {
    id: "",

    backgroundColor: defaultBackgroundColor,
    countOfBlocksInRow: 6,

    titleColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Professional Accountants that trust 1C:Drive",
        visible: true
    },

    banners: range(12).map(() => ({
        image: {
            url: ""
        }
    }))
}