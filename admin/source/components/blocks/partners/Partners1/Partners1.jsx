import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, arrayOf, object, number } from "prop-types";

import classes from "./Partners1.less";
import defaultProps from "./Partners1.settings";


export default class Partners1 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        countOfBlocksInRow: number,
        titleColor: object,
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        banners: arrayOf(shape({
            image: shape({
                url: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle (typeBlock, name) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getBannerStyles(banner, index) {
        let { countOfBlocksInRow } = this.props;

        let styles = {
            backgroundImage: `url(${banner.image.url})`
        };

        if (index > countOfBlocksInRow || index > 5) {
            styles.marginTop = "32px";
        }

        return styles;
    }

    get classes() {
        return classnames(classes.partners1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get styleRowBlocks() {
        let { countOfBlocksInRow } = this.props;

        return {
            width: `${countOfBlocksInRow * 171}px`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    renderBanner(banner, key) {
        let { countOfBlocksInRow } = this.props;

        let styles = this.getBannerStyles(banner, key);

        let partnersBannerClasses = classnames({
            [classes.partners1Banner]: true,
            [classes.partners1BannerSix]: countOfBlocksInRow === 6,
            [classes.partners1BannerFive]: countOfBlocksInRow === 5,
            [classes.partners1BannerFour]: countOfBlocksInRow === 4,
            [classes.partners1BannerThree]: countOfBlocksInRow === 3,
            [classes.partners1BannerTwo]: countOfBlocksInRow === 2
        });

        return (
            <div key={key} style={styles} className={partnersBannerClasses}>
            </div>
        );
    }

    render() {
        let { title, titleColor } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                {
                    title.visible &&
                    <TitleTagName
                        style={titleStyle}
                        className={classes.partners1Title}
                        dangerouslySetInnerHTML={this.titleHtml}>
                    </TitleTagName>
                }

                <div className={classes.partners1Container} style={this.styleRowBlocks}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                </div>
            </div>
        );
    }
}