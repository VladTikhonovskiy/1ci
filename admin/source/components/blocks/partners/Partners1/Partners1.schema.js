export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    countOfBlocksInRow: {
        type: "dropdown",
        caption: "Count of Blocks in Row",
        options: [ 2, 3, 4, 5, 6]
    },

    banners: {
        type: "list",
        minCount: 1,
        caption: "Image",

        element: {
            image: {
                type: "image",
                caption: "Image"
            }
        },

        newElement: {
            image: {
                url: ""
            }
        }
    }
}