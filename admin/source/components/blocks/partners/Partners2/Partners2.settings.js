import { range } from "lodash";
import { defaultBackgroundColor } from "../../default";


export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    countOfBlocksInRow: 6,

    banners: range(12).map(() => ({
        image: {
            url: ""
        }
    }))
}