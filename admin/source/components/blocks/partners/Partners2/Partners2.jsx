import React from "react";
import { bool, shape, string, oneOf, arrayOf, object } from "prop-types";
import classnames from "classnames";

import classes from "./Partners2.less";
import defaultProps from "./Partners2.settings";


export default class Partners2 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        banners: arrayOf(shape({
            image: shape({
                url: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getBannerStyles(banner, index) {
        let { countOfBlocksInRow } = this.props;

        let styles = {
            backgroundImage: `url(${banner.image.url})`
        };

        if (index > countOfBlocksInRow || index > 5) {
            styles.marginTop = "32px";
        }

        return styles;
    }

    get classes() {
        return classnames(classes.partners2, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get styleRowBlocks() {
        let { countOfBlocksInRow } = this.props;

        return {
            width: `${countOfBlocksInRow * 171}px`
        };
    }

    renderBanner(banner, key) {
        let { countOfBlocksInRow } = this.props;

        let styles = this.getBannerStyles(banner, key);

        let partners2BannerClasses = classnames({
            [classes.partners2Banner]: true,
            [classes.partners2BannerSix]: countOfBlocksInRow === 6,
            [classes.partners2BannerFive]: countOfBlocksInRow === 5,
            [classes.partners2BannerFour]: countOfBlocksInRow === 4,
            [classes.partners2BannerThree]: countOfBlocksInRow === 3,
            [classes.partners2BannerTwo]: countOfBlocksInRow === 2
        });

        return (
            <div key={key} style={styles} className={partners2BannerClasses}>
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.partners2Container} style={this.styleRowBlocks}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                </div>
            </div>
        );
    }
}