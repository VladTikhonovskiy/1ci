import React from "react";
import classnames from "classnames";
import { string, object, shape } from "prop-types";

import classes from "./HtmlEditor.less";
import defaultProps from "./HtmlEditor.settings";


export default class HtmlEditor extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        html:shape({
            text: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    get classes() {
        return classnames(classes.htmlEditor, this.props.className, "ql-editor");
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`,
        };
    }

    render() {
        let { html } = this.props;

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.htmlEditorContainer}>

                    <div dangerouslySetInnerHTML={{ __html: html.text }} />
                </div>
            </div>
        );
    }
}