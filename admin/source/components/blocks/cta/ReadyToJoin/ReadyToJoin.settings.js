import { defaultTextColor , defaultColorWhite} from "../../default";

export default {
    id: "",

    backgroundColor: {
        r: 21,
        g: 138,
        b: 255,
        a: 1
    },

    titleColor: defaultColorWhite,

    descriptionColor: defaultColorWhite,

    buttonColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Ready to join AccountingSuite?",
        visible: true
    },

    description: {
        text: "You’ll be redirected to AccountingSuite.com for a free trial. Please note, the service is only available in US"
    },

    firstButton: {
        link: "",
        label: "Free Sign Up"
    },

    secondButton: {
        link: "",
        label: "Learn More"
    },

    theme: "Simple"
};