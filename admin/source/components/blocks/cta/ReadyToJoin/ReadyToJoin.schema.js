export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    buttonColor: {
        type: "backgroundColor",
        caption: "Button text color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    description: {
        type: "description",
        caption: "Description"
    },

    firstButton: {
        type: "button",
        caption: "First Button"
    },

    secondButton: {
        type: "button",
        caption: "Second Button"
    },

    theme: {
        type: "dropdown",
        caption: "Theme",
        options: [ "Simple", "Centered"]
    }
};