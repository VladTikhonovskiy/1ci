export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    buttonColor: {
        type: "backgroundColor",
        caption: "Button text color"
    },

    textbox1: {
        type: "textbox",
        caption: "Textbox №1"
    },

    textbox2: {
        type: "textbox",
        caption: "Textbox №2"
    },

    textbox3: {
        type: "textbox",
        caption: "Textbox №3"
    },

    button: {
        type: "button",
        caption: "Button"
    }
}