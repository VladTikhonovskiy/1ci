import { defaultTextColor, defaultColorWhite } from "../../default";

export default {
    id: "",

    backgroundColor: {
        r: 21,
        g: 138,
        b: 255,
        a: 1
    },

    titleColor: defaultColorWhite,

    buttonColor: defaultTextColor,

    textbox1: {
        label: "First Name",
        placeholder: ""
    },

    textbox2: {
        label: "Last Name",
        placeholder: ""
    },

    textbox3: {
        label: "E-mail",
        placeholder: ""
    },

    button: {
        link: "",
        label: "Download to Get Started"
    }
}