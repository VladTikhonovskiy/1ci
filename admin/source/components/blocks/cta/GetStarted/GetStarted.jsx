import React from "react";
import classnames from "classnames";
import { shape, string, object } from "prop-types";

import { Button, Textbox } from "components/controls";

import classes from "./GetStarted.less";
import defaultProps from "./GetStarted.settings";


export default class GetStarted extends React.PureComponent {
    static propTypes = {
        id: string,
        textbox1: shape({
            label: string,
            placeholder: string
        }),
        textbox2: shape({
            label: string,
            placeholder: string
        }),
        textbox3: shape({
            label: string,
            placeholder: string
        }),
        button: shape({
            link: string,
            label: string
        }),
        backgroundColor: object,
        titleColor: object,
        buttonColor: object,
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle (typeBlock, name) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get classes() {
        return classnames(classes.getStarted, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    render() {
        let { textbox1, textbox2, textbox3, button, titleColor, buttonColor } = this.props;

        let titleStyles = this.getTextColorStyle(titleColor);
        let buttonStyles = this.getTextColorStyle(buttonColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.getStartedContainer}>
                    <Textbox
                        label={textbox1.label}
                        className={classes.getStartedTextbox}
                        labelStyles={titleStyles}
                        placeholder={textbox1.placeholder}
                    />

                    <Textbox
                        label={textbox2.label}
                        className={classes.getStartedTextbox}
                        labelStyles={titleStyles}
                        placeholder={textbox2.placeholder}
                    />

                    <Textbox
                        label={textbox3.label}
                        className={classes.getStartedTextbox}
                        labelStyles={titleStyles}
                        placeholder={textbox3.placeholder}
                    />

                    <Button
                        link={button.link}
                        label={button.label}
                        className={classes.getStartedButton}
                        buttonStyles={buttonStyles}
                    />
                </div>
            </div>
        );
    }
}