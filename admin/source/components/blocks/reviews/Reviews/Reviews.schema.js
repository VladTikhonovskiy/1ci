export default {
    id: {
        type: "string",
        caption: "ID"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    textColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    signatureColor: {
        type: "backgroundColor",
        caption: "Signature color"
    },

    slides: {
        type: "list",
        minCount: 1,
        caption: "Slides",
        menuCollapse: true,
        addElementButtonCaption: "Add slide",

        element: {
            title: {
                type: "title",
                caption: "Title"
            },

            description: {
                type: "string",
                caption: "Description",
                multiLine: true
            },

            signature: {
                type: "string",
                caption: "Signature",
                multiLine: true
            }
        },

        newElement: {
            title: {
                text: "Review title",
                visible: true
            },

            description: "Review text",
            signature: "Signature"
        }
    }
};