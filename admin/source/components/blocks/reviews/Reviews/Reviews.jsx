import React from "react";
import classnames from "classnames";
import { shape, string, number, arrayOf } from "prop-types";

import * as icons from "images/icons";
import { CircleButton } from "components/controls";

import classes from "./Reviews.less";
import defaultProps from "./Reviews.settings";


export default class Reviews extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        textColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        signatureColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        slides: arrayOf(shape({
            title: shape({
                text: string
            }),
            description: string,
            signature: string,
        })),
        className: string
    }

    static defaultProps = defaultProps

    state = {
        activeSlideIndex: 0
    }

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.reviews, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    handlePreviousSlideButtonClick = () => {
        let { slides } = this.props;
        let { activeSlideIndex } = this.state;

        let newActiveSlideIndex = activeSlideIndex - 1;
        if (newActiveSlideIndex < 0) {
            newActiveSlideIndex = slides.length - 1;
        }

        this.setState({
            activeSlideIndex: newActiveSlideIndex
        });
    }

    handleNextSlideButtonClick = () => {
        let { slides } = this.props;
        let { activeSlideIndex } = this.state;

        let newActiveSlideIndex = activeSlideIndex + 1;
        if (newActiveSlideIndex === slides.length) {
            newActiveSlideIndex = 0;
        }

        this.setState({
            activeSlideIndex: newActiveSlideIndex
        });
    }

    getSliderStyle() {
        let { slides } = this.props;
        let { activeSlideIndex } = this.state;

        return {
            width: `${slides.length * 100}%`,
            left: activeSlideIndex === 0 ? "0%" : `${-100 * activeSlideIndex}%`
        }
    }

    renderSlide(slide, index) {
        let { titleColor, textColor, signatureColor } = this.props;

        let titleStyle = this.getTextColorStyle(titleColor);
        let textStyle = this.getTextColorStyle(textColor);
        let signatureStyle = this.getTextColorStyle(signatureColor);

        let signatureHtml = {
            __html: slide.signature.replace(/(?:\r\n|\r|\n)/g, "<br />")
        }

        return (
            <li key={index} className={classes.reviewsSlide}>
                {
                    slide.title.visible &&
                    <p className={classes.reviewsTitle} style={titleStyle}>
                        { slide.title.text }
                    </p>
                }

                <p
                    style={textStyle}
                    className={classes.reviewsText}
                    dangerouslySetInnerHTML={this.getContentHtml(slide.description)}>
                </p>

                <p
                    style={signatureStyle}
                    className={classes.reviewsSignature}
                    dangerouslySetInnerHTML={signatureHtml}>
                </p>
            </li>
        );
    }

    render() {
        let { slides } = this.props;

        let arrowLeftClasses = classnames(classes.reviewsArrow, classes.reviewsArrowLeft);
        let arrowRightClasses = classnames(classes.reviewsArrow, classes.reviewsArrowRight)

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.reviewsContainer}>
                    <img className={classes.reviewsQuotesIcon} src={icons.quotes} alt="" />
                    <img className={classes.reviewsBubblesIcon} src={icons.bubble} alt="" />

                    <CircleButton className={arrowLeftClasses} icon={icons.arrowLeft} onClick={this.handlePreviousSlideButtonClick} />
                    <CircleButton className={arrowRightClasses} icon={icons.arrowRight} onClick={this.handleNextSlideButtonClick} />

                    <div className={classes.reviewsSlideContainer}>
                        <ul className={classes.reviewsSlideWrapper} style={this.getSliderStyle()}>
                            { slides.map((slide, index) => this.renderSlide(slide, index)) }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}