import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    titleColor: defaultTextColor,
    textColor: defaultTextColor,
    signatureColor: defaultTextColor,
    backgroundColor: defaultBackgroundColor,

    slides: [
        {
            title: {
                text: "Partner Success",
                visible: true
            },
            description: "“I was very impressed with the software and how simple it is to make work for your benefit. It is extremely difficult to find an accounting program that a manufacturing company can easily adapt to, but we have found that with AccountingSuite.”",
            signature: "Sidera Pittman Parthenon Envelope Company"
        }
    ]
};