export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    reviews: {
        type: "list",
        minCount: 1,
        caption: "Reviews",

        element: {
            name: {
                type: "string",
                caption: "User name"
            },

            company: {
                type: "string",
                caption: "Company name"
            },

            avatar: {
                type: "image",
                caption: "Avatar"
            },

            text: {
                type: "string",
                caption: "Review text",
                multiLine: true
            }
        },

        newElement: {
            name: "User Name",
            company: "Company Name",
            avatar: {
                url: ""
            },
            text: "Review text"
        }
    }
};