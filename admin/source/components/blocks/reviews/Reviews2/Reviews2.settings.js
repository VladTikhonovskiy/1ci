import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    reviews: [
        {
            name: "Sidera Pittman",
            company: "Company Name",
            avatar: {
                url: ""
            },
            text: "I was very impressed with the software and how simple it is to make work for your benefit. It is extremely difficult to find an accounting program that a manufacturing company can easily adapt to, but we have found that with AccountingSuite."
        },
        {
            name: "Sidera Pittman",
            company: "Company Name",
            avatar: {
                url: ""
            },
            text: "It is extremely difficult to find an accounting program that a manufacturing company can easily adapt to, but we have found that with AccountingSuite."
        },
        {
            name: "Sidera Pittman",
            company: "Company Name",
            avatar: {
                url: ""
            },
            text: "It is extremely difficult to find an accounting program that a manufacturing company can easily adapt to, but we have found that with AccountingSuite."
        }
    ]
};