import React from "react";
import classnames from "classnames";
import { shape, string, arrayOf, object } from "prop-types";

import * as icons from "images/icons";

import classes from "./Reviews2.less";
import defaultProps from "./Reviews2.settings";


export default class Products1 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        reviews: arrayOf(shape({
            name: string,
            company: string,
            avatar: shape({
                url: string
            }),
            text: string
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getReviewQuoteText(reviewText) {
        return `“${reviewText}”`;
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.reviews2, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderReviewHeader({ avatar, name, company }) {
        let { titleColor } = this.props;
        let avatarStyle = {
            backgroundImage: `url(${avatar.url})`
        }

        let titleStyles = this.getTextColorStyle(titleColor);

        return (
            <div className={classes.reviews2ReviewHeader}>
                <div className={classes.reviews2ReviewAvatar} style={avatarStyle}></div>

                <p className={classes.reviews2ReviewCaption} style={titleStyles}>
                    { name }, <br/> { company }
                </p>
            </div>
        );
    }

    renderMainReview() {
        let { descriptionColor } = this.props;
        let [ mainReview ] = this.props.reviews;

        let mainReviewClasses = classnames(classes.reviews2Review, classes.reviews2ReviewHighlighted);
        let bottomQuotesClasses = classnames(classes.reviews2Quotes, classes.reviews2QuotesBottom);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={mainReviewClasses}>
                { this.renderReviewHeader(mainReview) }

                <img className={classes.reviews2Quotes} src={icons.quotes} alt="" />
                <img className={bottomQuotesClasses} src={icons.quotes} alt="" />

                <p
                    style={descriptionStyle}
                    className={classes.reviews2ReviewText}
                    dangerouslySetInnerHTML={this.getContentHtml(this.getReviewQuoteText(mainReview.text))}>
                </p>
            </div>
        );
    }

    renderReviews() {
        let { descriptionColor } = this.props;

        let [ , ...reviews ] = this.props.reviews;

        let descriptionStyles = this.getTextColorStyle(descriptionColor);

        return (
            <div className={classes.reviews2ReviewList}>
                {
                    reviews.map((review, index) => (
                        <div key={index} className={classes.reviews2Review}>

                            { this.renderReviewHeader(review) }

                            <p
                                style={descriptionStyles}
                                className={classes.reviews2ReviewText}
                                dangerouslySetInnerHTML={this.getContentHtml(this.getReviewQuoteText(review.text))}>
                            </p>
                        </div>
                    ))
                }
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.reviews2Container}>
                    { this.renderMainReview() }
                    { this.renderReviews() }
                </div>
            </div>
        );
    }
}