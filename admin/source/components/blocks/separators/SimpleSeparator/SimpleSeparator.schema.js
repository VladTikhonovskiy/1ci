export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    size: {
        type: "dropdown",
        caption: "Size",
        options: ["16", "24", "32", "40", "48", "56"]
    }
};