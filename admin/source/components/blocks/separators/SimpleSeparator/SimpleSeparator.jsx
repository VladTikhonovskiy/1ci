import React from "react";
import classnames from "classnames";
import { string, number, shape } from "prop-types";

import classes from "./SimpleSeparator.less";
import defaultProps from "./SimpleSeparator.settings";


export default class SimpleSeparator extends React.PureComponent {
    static propTypes = {
        id: string,
        size: string,
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        className: string
    }

    static defaultProps = defaultProps

    get classes() {
        return classnames(classes.simpleSeparator, this.props.className);
    }

    get style() {
        let { backgroundColor: { r, g, b, a }, size } = this.props;

        return {
            height: `${size}px`,
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    render() {
        return <div className={this.classes} style={this.style} {...this.attributes}></div>;
    }
}