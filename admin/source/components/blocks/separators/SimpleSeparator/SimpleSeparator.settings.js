import { defaultColorWhite } from "../../default";

export default {
    size: "16",

    backgroundColor: defaultColorWhite,
};