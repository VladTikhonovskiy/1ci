// cta
import GetStartedSchema from "./cta/GetStarted/GetStarted.schema";
import ReadyToJoinSchema from "./cta/ReadyToJoin/ReadyToJoin.schema";

// cards
import Cards1Schema from "./cards/Cards1/Cards1.schema";
import Cards2Schema from "./cards/Cards2/Cards2.schema";
import Cards3Schema from "./cards/Cards3/Cards3.schema";

// link with icon
import LinkWithIconSchema from "./buttonsAndLinks/LinkWithIcon/LinkWithIcon.schema";
import LinkWithButtonSchema from "./buttonsAndLinks/LinkWithButton/LinkWithButton.schema";

// heroes
import Hero1Schema from "./hero/Hero1/Hero1.schema";
import Hero2Schema from "./hero/Hero2/Hero2.schema";
import Hero3Schema from "./hero/Hero3/Hero3.schema";
import Hero4Schema from "./hero/Hero4/Hero4.schema";
import Hero6Schema from "./hero/Hero6/Hero6.schema";

// titles
import Title1Schema from "./title/Title1/Title1.schema";
import Title2Schema from "./title/Title2/Title2.schema";
import Title3Schema from "./title/Title3/Title3.schema";

// common
import BreadcrumbsSchema from "./common/Breadcrumbs/Breadcrumbs.schema";

// benefits
import Benefits1Schema from "./benefits/Benefits1/Benefits1.schema";
import Benefits2Schema from "./benefits/Benefits2/Benefits2.schema";
import Benefits3Schema from "./benefits/Benefits3/Benefits3.schema";
import Benefits4Schema from "./benefits/Benefits4/Benefits4.schema";
import PersonsSchema from "./benefits/Persons/Persons.schema";

// partners
import Partners1Schema from "./partners/Partners1/Partners1.schema";
import Partners2Schema from "./partners/Partners2/Partners2.schema";

// dividers
import SimpleDividerSchema from "./dividers/SimpleDivider/SimpleDivider.schema";
import CountryDividerSchema from "./dividers/CountryDivider/CountryDivider.schema";

// separators
import SimpleSeparatorSchema from "./separators/SimpleSeparator/SimpleSeparator.schema";

// statistics
import Statistics1Schema from "./statistics/Statistics1/Statistics1.schema";

// products
import EnterpriseSchema from "./products/Enterprise/Enterprise.schema";
import Products1Schema from "./products/Products1/Products1.schema";
import Products2Schema from "./products/Products2/Products2.schema";

// HtmlEditor
import HtmlEditorSchema from "./htmlEditor/HtmlEditor.schema";

// features
import Features1Schema from "./features/Features1/Features1.schema";
import BigIcons1Schema from "./features/BigIcons1/BigIcons1.schema";
import BigIcons2Schema from "./features/BigIcons2/BigIcons2.schema";
import BigIcons3Schema from "./features/BigIcons3/BigIcons3.schema";
import ImageListSchema from "./features/ImageList/ImageList.schema";
import ImageList2Schema from "./features/ImageList2/ImageList2.schema";
import ListImageSchema from "./features/ListImage/ListImage.schema";
import ChecklistSchema from "./features/Checklist/Checklist.schema";
import ContactsSchema from "./features/Contacts/Contacts.schema";
import BrochureSchema from "./features/Brochure/Brochure.schema";

// reviews
import ReviewsSchema from "./reviews/Reviews/Reviews.schema";
import Reviews2Schema from "./reviews/Reviews2/Reviews2.schema";

// visual
import MainSchema from "./visual/Main/Main.schema";

// infographic
import InfographicSchema from "./infographic/Infographic.schema";


export default {
    // cta
    GetStarted: GetStartedSchema,
    ReadyToJoin: ReadyToJoinSchema,

    // link with icon
    LinkWithIcon: LinkWithIconSchema,
    LinkWithButton: LinkWithButtonSchema,

    // cards
    Cards1: Cards1Schema,
    Cards2: Cards2Schema,
    Cards3: Cards3Schema,

    // hero
    Hero1: Hero1Schema,
    Hero2: Hero2Schema,
    Hero3: Hero3Schema,
    Hero4: Hero4Schema,
    Hero6: Hero6Schema,

    // titles
    Title1: Title1Schema,
    Title2: Title2Schema,
    Title3: Title3Schema,

    // common
    Breadcrumbs: BreadcrumbsSchema,

    // benefits
    Benefits1: Benefits1Schema,
    Benefits2: Benefits2Schema,
    Benefits3: Benefits3Schema,
    Benefits4: Benefits4Schema,
    Persons: PersonsSchema,

    // partners
    Partners1: Partners1Schema,
    Partners2: Partners2Schema,

    // dividers & separators
    SimpleDivider: SimpleDividerSchema,
    CountryDivider: CountryDividerSchema,
    SimpleSeparator: SimpleSeparatorSchema,

    // statistics
    Statistics1: Statistics1Schema,

    // products
    Enterprise: EnterpriseSchema,
    Products1: Products1Schema,
    Products2: Products2Schema,

    // features
    Features1: Features1Schema,
    BigIcons1: BigIcons1Schema,
    BigIcons2: BigIcons2Schema,
    BigIcons3: BigIcons3Schema,
    ImageList: ImageListSchema,
    ImageList2: ImageList2Schema,
    ListImage: ListImageSchema,
    Checklist: ChecklistSchema,
    Contacts: ContactsSchema,
    Brochure: BrochureSchema,

    // HtmlEditor
    HtmlEditor: HtmlEditorSchema,

    // reviews
    Reviews: ReviewsSchema,
    Reviews2: Reviews2Schema,

    // visual
    Main: MainSchema,

    // infographic
    Infographic: InfographicSchema
};