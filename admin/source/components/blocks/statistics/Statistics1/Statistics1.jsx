import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, arrayOf, object } from "prop-types";

import classes from "./Statistics1.less";
import defaultProps from "./Statistics1.settings";


export default class Statistics1 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        icon: shape({
            url: string
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        item: arrayOf(shape({
            caption: string,
            description: shape({
                text: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.statistics1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderTitleContainer() {
        let { icon, title, titleColor } = this.props;

        let TitleTagName = title.tag;

        let titleStyles = this.getTextColorStyle(titleColor);

        return (
            <div className={classes.statistics1TitleContainer}>
                <div className={classes.statistics1Icon} style={{backgroundImage: `url(${icon.url})`}}></div>

                {
                    title.visible &&
                    <TitleTagName
                        style={titleStyles}
                        className={classes.statistics1Title}
                        dangerouslySetInnerHTML={this.getContentHtml(title.text)}>
                    </TitleTagName>
                }
            </div>
        );
    }

    renderStatisticsContainer() {
        let { items, descriptionColor } = this.props;

        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={classes.statistics1StatisticsContainer}>
                {
                    items.map((item, index) => {
                        return (
                            <div className={classes.statistics1InfoItem} key={index}>
                                <p className={classes.statistics1InfoItemCaption} style={descriptionStyle}>
                                    { item.caption }
                                </p>

                                <p
                                    style={descriptionStyle}
                                    className={classes.statistics1InfoItemDescription}
                                    dangerouslySetInnerHTML={this.getContentHtml(item.description.text)}>
                                </p>
                            </div>
                        );
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.statistics1Container}>
                    { this.renderTitleContainer() }
                    { this.renderStatisticsContainer() }
                </div>
            </div>
        );
    }
}