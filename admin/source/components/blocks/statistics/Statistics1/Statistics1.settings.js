import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    icon: {
        url: ""
    },

    title: {
        tag: "h2",
        text: "1C:Enterprise is deployed in 80+ countries in Asia, the Middle East, Europe, Africa and both Americas.",
        visible: true
    },

    items: [
        {
            caption: "1.5 mln+",
            description: {
                text: "companies use 1C:Enterprise"
            }
        },

        {
            caption: "5 mln+",
            description: {
                text: "users rely on 1C:enterprise applications in their everyday work"
            }
        },

        {
            caption: "6K+",
            description: {
                text: "partners deployed solutions based on 1C:Enterprise"
            }
        },

        {
            caption: "1.5K+",
            description: {
                text: "partners deployed solutions based on 1C:Enterprise"
            }
        },

        {
            caption: "600+",
            description: {
                text: "successful automation projects"
            }
        }
    ]
}