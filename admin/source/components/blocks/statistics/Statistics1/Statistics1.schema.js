export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    icon: {
        type: "image",
        caption: "Icon"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    items: {
        type: "list",
        minCount: 1,
        caption: "Item",

        element: {
            caption: {
                type: "string",
                caption: "Caption"
            },

            description: {
                type: "description",
                caption: "Description"
            }
        },

        newElement: {
            caption: "caption",
            description: {
                text: "description"
            }
        }
    }
}