export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    description: {
        type: "description",
        caption: "Description"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    image: {
        type: "image",
        caption: "Image"
    }
};