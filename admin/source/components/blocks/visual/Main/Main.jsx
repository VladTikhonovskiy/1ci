import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, object } from "prop-types";

import classes from "./Main.less";
import defaultProps from "./Main.settings";


export default class Main extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        titleColor: object,
        description: shape({
            text: string,
        }),
        descriptionColor: object,
        image: shape({
            url: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.main, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    render() {
        let { title, description, image, titleColor, descriptionColor } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>

                <div className={classes.mainContainer}>
                    {
                        title.visible &&
                        <TitleTagName
                            style={titleStyle}
                            className={classes.mainTitle}
                            dangerouslySetInnerHTML={this.getContentHtml(title.text)}>
                        </TitleTagName>
                    }

                    <p
                        style={descriptionStyle}
                        className={classes.mainDescription}
                        dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                    </p>

                    <img src={image.url} alt="" className={classes.mainImage} />
                </div>
            </div>
        );
    }
}