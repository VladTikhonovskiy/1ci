import { defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: {
        r: 244,
        g: 245,
        b: 247,
        a: 1
    },

    title: {
        tag: "h2",
        text: "Skyrocket Your Business",
        visible: true
    },

    titleColor: defaultTextColor,

    description: {
        text: "Customized ERP solutions for companies of all sizes and types of business on the higly flexible, developer-friendly business automation platform."
    },

    descriptionColor: defaultTextColor,

    image: {
        url: ""
    }
};