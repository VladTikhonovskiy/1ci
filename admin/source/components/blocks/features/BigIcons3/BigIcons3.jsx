import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, arrayOf } from "prop-types";

import { Icon } from "components/common";

import classes from "./BigIcons3.less";
import defaultProps from "./BigIcons3.settings";


export default class BigIcons3 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        linkColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        banners: arrayOf(shape({
            icon: shape({
                url: string
            }),
            caption: string,
            description: shape({
                text: string
            })
        })),
        link: shape({
            url: string,
            label: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getBannerStyle(banner, index) {
        let styles = {};

        if (index > 2) {
            styles.marginTop = "24px";
        }

        return styles;
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.bigIcons3, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    renderBannerIcon(icon) {
        let bannerIconStyle = {};

        if (icon.url) {
            bannerIconStyle.backgroundColor = "transparent";
            bannerIconStyle.backgroundImage = `url(${icon.url})`;
        };

        return <div className={classes.bigIcons3BannerIcon} style={bannerIconStyle}></div>;
    }

    renderBanner(banner, key) {
        let { descriptionColor, titleColor } = this.props;
        let { icon, caption, description } = banner;

        let bannerStyle = this.getBannerStyle(banner, key);
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} style={bannerStyle} className={classes.bigIcons3Banner}>
                { this.renderBannerIcon(icon) }

                <p className={classes.bigIcons3BannerCaption} style={titleStyle}>
                    { caption }
                </p>

                <p
                    style={descriptionStyle}
                    className={classes.bigIcons3BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    renderLink() {
        let { link, linkColor } = this.props;

        let linkStyles = this.getTextColorStyle(linkColor, "fill");

        return (
            <div className={classes.bigIcons3LinkContainer}>
                <a href={link.url} className={classes.bigIcons3Link} style={linkStyles}>
                    <Icon name="filePdf" className={classes.bigIcons3LinkIcon}/>
                    { link.label }
                </a>
            </div>
        );
    }

    render() {
        let { title } = this.props;

        let TitleTagName = title.tag;

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                {
                    title.visible &&
                    <TitleTagName
                        className={classes.bigIcons3Title}
                        dangerouslySetInnerHTML={this.titleHtml}>
                    </TitleTagName>
                }

                <div className={classes.bigIcons3Container}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                </div>

                { this.renderLink() }
            </div>
        );
    }
}