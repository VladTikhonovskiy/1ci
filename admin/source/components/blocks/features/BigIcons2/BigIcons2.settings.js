import {defaultBackgroundColor, defaultLinkColor, defaultTextColor} from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    descriptionColor: defaultTextColor,

    linkColor: defaultLinkColor,

    banners: [
        {
            icon: {
                url: ""
            },
            description: {
                text: "One environment for developing Desktop, Mobile and Cloud solutions"
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "100% customizable - set up the workflows according to the business needs"
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Rich library of programming patterns to speed up and standardize development, customization and support"
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Built-in Business Intelligence for creating a wide range of adjustable financial and management reports "
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Access rights management to secure and control data availability in accordance with user roles"
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Integration with third party software or with other 1C:Enterprise-based apps through web-services, ODBC and COM interfaces"
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "Support of various database management systems, including MS SQL, PostgreSQL, IBM DB2, and Oracle DB"
            }
        },
        {
            icon: {
                url: ""
            },
            description: {
                text: "On-premise or managed hosting web-based delivery as well as Web, tablet, or Windows client"
            }
        }
    ],

    link: {
        url: "",
        label: "Learn more about 1C:Enterprise features and architecture"
    }
};