import React from "react";
import classnames from "classnames";
import { shape, string, number, arrayOf } from "prop-types";

import { Link } from "components/controls";

import classes from "./BigIcons2.less";
import defaultProps from "./BigIcons2.settings";


export default class BigIcons2 extends React.PureComponent {
    static propTypes = {
        id: string,
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        linkColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        banners: arrayOf(shape({
            icon: shape({
                url: string
            }),
            description: shape({
                text: string
            })
        })),
        link: shape({
            url: string,
            label: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getBannerStyle(index) {
        let styles = {};

        if (index > 2) {
            styles.marginTop = "24px";
        }

        return styles;
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.bigIcons2, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderBannerIcon(icon) {
        let bannerIconStyle = {};

        if (icon.url) {
            bannerIconStyle.backgroundColor = "transparent";
            bannerIconStyle.backgroundImage = `url(${icon.url})`;
        };

        return <div className={classes.bigIcons2BannerIcon} style={bannerIconStyle}></div>;
    }

    renderBanner(banner, key) {
        let { descriptionColor } = this.props;
        let { icon, description } = banner;

        let bannerStyle = this.getBannerStyle(key);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} style={bannerStyle} className={classes.bigIcons2Banner}>
                { this.renderBannerIcon(icon) }

                <p
                    style={descriptionStyle}
                    className={classes.bigIcons2BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    renderLink() {
        let { link, banners, linkColor } = this.props;

        let styles = this.getBannerStyle(banners.length);

        let linkStyles = this.getTextColorStyle(linkColor, "fill");

        return (
            <div className={classes.bigIcons2Banner} style={styles}>
                <Link
                    url={link.url}
                    label={link.label}
                    showArrow={true}
                    className={classes.bigIcons2Link}
                    linkStyles={linkStyles}
                />
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.bigIcons2Container}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }

                    { this.renderLink() }
                </div>
            </div>
        );
    }
}