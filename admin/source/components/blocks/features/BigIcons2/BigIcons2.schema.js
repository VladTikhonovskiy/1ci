export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    linkColor: {
        type: "backgroundColor",
        caption: "Link color"
    },

    banners: {
        type: "list",
        minCount: 1,
        caption: "Banner",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            description: {
                type: "description",
                caption: "Description"
            }
        },

        newElement: {
            icon: {
                url: ""
            },
            description: {
                text: "Description"
            }
        }
    },

    link: {
        type: "link",
        caption: "Link"
    }
};