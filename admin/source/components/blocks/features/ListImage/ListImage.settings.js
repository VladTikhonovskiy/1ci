import { point } from "images/icons";
import { defaultBackgroundColor, defaultTextColor } from "../../default";


export default {
    id: "",

    titleColor: defaultTextColor,
    backgroundColor: defaultBackgroundColor,
    descriptionColor: defaultTextColor,

    theme: "Left",

    title: {
        tag: "h2",
        text: "Inventory & Light Manufacturing",
        visible: true
    },

    image: {
        url: ""
    },

    points: [
        {
            icon: {
                url: point
            },
            caption: "Estimate materials required for production. Calculate cost of goods."
        },
        {
            icon: {
                url: point
            },
            caption: "Control stock levels and prevent product outages."
        }
    ]
};