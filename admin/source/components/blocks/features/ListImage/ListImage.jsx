import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, arrayOf } from "prop-types";

import classes from "./ListImage.less";
import defaultProps from "./ListImage.settings";


export default class ListImage extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        image: shape({
            url: string
        }),
        points: arrayOf(shape({
            icon: shape({
                url: string
            }),
            caption: string
        })),
        theme: oneOf(["Left", "Right"]),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.listImage, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    renderPoint(point, key) {
        let { descriptionColor } = this.props;

        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} className={classes.listImagePoint} style={descriptionStyle}>
                <img src={point.icon.url} alt="" className={classes.listImagePointIcon} />

                <span dangerouslySetInnerHTML={this.getContentHtml(point.caption)}></span>
            </div>
        );
    }

    renderImage() {
        let { image } = this.props;

        return (
            <div className={classes.listImageImageContainer}>
                <img src={image.url} alt="" className={classes.listImageImage} />
            </div>
        );
    }

    render() {
        let { title, theme, points, titleColor } = this.props;
        let TitleTagName = title.tag;

        let titleStyles = this.getTextColorStyle(titleColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.listImageContainer}>
                    { theme === "Left" && this.renderImage() }

                    <div className={classes.listImageContent}>
                        {
                            title.visible &&
                            <TitleTagName
                                style={titleStyles}
                                className={classes.listImageTitle}
                                dangerouslySetInnerHTML={this.titleHtml}>
                            </TitleTagName>
                        }

                        { points.map((point, index) => this.renderPoint(point, index)) }
                    </div>

                    { theme === "Right" && this.renderImage() }
                </div>
            </div>
        );
    }
}