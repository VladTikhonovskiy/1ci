export default {
    id: {
        type: "string",
        caption: "ID"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    labelColor: {
        type: "backgroundColor",
        caption: "Label color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    contacts: {
        type: "list",
        minCount: 1,
        caption: "Contacts",
        menuCollapse: true,
        addElementButtonCaption: "Add contact",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            title: {
                type: "title",
                caption: "Title"
            },

            addressLabel: {
                type: "title",
                caption: "Address label"
            },

            addressText: {
                type: "string",
                caption: "Address text"
            },

            emailLabel: {
                type: "title",
                caption: "E-mail label"
            },

            emailText: {
                type: "string",
                caption: "E-mail text"
            },

            phoneLabel: {
                type: "title",
                caption: "Phone label"
            },

            phoneText: {
                type: "string",
                caption: "Phone text"
            },

        },

        newElement: {
            icon: {
                url: ""
            },

            title: {
                text: "Contact",
                visible: true
            },

            addressLabel: {
                text: "Address",
                visible: true
            },

            addressText: "Address",

            emailLabel: {
                text: "E-mail",
                visible: true
            },

            emailText: "E-mail",

            phoneLabel: {
                text: "Phone",
                visible: true
            },

            phoneText: "Phone"
        }
    },
};