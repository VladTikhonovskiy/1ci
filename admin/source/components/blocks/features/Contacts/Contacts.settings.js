import { defaultTextColor } from "../../default";


export default {
    id: "",

    titleColor: defaultTextColor,

    backgroundColor: {
        r: 244,
        g: 245,
        b: 247,
        a: 1
    },

    descriptionColor: defaultTextColor,

    labelColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    title: {
        tag: "h2",
        text: "Why 1C:Drive?",
        visible: true
    },

    contacts: [
        {
            icon: {
                url: ""
            },

            title: {
                text: "Moscow",
                visible: true
            },

            addressLabel: {
                text: "Address",
                visible: true
            },

            addressText: "Moscow, Dmitrovskoe sh., d.9, str.B",

            emailLabel: {
                text: "E-mail",
                visible: true
            },

            emailText: "int@1c.com",

            phoneLabel: {
                text: "Phone",
                visible: true
            },

            phoneText: "+7 (499) 976 48 88"
        },

        {
            icon: {
                url: ""
            },

            title: {
                text: "San Francisco",
                visible: true
            },

            addressLabel: {
                text: "Address",
                visible: true
            },

            addressText: "600 California St, San Francisco, CA 94108",

            emailLabel: {
                text: "E-mail",
                visible: true
            },

            emailText: "",

            phoneLabel: {
                text: "Phone",
                visible: true
            },

            phoneText: ""
        }
    ]
};