import React from "react";
import classnames from "classnames";
import { shape, string, arrayOf, oneOf, bool, number } from "prop-types";

import classes from "./Contacts.less";
import defaultProps from "./Contacts.settings";


export default class Contacts extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        labelColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        contacts: arrayOf(shape({
            icon: shape({
                url: string
            }),
            title: shape({
                text: string
            }),
            addressLabel: shape({
                text: string
            }),
            addressText: string,
            emailLabel: shape({
                text: string
            }),
            emailText: string,
            phoneLabel: shape({
                text: string
            }),
            phoneText: string
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get classes() {
        return classnames(classes.contacts, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderContactIcon(icon) {
        let style = {};

        if (icon.url) {
            style.backgroundColor = "transparent";
            style.backgroundImage = `url(${icon.url})`;
        }

        return <div className={classes.contactsItemIcon} style={style}></div>;
    }

    renderContactDetails(contact) {
        let {
            addressLabel, addressText,
            emailLabel, emailText,
            phoneLabel, phoneText
        } = contact;

        return (
            <div className={classes.contactsItemDetails}>
                { this.renderContactDetailsLine(addressLabel, addressText) }
                { this.renderContactDetailsLine(emailLabel, emailText) }
                { this.renderContactDetailsLine(phoneLabel, phoneText) }
            </div>
        );
    }

    renderContactDetailsLine(label, value) {
        let { labelColor, descriptionColor } = this.props;

        let labelStyle = this.getTextColorStyle(labelColor);
        let valueStyle = this.getTextColorStyle(descriptionColor);

        if (label.visible === false || value.trim() === "")
            return null;

        return (
            <div className={classes.contactsItemDetailsLine}>
                <p className={classes.contactsItemDetailsLabel} style={labelStyle}>
                    { label.text }:
                </p>

                <p className={classes.contactsItemDetailsValue} style={valueStyle}>
                    { value }
                </p>
            </div>
        );
    }

    renderContact(contact, key) {
        let { titleColor } = this.props;
        let { icon, title } = contact;

        let contactTitleStyle = this.getTextColorStyle(titleColor);

        return (
            <div key={key} className={classes.contactsItem}>
                { this.renderContactIcon(icon) }

                {
                    title.visible &&
                    <p className={classes.contactsItemTitle} style={contactTitleStyle}>
                        { title.text }
                    </p>
                }

                { this.renderContactDetails(contact) }
            </div>
        );
    }

    render() {
        let { title, titleColor, contacts } = this.props;

        let TitleTagName = title.tag;
        let titleStyle = this.getTextColorStyle(titleColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.contactsContainer}>
                    {
                        title.visible &&
                        <TitleTagName style={titleStyle} className={classes.contactsTitle}>
                            { title.text }
                        </TitleTagName>
                    }

                    <div className={classes.contactsListContainer}>
                        { contacts.map((contact, index) => this.renderContact(contact, index)) }
                    </div>
                </div>
            </div>
        );
    }
}