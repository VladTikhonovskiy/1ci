import { point } from "images/icons";
import { defaultBackgroundColor, defaultTextColor } from "../../default";


export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Automate Key Workflows",
        visible: true
    },

    image: {
        url: ""
    },

    points: [
        {
            icon: {
                url: point
            },
            caption: "Support for all common OS and Database formats - Linux, Windows, Mac OS, PostgreSQL, MS SQL Server, IBM DB2, Oracle Database"
        },
        {
            icon: {
                url: point
            },
            caption: "Production management for multi-stage production cycles"
        },
        {
            icon: {
                url: point
            },
            caption: "Repair and maintenance management"
        },
        {
            icon: {
                url: point
            },
            caption: "Stock and warehouse management"
        },
        {
            icon: {
                url: point
            },
            caption: "Sales and purchases"
        },
        {
            icon: {
                url: point
            },
            caption: "Customer relationship management"
        },
        {
            icon: {
                url: point
            },
            caption: "HR and payroll management"
        },
        {
            icon: {
                url: point
            },
            caption: "Mobile workplaces and remote work via the Internet"
        },
    ]
};