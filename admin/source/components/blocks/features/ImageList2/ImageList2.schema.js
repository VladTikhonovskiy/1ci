import { point } from "images/icons";


export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    image: {
        type: "image",
        caption: "Image"
    },

    points: {
        type: "list",
        minCount: 1,
        caption: "Points",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            caption: {
                type: "string",
                caption: "Caption",
                multiLine: true
            }
        },

        newElement: {
            icon: {
                url: point
            },
            caption: "Caption"
        }
    }
};