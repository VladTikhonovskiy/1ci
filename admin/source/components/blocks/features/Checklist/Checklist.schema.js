import { point } from "images/icons";


export default {
    id: {
        type: "string",
        caption: "ID"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    points: {
        type: "list",
        minCount: 1,
        caption: "Points",
        menuCollapse: true,
        addElementButtonCaption: "Add point",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            title: {
                type: "string",
                caption: "Title"
            },

            description: {
                type: "description",
                caption: "Description"
            }
        },

        newElement: {
            icon: {
                url: point
            },

            title: "Title",

            description: {
                text: "Description"
            }
        }
    }
};