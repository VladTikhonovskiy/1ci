import React from "react";
import classnames from "classnames";
import { string, number, shape, arrayOf } from "prop-types";

import classes from "./Checklist.less";
import defaultProps from "./Checklist.settings";


export default class Checklist extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        points: arrayOf(shape({
            icon: shape({
                url: string
            }),
            title: string,
            description: shape({
                text: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.checklist, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderPoint(point, key) {
        let { title, description } = point;
        let { titleColor, descriptionColor } = this.props;

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} className={classes.checklistPoint} style={descriptionStyle}>
                <img src={point.icon.url} alt="" className={classes.checklistPointIcon} />

                <div>
                    <p className={classes.checklistTitle} style={titleStyle}>
                        { title }
                    </p>

                    <p
                        className={classes.checklistDescription}
                        dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                    </p>
                </div>
            </div>
        );
    }

    render() {
        let { points } = this.props;

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.checklistContainer}>
                    { points.map((point, index) => this.renderPoint(point, index)) }
                </div>
            </div>
        );
    }
}