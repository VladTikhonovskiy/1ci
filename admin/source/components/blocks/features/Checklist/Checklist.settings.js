import { point } from "images/icons";
import { defaultBackgroundColor, defaultTextColor } from "../../default";


export default {
    id: "",

    titleColor: defaultTextColor,
    backgroundColor: defaultBackgroundColor,

    descriptionColor: {
        r: 108,
        g: 121,
        b: 143,
        a: 1
    },

    points: [
        {
            icon: {
                url: point
            },

            title: "Header",

            description: {
                text: "Move away from using a bunch of individual products that don’t talk to each other to a complete solution that automates the whole range of company’s processes"
            }
        },
        {
            icon: {
                url: point
            },

            title: "Header",

            description: {
                text: "Transparent business processes allow to grow your company faster and stay competitive knowing how your company operates in the smallest detail"
            }
        },
        {
            icon: {
                url: point
            },

            title: "Header",

            description: {
                text: "Stay on top of your business KPIs, estimate operational efficiency, individual departments and employee performance to achieve financial goals"
            }
        },
        {
            icon: {
                url: point
            },

            title: "Header",

            description: {
                text: "Take informed decisions with critical data available by your hand and in real time"
            }
        }
    ]
};