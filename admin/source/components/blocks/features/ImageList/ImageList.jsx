import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, arrayOf } from "prop-types";

import classes from "./ImageList.less";
import defaultProps from "./ImageList.settings";


export default class BigIcons1 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        image: shape({
            url: string
        }),
        points: arrayOf(shape({
            icon: shape({
                url: string
            }),
            caption: string
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.imageList, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    renderPoint(point, key) {
        let { descriptionColor } = this.props;

        let descriptionStyles = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} className={classes.imageListPoint} style={descriptionStyles}>
                <img src={point.icon.url} alt="" className={classes.imageListPointIcon} />

                <span dangerouslySetInnerHTML={this.getContentHtml(point.caption)}></span>
            </div>
        );
    }

    render() {
        let { title, image, points, titleColor } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.imageListContainer}>
                    <div className={classes.imageListImageContainer}>
                        <img src={image.url} alt="" className={classes.imageListImage} />
                    </div>

                    <div className={classes.imageListContent}>
                        {
                            title.visible &&
                            <TitleTagName
                                style={titleStyle}
                                className={classes.imageListTitle}
                                dangerouslySetInnerHTML={this.titleHtml}>
                            </TitleTagName>
                        }

                        { points.map((point, index) => this.renderPoint(point, index)) }
                    </div>
                </div>
            </div>
        );
    }
}