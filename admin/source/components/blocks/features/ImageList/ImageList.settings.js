import { point } from "images/icons";
import { defaultBackgroundColor, defaultTextColor } from "../../default";


export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Sales",
        visible: true
    },

    image: {
        url: ""
    },

    points: [
        {
            icon: {
                url: point
            },
            caption: "Close deals faster and stay updated on customer orders status."
        },
        {
            icon: {
                url: point
            },
            caption: "Speed-up order-to-cash process and stay on top of your pipeline."
        },
        {
            icon: {
                url: point
            },
            caption: "Monitor your right-now cash position. Direct integration with cash registers."
        }
    ]
};