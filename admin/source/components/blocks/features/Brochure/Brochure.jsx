import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, number } from "prop-types";

import { Button } from "components/controls";

import classes from "./Brochure.less";
import defaultProps from "./Brochure.settings";


export default class Brochure extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        overheadColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        buttonColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        overhead: shape({
            text: string,
            visible: bool
        }),
        description: shape({
            text: string
        }),
        button: shape({
            link: string,
            label: string
        }),
        image: shape({
            url: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.brochure, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    render() {
        let {
            title, overhead, description, button, image,
            titleColor, descriptionColor, buttonColor, overheadColor
        } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);
        let overheadStyle = this.getTextColorStyle(overheadColor);
        let buttonStyle = this.getTextColorStyle(buttonColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <img src={image.url} alt="" className={classes.brochureImage} />

                <div className={classes.brochureContainer}>
                    {
                        overhead.visible &&
                        <p className={classes.brochureOverhead} style={overheadStyle}>
                            { overhead.text }
                        </p>
                    }

                    {
                        title.visible &&
                        <TitleTagName style={titleStyle} className={classes.brochureTitle}>
                            { title.text }
                        </TitleTagName>
                    }

                    <p
                        style={descriptionStyle}
                        className={classes.brochureDescription}
                        dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                    </p>

                    <Button
                        link={button.link}
                        label={button.label}
                        className={classes.brochureButton}
                        buttonStyles={buttonStyle}
                    />
                </div>
            </div>
        );
    }
}