import { defaultTextColor, defaultColorWhite } from "../../default";

export default {
    id: "",

    titleColor: defaultTextColor,
    overheadColor: defaultTextColor,
    descriptionColor: defaultTextColor,
    backgroundColor: defaultColorWhite,
    buttonColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Exceptional Online Accounting",
        visible: true
    },

    overhead: {
        text: "Brochure",
        visible: true
    },

    description: {
        text: "Download the product’s datasheet to learn how AccountingSuite empowers accounting professionals and their clientswith a robust tool to increase their firm’s value and bring transparency to business operations."
    },

    button: {
        link: "",
        label: "Download Brochure"
    },

    image: {
        url: ""
    }
};