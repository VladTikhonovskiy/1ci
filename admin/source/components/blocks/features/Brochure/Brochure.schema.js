export default {
    id: {
        type: "string",
        caption: "ID"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    overheadColor: {
        type: "backgroundColor",
        caption: "Overhead color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    buttonColor: {
        type: "backgroundColor",
        caption: "Button text color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    overhead: {
        type: "title",
        caption: "Overhead"
    },

    description: {
        type: "description",
        caption: "Description"
    },

    button: {
        type: "button",
        caption: "Button"
    },

    image: {
        type: "image",
        caption: "Image"
    }
};