export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    banners: {
        type: "list",
        minCount: 1,
        caption: "Banner",

        element: {
            icon: {
                type: "image",
                caption: "Icon"
            },

            caption: {
                type: "string",
                caption: "Caption"
            },

            description: {
                type: "description",
                caption: "Description"
            }
        },

        newElement: {
            icon: {
                url: ""
            },
            caption: "Caption",
            description: {
                text: "Description"
            }
        }
    }
};