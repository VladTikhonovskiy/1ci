import React from "react";
import classnames from "classnames";
import { shape, string, number, arrayOf } from "prop-types";

import classes from "./Features1.less";
import defaultProps from "./Features1.settings";


export default class Features1 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        banners: arrayOf(shape({
            icon: shape({
                url: string
            }),
            caption: string,
            description: shape({
                text: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(typeBlock) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getBannerStyle(banner, index) {
        let styles = {};

        if (index > 2) {
            styles.marginTop = "24px";
        }

        return styles;
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.features1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderBannerIcon(icon) {
        let bannerIconStyle = {};

        if (icon.url) {
            bannerIconStyle.backgroundColor = "transparent";
            bannerIconStyle.backgroundImage = `url(${icon.url})`;
        };

        return <div className={classes.features1BannerIcon} style={bannerIconStyle}></div>;
    }

    renderBanner(banner, key) {
        let { titleColor, descriptionColor } = this.props;

        let { icon, caption, description } = banner;

        let bannerStyle = this.getBannerStyle(banner, key);
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} style={bannerStyle} className={classes.features1Banner}>
                { this.renderBannerIcon(icon) }

                <p className={classes.features1BannerCaption} style={titleStyle}>
                    { caption }
                </p>

                <p
                    style={descriptionStyle}
                    className={classes.features1BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.features1Container}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                </div>
            </div>
        );
    }
}