import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    banners: [
        {
            icon: {
                url: ""
            },
            caption: "Take informed decisions",
            description: {
                text: "Monitor and retrieve data on your company performance for taking business decisions."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Forget about tons of spreadsheets",
            description: {
                text: "Consolidate the detailed customer, order, stock and sales information in one straightforward system."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Adapt to your requirements",
            description: {
                text: "Easily customize the software instead of adapting your unique processes to it."
            }
        }
    ]
};