import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf, arrayOf } from "prop-types";

import classes from "./BigIcons1.less";
import defaultProps from "./BigIcons1.settings";


export default class BigIcons1 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        banners: arrayOf(shape({
            icon: shape({
                url: string
            }),
            caption: string,
            description: shape({
                text: string
            })
        })),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(typeBlock) {
        let { r, g, b, a } = typeBlock;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getBannerStyle(banner, index) {
        let styles = {};

        if (index > 2) {
            styles.marginTop = "24px";
        }

        return styles;
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.bigIcons1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    renderBannerIcon(icon) {
        let bannerIconStyle = {};

        if (icon.url) {
            bannerIconStyle.backgroundColor = "transparent";
            bannerIconStyle.backgroundImage = `url(${icon.url})`;
        };

        return <div className={classes.bigIcons1BannerIcon} style={bannerIconStyle}></div>;
    }

    renderBanner(banner, key) {
        let { titleColor, descriptionColor } = this.props;
        let { icon, caption, description } = banner;

        let styles = this.getBannerStyle(banner, key);

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div key={key} style={styles} className={classes.bigIcons1Banner}>
                { this.renderBannerIcon(icon) }

                <p className={classes.bigIcons1BannerCaption} style={titleStyle}>
                    { caption }
                </p>

                <p
                    style={descriptionStyle}
                    className={classes.bigIcons1BannerDescription}
                    dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                </p>
            </div>
        );
    }

    render() {
        let { title, titleColor } = this.props;

        let TitleTagName = title.tag;

        let titleStyle = this.getTextColorStyle(titleColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                {
                    title.visible &&
                    <TitleTagName
                        style={titleStyle}
                        className={classes.bigIcons1Title}
                        dangerouslySetInnerHTML={this.titleHtml}>
                    </TitleTagName>
                }

                <div className={classes.bigIcons1Container}>
                    { this.props.banners.map((banner, index) => this.renderBanner(banner, index)) }
                </div>
            </div>
        );
    }
}