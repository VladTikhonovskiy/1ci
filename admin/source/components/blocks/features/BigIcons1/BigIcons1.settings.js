import { defaultBackgroundColor, defaultTextColor } from "../../default";

export default {
    id: "",

    backgroundColor: defaultBackgroundColor,

    titleColor: defaultTextColor,

    descriptionColor: defaultTextColor,

    title: {
        tag: "h2",
        text: "Understand how successful your business",
        visible: true
    },

    banners: [
        {
            icon: {
                url: ""
            },
            caption: "Financial documents",
            description: {
                text: "Automatically register each financial operation and create corresponding documents."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Stock Management",
            description: {
                text: "Real-time inventory management. Set up alerts when stock limits are close to the predefined values."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Production",
            description: {
                text: "Calculate costs and create specifications. Estimate the set of components and materials required for customer order fulfillment."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Human resources",
            description: {
                text: "Manage personnel and payroll centrally and in one place."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Hardware Integration",
            description: {
                text: "Seamlessly connect with sales equipment. Direct integration with cash registers, tax printers, barcode readers and more."
            }
        },
        {
            icon: {
                url: ""
            },
            caption: "Tax Declarations",
            description: {
                text: "Automatically generate tax declarations and send them directly to your tax office, minimizing manual effort and potential human-related errors."
            }
        }
    ]
};