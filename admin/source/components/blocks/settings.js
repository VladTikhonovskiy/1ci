// cta
import GetStartedSettings from "./cta/GetStarted/GetStarted.settings";
import ReadyToJoinSettings from "./cta/ReadyToJoin/ReadyToJoin.settings";

// cards
import Cards1Settings from "./cards/Cards1/Cards1.settings";
import Cards2Settings from "./cards/Cards2/Cards2.settings";
import Cards3Settings from "./cards/Cards3/Cards3.settings";

// link with icon
import LinkWithIconSettings from "./buttonsAndLinks/LinkWithIcon/LinkWithIcon.settings";
import LinkWithButtonSettings from "./buttonsAndLinks/LinkWithButton/LinkWithButton.settings";

// heroes
import Hero1Settings from "./hero/Hero1/Hero1.settings";
import Hero2Settings from "./hero/Hero2/Hero2.settings";
import Hero3Settings from "./hero/Hero3/Hero3.settings";
import Hero4Settings from "./hero/Hero4/Hero4.settings";
import Hero6Settings from "./hero/Hero6/Hero6.settings";

// titles
import Title1Settings from "./title/Title1/Title1.settings";
import Title2Settings from "./title/Title2/Title2.settings";
import Title3Settings from "./title/Title3/Title3.settings";

// common
import BreadcrumbsSettings from "./common/Breadcrumbs/Breadcrumbs.settings";

// benefits
import Benefits1Settings from "./benefits/Benefits1/Benefits1.settings";
import Benefits2Settings from "./benefits/Benefits2/Benefits2.settings";
import Benefits3Settings from "./benefits/Benefits3/Benefits3.settings";
import Benefits4Settings from "./benefits/Benefits4/Benefits4.settings";
import PersonsSettings from "./benefits/Persons/Persons.settings";

// partners
import Partners1Settings from "./partners/Partners1/Partners1.settings";
import Partners2Settings from "./partners/Partners2/Partners2.settings";

// dividers
import SimpleDividerSettings from "./dividers/SimpleDivider/SimpleDivider.settings";
import CountryDividerSettings from "./dividers/CountryDivider/CountryDivider.settings";

// separators
import SimpleSeparatorSettings from "./separators/SimpleSeparator/SimpleSeparator.settings";

// statistics
import Statistics1Settings from "./statistics/Statistics1/Statistics1.settings";

// products
import EnterpriseSettings from "./products/Enterprise/Enterprise.settings";
import Products1Settings from "./products/Products1/Products1.settings";
import Products2Settings from "./products/Products2/Products2.settings";

// HtmlEditor
import HtmlEditorSettings from "./htmlEditor/HtmlEditor.settings";

// features
import Features1Settings from "./features/Features1/Features1.settings";
import BigIcons1Settings from "./features/BigIcons1/BigIcons1.settings";
import BigIcons2Settings from "./features/BigIcons2/BigIcons2.settings";
import BigIcons3Settings from "./features/BigIcons3/BigIcons3.settings";
import ImageListSettings from "./features/ImageList/ImageList.settings";
import ImageList2Settings from "./features/ImageList2/ImageList2.settings";
import ListImageSettings from "./features/ListImage/ListImage.settings";
import ChecklistSettings from "./features/Checklist/Checklist.settings";
import ContactsSettings from "./features/Contacts/Contacts.settings";
import BrochureSettings from "./features/Brochure/Brochure.settings";

// reviews
import ReviewsSettings from "./reviews/Reviews/Reviews.settings";
import Reviews2Settings from "./reviews/Reviews2/Reviews2.settings";

// visual
import MainSettings from "./visual/Main/Main.settings";

// infographic
import InfographicSettings from "./infographic/Infographic.settings";


export default {
    // cta
    GetStarted: GetStartedSettings,
    ReadyToJoin: ReadyToJoinSettings,

    // link with icon
    LinkWithIcon: LinkWithIconSettings,
    LinkWithButton: LinkWithButtonSettings,

    // cards
    Cards1: Cards1Settings,
    Cards2: Cards2Settings,
    Cards3: Cards3Settings,

    // hero
    Hero1: Hero1Settings,
    Hero2: Hero2Settings,
    Hero3: Hero3Settings,
    Hero4: Hero4Settings,
    Hero6: Hero6Settings,

    // titles
    Title1: Title1Settings,
    Title2: Title2Settings,
    Title3: Title3Settings,

    // common
    Breadcrumbs: BreadcrumbsSettings,

    // benefits
    Benefits1: Benefits1Settings,
    Benefits2: Benefits2Settings,
    Benefits3: Benefits3Settings,
    Benefits4: Benefits4Settings,
    Persons: PersonsSettings,

    // partners
    Partners1: Partners1Settings,
    Partners2: Partners2Settings,

    // dividers & separators
    SimpleDivider: SimpleDividerSettings,
    CountryDivider: CountryDividerSettings,
    SimpleSeparator: SimpleSeparatorSettings,

    // statistics
    Statistics1: Statistics1Settings,

    // products
    Enterprise: EnterpriseSettings,
    Products1: Products1Settings,
    Products2: Products2Settings,

    // features
    Features1: Features1Settings,
    BigIcons1: BigIcons1Settings,
    BigIcons2: BigIcons2Settings,
    BigIcons3: BigIcons3Settings,
    ImageList: ImageListSettings,
    ImageList2: ImageList2Settings,
    ListImage: ListImageSettings,
    Checklist: ChecklistSettings,
    Contacts: ContactsSettings,
    Brochure: BrochureSettings,

    // HtmlEditor
    HtmlEditor: HtmlEditorSettings,

    // reviews
    Reviews: ReviewsSettings,
    Reviews2: Reviews2Settings,

    // visual
    Main: MainSettings,

    // infographic
    Infographic: InfographicSettings
};