import React from "react";
import classnames from "classnames";
import { bool, shape, string, number, oneOf } from "prop-types";

import { Link } from "components/controls";

import classes from "./Products2.less";
import defaultProps from "./Products2.settings";


export default class Products1 extends React.PureComponent {
    static propTypes = {
        id: string,
        titleColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        descriptionColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        linkColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        backgroundColor: shape({
            r: number,
            g: number,
            b: number,
            a: number
        }),
        leftBannerBackgroundImage: shape({
            url: string
        }),
        leftBannerTitle: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        leftBannerDescription: shape({
            text: string,
        }),
        leftBannerLink: shape({
            url: string,
            label: string
        }),
        rightBannerBackgroundImage: shape({
            url: string
        }),
        rightBannerTitle: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        rightBannerDescription: shape({
            text: string,
        }),
        rightBannerLink: shape({
            url: string,
            label: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.products2, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    renderBanner(backgroundImage, title, description, link, className) {
        let { titleColor, descriptionColor, linkColor } = this.props;

        let TitleTagName = title.tag;
        let bannerClasses = classnames(classes.products2Banner, className);

        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);
        let linkStyle = this.getTextColorStyle(linkColor, "fill");

        return (
            <div className={classes.products2BannerContainer} style={{backgroundImage: `url(${backgroundImage.url})`}}>
                <div className={bannerClasses}>
                    {
                        title.visible &&
                        <TitleTagName className={classes.products2BannerTitle} style={titleStyle}>
                            { title.text }
                        </TitleTagName>
                    }

                    <p
                        style={descriptionStyle}
                        className={classes.products2BannerDescription}
                        dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                    </p>

                    <Link
                        url={link.url}
                        label={link.label}
                        theme="white"
                        showArrow={true}
                        className={classes.products2BannerLink}
                        linkStyles={linkStyle}
                    />
                </div>
            </div>
        );
    }

    renderLeftBanner() {
        let {
            leftBannerBackgroundImage,
            leftBannerTitle,
            leftBannerDescription,
            leftBannerLink
        } = this.props;

        return this.renderBanner(
            leftBannerBackgroundImage,
            leftBannerTitle,
            leftBannerDescription,
            leftBannerLink,
            classes.products2BannerLeft
        );
    }

    renderRightBanner() {
        let {
            rightBannerBackgroundImage,
            rightBannerTitle,
            rightBannerDescription,
            rightBannerLink
        } = this.props;

        return this.renderBanner(
            rightBannerBackgroundImage,
            rightBannerTitle,
            rightBannerDescription,
            rightBannerLink,
            classes.products2BannerRight
        );
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.products2Container}>
                    { this.renderLeftBanner() }
                    { this.renderRightBanner() }
                </div>
            </div>
        );
    }
}