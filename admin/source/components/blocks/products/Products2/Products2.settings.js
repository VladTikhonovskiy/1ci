import { defaultBackgroundColor, defaultColorWhite } from "../../default";

export default {
    id: "",

    titleColor: defaultColorWhite,
    descriptionColor: defaultColorWhite,
    linkColor: defaultColorWhite,
    backgroundColor: defaultBackgroundColor,

    leftBannerBackgroundImage: {
        url: ""
    },

    leftBannerTitle: {
        tag: "h2",
        text: "1C:Enterprise",
        visible: true
    },

    leftBannerDescription: {
        text: "Highly adaptive and configurable development platform for streamlining corporate processes such as financial operations, accounting, production management, sales management, corporate services and more."
    },

    leftBannerLink: {
        url: "",
        label: "Learn more"
    },

    rightBannerBackgroundImage: {
        url: ""
    },

    rightBannerTitle: {
        tag: "h2",
        text: "1C:ERP 2",
        visible: true
    },

    rightBannerDescription: {
        text: "Innovative application for building corporate management systems based on the best global practices in automating large and medium-sized businesses."
    },

    rightBannerLink: {
        url: "",
        label: "Learn more"
    }
};