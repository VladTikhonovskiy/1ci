export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },

    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    linkColor: {
        type: "backgroundColor",
        caption: "Link color"
    },

    leftBannerBackgroundImage: {
        type: "image",
        caption: "Left banner background image"
    },

    leftBannerTitle: {
        type: "title",
        caption: "Left banner title"
    },

    leftBannerDescription: {
        type: "description",
        caption: "Left banner description"
    },

    leftBannerLink: {
        type: "link",
        caption: "Left banner link"
    },

    rightBannerBackgroundImage: {
        type: "image",
        caption: "Right banner background image"
    },

    rightBannerTitle: {
        type: "title",
        caption: "Right banner title"
    },

    rightBannerDescription: {
        type: "description",
        caption: "Right banner description"
    },

    rightBannerLink: {
        type: "link",
        caption: "Right banner link"
    }
};