export default {
    id: {
        type: "string",
        caption: "ID"
    },

    backgroundColor: {
        type: "backgroundColor",
        caption: "Background color"
    },
    titleColor: {
        type: "backgroundColor",
        caption: "Title color"
    },

    descriptionColor: {
        type: "backgroundColor",
        caption: "Description color"
    },

    linkColor: {
        type: "backgroundColor",
        caption: "Link color"
    },

    title: {
        type: "title",
        caption: "Title"
    },

    description: {
        type: "description",
        caption: "Description"
    },

    link: {
        type: "link",
        caption: "Link"
    },

    backgroundImage: {
        type: "image",
        caption: "Background image"
    }
};