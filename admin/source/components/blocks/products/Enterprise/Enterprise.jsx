import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, object } from "prop-types";

import { Link } from "components/controls";

import classes from "./Enterprise.less";
import defaultProps from "./Enterprise.settings";


export default class Enterprise extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        linkColor: object,
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        description: shape({
            text: string,
        }),
        link: shape({
            url: string,
            label: string
        }),
        backgroundImage: shape({
            url: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.enterprise, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a }, backgroundImage } = this.props;

        return {
            backgroundImage: `url(${backgroundImage.url})`,
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    render() {
        let { title, description, link, descriptionColor, titleColor, linkColor } = this.props;

        let TitleTagName = title.tag;

        let linkStyle = this.getTextColorStyle(linkColor, "fill");
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.enterpriseContainer}>
                    {
                        title.visible &&
                        <TitleTagName
                            style={titleStyle}
                            className={classes.enterpriseTitle}
                            dangerouslySetInnerHTML={this.titleHtml}>
                        </TitleTagName>
                    }

                    <p
                        style={descriptionStyle}
                        className={classes.enterpriseDescription}
                        dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                    </p>

                    <Link
                        url={link.url}
                        label={link.label}
                        theme="white"
                        showArrow={true}
                        className={classes.enterpriseLink}
                        linkStyles={linkStyle}
                    />
                </div>
            </div>
        );
    }
}