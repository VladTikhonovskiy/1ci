import React from "react";
import classnames from "classnames";
import { bool, shape, string, oneOf, object } from "prop-types";

import { Link } from "components/controls";

import classes from "./Products1.less";
import defaultProps from "./Products1.settings";


export default class Products1 extends React.PureComponent {
    static propTypes = {
        id: string,
        backgroundColor: object,
        titleColor: object,
        descriptionColor: object,
        linkColor: object,
        image: shape({
            url: string
        }),
        title: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool
        }),
        description: shape({
            text: string,
        }),
        link: shape({
            url: string,
            label: string
        }),
        className: string
    }

    static defaultProps = defaultProps

    getTextColorStyle(color, name) {
        let { r, g, b, a } = color;

        return {
            color: `rgba(${r}, ${g}, ${b}, ${a})`,
            [name]: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    getContentHtml(text) {
        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    get classes() {
        return classnames(classes.products1, this.props.className);
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    get style() {
        let { backgroundColor: { r, g, b, a } } = this.props;

        return {
            backgroundColor: `rgba(${r}, ${g}, ${b}, ${a})`
        };
    }

    get titleHtml() {
        let { title: { text } } = this.props;

        return {
            __html: text.replace(/(?:\r\n|\r|\n)/g, "<br />")
        };
    }

    renderImage() {
        let { image } = this.props;

        let containerStyle = {};
        if (image.url) {
            containerStyle.backgroundColor = "transparent";
        }

        return (
            <div className={classes.products1ImageContainer} style={containerStyle}>
                <img src={image.url} alt="" className={classes.products1Image} />
            </div>
        );
    }

    render() {
        let { title, description, link, descriptionColor, titleColor, linkColor } = this.props;

        let TitleTagName = title.tag;

        let linkStyle = this.getTextColorStyle(linkColor, "fill");
        let titleStyle = this.getTextColorStyle(titleColor);
        let descriptionStyle = this.getTextColorStyle(descriptionColor);

        return (
            <div className={this.classes} {...this.attributes} style={this.style}>
                <div className={classes.products1Container}>
                    { this.renderImage() }

                    <div className={classes.products1Content}>
                        {
                            title.visible &&
                            <TitleTagName
                                style={titleStyle}
                                className={classes.products1Title}
                                dangerouslySetInnerHTML={this.titleHtml}>
                            </TitleTagName>
                        }

                        <p
                            style={descriptionStyle}
                            className={classes.products1Description}
                            dangerouslySetInnerHTML={this.getContentHtml(description.text)}>
                        </p>

                        <Link
                            url={link.url}
                            label={link.label}
                            theme="white"
                            showArrow={true}
                            className={classes.products1Link}
                            linkStyles={linkStyle}
                        />
                    </div>
                </div>
            </div>
        );
    }
}