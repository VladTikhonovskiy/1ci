import { defaultColorWhite } from "../../default";

export default {
    id: "",

    backgroundColor: {
        r: 21,
        g: 138,
        b: 255,
        a: 1
    },

    titleColor: defaultColorWhite,

    descriptionColor: defaultColorWhite,

    linkColor: defaultColorWhite,


    image: {
        url: ""
    },

    title: {
        tag: "h2",
        text: "Software Developers",
        visible: true
    },

    description: {
        text: "If you are a VAR or developer, turn your business ideas into reality with 1Ci powerful development platform. Quickly deploy your industry-specific solutions, cloud apps, knowledge-based applications for projects of any type and scale: accounting, finance, manufacturing, inventory, sales, orders, HR and many more."
    },

    link: {
        url: "",
        label: "Learn more"
    }
};