export const defaultBackgroundColor = {
    r: 255,
    g: 255,
    b: 255,
    a: 1
}

export const defaultTextColor = {
    r: 9,
    g: 30,
    b: 66,
    a: 1
}

export const defaultLinkColor = {
    r: 21,
    g: 138,
    b: 255,
    a: 1
}

export const defaultColorWhite = {
    r: 255,
    g: 255,
    b: 255,
    a: 1
}
