import React from "react";
import classnames from "classnames";
import { string, oneOf, array } from "prop-types";

import classes from "./Breadcrumbs.less";
import defaultProps from "./Breadcrumbs.settings";


export default class Breadcrumbs extends React.PureComponent {
    static propTypes = {
        id: string,
        breadcrumbsLinks: array,
        theme: oneOf(["Light", "Dark"]),
        className: string
    }

    static defaultProps = defaultProps

    renderBreadcrumbsLinks() {
        let { breadcrumbsLinks, id } =this.props;

        return (
            breadcrumbsLinks.map((element, index) => {
                return(
                    <React.Fragment key={`item-${id + index}`}>
                        <a className={classes.breadcrumbsLink} href={element.button.link}>{element.button.label}</a>
                        {
                            breadcrumbsLinks.length !== index + 1 &&
                            <span className={classes.breadcrumbsDivider}> › </span>
                        }
                    </React.Fragment>)
            })
        )
    }

    get classes() {
        let { theme, className } = this.props;

        return classnames(classes.breadcrumbs, className, {
            [classes.breadcrumbsDark]: theme === "Dark",
            [classes.breadcrumbsLight]: theme === "Light"
        });
    }

    get attributes() {
        let attributes = {};

        if (this.props.id) {
            attributes.id = this.props.id;
        }

        return attributes;
    }

    render() {
        return (
            <div className={this.classes} {...this.attributes}>
                <span>{ this.renderBreadcrumbsLinks() }</span>
            </div>
        );
    }
}