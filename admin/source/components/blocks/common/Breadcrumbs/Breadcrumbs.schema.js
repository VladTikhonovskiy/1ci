export default {
    id: {
        type: "string",
        caption: "ID"
    },

    theme: {
        type: "dropdown",
        caption: "Theme",
        options: ["Light", "Dark"]
    },

    breadcrumbsLinks: {
        type: "list",
        minCount: 1,
        caption: "Breadcrumbs",

        element: {
            button: {
                type: "button",
                caption: "Button"
            }
        },

        newElement: {
            button: {
                link: ""
            }
        }
    }
}