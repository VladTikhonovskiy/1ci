import React from "react";
import classnames from "classnames";
import { string, object } from "prop-types";

import classes from "./Card.less";


export default class Card extends React.PureComponent {
    static propTypes = {
        style: object,
        className: string
    }

    get classes() {
        return classnames(classes.card, this.props.className);
    }

    render() {
        let { style, children } = this.props;

        return (
            <div style={style} className={this.classes}>
                { children }
            </div>
        );
    }
}