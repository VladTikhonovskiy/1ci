import React from "react";
import classnames from "classnames";
import { bool, string, func, oneOf, object } from "prop-types";

import classes from "./Textbox.less";


export default class Textbox extends React.PureComponent {
    static propTypes = {
        label: string,
        value: string,
        placeholder: string,
        error: string,
        theme: oneOf(["light", "dark"]),
        state: oneOf(["valid", "invalid"]),
        disabled: bool,
        className: string,
        labelStyles: object,
        onChange: func
    }

    static defaultProps = {
        value: "",
        placeholder: "",
        theme: "light",
        disabled: false
    }

    get classes() {
        let { theme, disabled, className } = this.props;

        return classnames(classes.textbox, className, {
            [classes.textboxDark]: theme === "dark",
            [classes.textboxLight]: theme === "light",
            
            [classes.textboxDisabled]: disabled
        });
    }

    get inputClasses() {
        let { state } = this.props;

        return classnames(classes.textboxInput, {
            [classes.textboxInputValid]: state === "valid",
            [classes.textboxInputInvalid]: state === "invalid"
        });
    }

    get shouldRenderLabel() {
        return this.props.label;
    }

    get shouldRenderError() {
        return this.props.error;
    }

    render() {
        let { label, value, placeholder, error, disabled,labelStyles, onChange } = this.props;

        return (
            <div className={this.classes}>
                <div className={classes.textboxHeader}>
                    {
                        this.shouldRenderLabel &&
                        <label className={classes.textboxLabel} style={labelStyles}> { label } </label>
                    }

                    {
                        this.shouldRenderError &&
                        <span className={classes.textboxError}  title={error}> { error } </span>
                    }
                </div>

                <input
                    type="text"
                    value={value}
                    placeholder={placeholder}
                    disabled={disabled}
                    className={this.inputClasses}
                    onChange={onChange}
                />
            </div>
        );
    }
}