import React from "react";
import classnames from "classnames";
import { bool, string, func, oneOf } from "prop-types";

import classes from "./Checkbox.less";


export default class Checkbox extends React.PureComponent {
    static propTypes = {
        text: string,
        theme: oneOf(["light", "dark"]),
        className: string,
        isChecked: bool,
        onChange: func
    }

    static defaultProps = {
        text: "",
        isChecked: false,
        theme: "light"
    }

    get classes() {
        let { theme, className } = this.props;

        return classnames(classes.checkbox, className, {
            [classes.checkboxDark]: theme === "dark",
            [classes.checkboxLight]: theme === "light",
        });
    }

    render() {
        let { text, onChange, isChecked } = this.props;

        return (
            <div className={this.classes}>
                <label className={classes.checkboxLabel}>
                    <input className={classes.checkboxInput} type="checkbox" checked={isChecked} onChange={onChange} />
                    <span className={classes.checkboxCheckedState}></span>
                    { text }
                </label>
            </div>
        );
    }
}