import React from "react";
import { string, func } from "prop-types";
import classnames from "classnames";

import classes from "./CircleButton.less";


export default class CircleButton extends React.PureComponent {
    static propTypes = {
        icon: string,
        className: string,
        onClick: func
    }

    get classes() {
        return classnames(classes.circleButton, this.props.className);
    }

    render() {
        return (
            <button className={this.classes} onClick={this.props.onClick}>
                <i className={classes.circleButtonIcon} dangerouslySetInnerHTML={{__html: this.props.icon}} />
            </button>
        );
    }
}