import React from "react";
import classnames from "classnames";
import { bool, string, oneOf, object } from "prop-types";

import { Icon } from "components/common";

import classes from "./Link.less";


export default class Link extends React.PureComponent {
    static propTypes = {
        url: string.isRequired,
        label: string.isRequired,
        theme: oneOf(["blue", "white"]),
        showArrow: bool,
        linkStyles: object,
        className: string
    }

    static defaultProps = {
        theme: "blue",
        showArrow: false
    }

    get classes() {
        let { theme, className } = this.props;

        return classnames(classes.link, className, {
            [classes.linkBlue]: theme === "blue",
            [classes.linkWhite]: theme === "white"
        });
    }

    get shouldRenderArrow() {
        return this.props.showArrow;
    }

    render() {
        let { url, label, linkStyles } = this.props;

        return (
            <a href={url} className={this.classes} style={linkStyles}>
                { label }

                {
                    this.shouldRenderArrow &&
                    <Icon name="arrowRight"  style={linkStyles} className={classes.linkIcon} />
                }
            </a>
        );
    }
}