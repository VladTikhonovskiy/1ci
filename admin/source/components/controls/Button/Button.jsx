import React from "react";
import classnames from "classnames";
import { bool, string, func, oneOf, object } from "prop-types";

import { Icon } from "components/common";

import classes from "./Button.less";


export default class Button extends React.PureComponent {
    static propTypes = {
        label: string.isRequired,
        type: oneOf(["primary", "secondary", "alternate"]),
        link: string,
        disabled: bool,
        className: string,
        buttonStyles: object,
        onClick: func
    }

    static defaultProps = {
        type: "primary",
        disabled: false
    }

    get classes() {
        let { type, link, disabled, className } = this.props;

        return classnames(classes.button, className, {
            [classes.buttonPrimary]: type === "primary",
            [classes.buttonSecondary]: type === "secondary",
            [classes.buttonAlternate]: type === "alternate",

            [classes.buttonLink]: link,
            [classes.buttonDisabled]: disabled
        });
    }

    get attributes() {
        let attributes = {};

        if (this.props.disabled) {
            attributes.disabled = "disabled";
        }

        return attributes;
    }

    renderLink() {
        let { link, label } = this.props;

        return (
            <a href={link} className={this.classes} {...this.attributes}>
                { label }
                
                <Icon name="link" className={classes.buttonLinkIcon} />
            </a>
        );
    }

    renderButton() {
        let { label, onClick, buttonStyles } = this.props;

        return (
            <button className={this.classes} {...this.attributes} style={buttonStyles} onClick={onClick}>
                { label }
            </button>
        );
    }

    render() {
        if (this.props.link) {
            return this.renderLink();
        }

        return this.renderButton();
    }
}