import React from "react";

import PartnersIcon from "material-ui/svg-icons/communication/contact-mail";
import PageBuilderIcon from "material-ui/svg-icons/av/web";
import SystemUsersIcon from "material-ui/svg-icons/action/lock";
import WebsiteUsersIcon from "material-ui/svg-icons/action/account-circle";
import LocalizationIcon from "material-ui/svg-icons/action/language";

import classes from "./Navigation.less";
import routes from "constants/routes";


export const menuItems = [
    {
        route: routes.pageBuilder,
        title: "Page Builder",
        icon: <PageBuilderIcon className={classes.navigationMenuLinkIcon} />
    },

    {
        route: routes.websiteUsers,
        title: "Web Site Users",
        icon: <WebsiteUsersIcon className={classes.navigationMenuLinkIcon} />
    },

    {
        route: routes.partners,
        title: "Partners",
        icon: <PartnersIcon className={classes.navigationMenuLinkIcon} />
    },

    {
        route: routes.localization,
        title: "Localization",
        icon: <LocalizationIcon className={classes.navigationMenuLinkIcon} />
    },

    {
        route: routes.systemUsers,
        title: "System Users",
        icon: <SystemUsersIcon className={classes.navigationMenuLinkIcon} />
    }
];