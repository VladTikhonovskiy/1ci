import React from "react";
import classnames from "classnames";
import { bool, func, string } from "prop-types";
import { NavLink } from "react-router-dom";
import { List, ListItem, Drawer, Divider, Subheader, IconButton } from "material-ui";
import { LogoutIcon, ArrowRightIcon } from "images/icons/svg-icons";

import { Image } from "components/common";

import classes from "./Navigation.less";
import { menuItems } from "./Navigation.settings";


export default class Navigation extends React.PureComponent {
    static propTypes = {
        open: bool,
        location: string,
        className: string,
        onMenuItemClick: func,
        onLogoutButtonClick: func
    }

    static defaultProps = {
        open: false
    }

    handleMenuItemClick = () => {
        this.props.onMenuItemClick && this.props.onMenuItemClick();
    }

    handleLogoutButtonClick = () => {
        this.props.onLogoutButtonClick && this.props.onLogoutButtonClick();
    }

    get classes() {
        return classnames(classes.navigation, this.props.className);
    }

    renderActiveRouteIcon(route) {
        let isActiveRoute = this.props.location === route;

        if (isActiveRoute)
            return <ArrowRightIcon className={classes.navigationMenuLinkIcon} />;

        return null;
    }

    renderMenuLink(route, title, icon) {
        let activeRouteIcon = this.renderActiveRouteIcon(route);

        return (
            <NavLink to={route} className={classes.navigationMenuLink} onClick={this.handleMenuItemClick}>
                <ListItem
                    leftIcon={icon}
                    primaryText={title}
                    rightIcon={activeRouteIcon}
                />
            </NavLink>
        );
    }

    renderMenuItem({ route, title, icon }, index) {
        let isLastMenuItem = index === menuItems.length - 1;
        let shouldRenderDivider = isLastMenuItem === false;

        let menuLink = this.renderMenuLink(route, title, icon);

        return (
            <React.Fragment key={route}>
                { menuLink }

                { shouldRenderDivider && <Divider /> }
            </React.Fragment>
        ); 
    }

    renderContainer() {
        return (
            <div className={classes.navigationContainer}>
                <div className={classes.navigationHeader}>
                    <Image name="logo" className={classes.navigationLogo} />

                    <IconButton onClick={this.handleLogoutButtonClick}>
                        <LogoutIcon />
                    </IconButton>
                </div>

                <div className={classes.navigationUser}>
                    <Subheader> User </Subheader>

                    <span className={classes.navigationUserName}>
                        admin@example.com
                    </span>
                </div>

                <div className={classes.navigationMenu}>
                    <List>
                        { menuItems.map((...props) => this.renderMenuItem(...props)) }
                    </List>
                </div>
            </div>
        );
    }

    render() {
        let container = this.renderContainer();

        return (
            <div className={this.classes}>
                <Drawer width={360} open={this.props.open}>
                    { container }
                </Drawer>
            </div>
        );
    }
}