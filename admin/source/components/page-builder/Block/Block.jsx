import React from "react";
import classnames from "classnames";
import { bool, string, object, func } from "prop-types";

import blocks from "components/blocks";

import classes from "./Block.less";


export default class Block extends React.PureComponent {
    static propTypes = {
        name: string.isRequired,
        settings: object,
        selected: bool,
        className: string,
        onClick: func,
        onMouseDown: func
    }

    static defaultProps = {
        selected: false
    }

    get classes() {
        return classnames(classes.block, this.props.className, {
            [classes.blockSelected]: this.props.selected
        });
    }
        
    render() {
        let { onClick, onMouseDown } = this.props;

        let Block = blocks[this.props.name];

        return (
            <div className={this.classes} onClick={onClick} onMouseDown={onMouseDown}>
                <Block {...this.props.settings} className={classes.blockElement} />
            </div>
        );
    }
}