export const initialSocialLinkState = {
    icon: "",
    title: "",
    isLink: false,
    linkType: "page",
    linkHref: "",
    linkPageId: null,
    linkTarget: "_blank"
};