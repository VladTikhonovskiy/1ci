import React from "react";
import classnames from "classnames";
import { Scrollbars } from "react-custom-scrollbars";
import { bool, shape, string, number, oneOf, arrayOf, func } from "prop-types";
import { Dialog, TextField, IconButton, Drawer, FlatButton, RaisedButton } from "material-ui";

import { pageBuilder } from "config";
import { DeleteIcon, BackspaceIcon as BackIcon } from "images/icons/svg-icons";
import { ImageSelector, MenuLinkEditor } from "components/page-builder";

import classes from "./FooterMenuSocialLinkEditor.less";
import { initialSocialLinkState } from "./FooterMenuSocialLinkEditor.settings";


const keys = {
    escape: 27
};

export default class FooterMenuSocialLinkEditor extends React.PureComponent {
    static propTypes = {
        open: bool,
        pages: arrayOf(shape({
            id: number,
            name: string
        })),
        socialLink: shape({
            id: string,
            icon: "",
            title: string,
            isLink: bool,
            linkType: oneOf(["page", "external-link"]),
            linkHref: string,
            linkPageId: number,
            linkTarget: oneOf(["_blank", "_self", "_parent", "_top"])
        }),
        className: string,
        onApply: func,
        onClose: func,
        onRemove: func,
        uploadIconAction: func,
        showMessageAction: func
    }

    static defaultProps = {
        open: false
    }

    state = {
        ...initialSocialLinkState,
        removeWarningDialogShown: false
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleKeyDown = event => {
        if (event.keyCode === keys.escape) {
            this.props.onClose && this.props.onClose();
        }
    }

    handleFormSubmit = event => {
        event.preventDefault();

        this.props.onApply && this.props.onApply(this.state);
    }

    handleRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: true
        });
    }

    handleTitleChange = (event, value) => {
        this.setState({
            title: value
        });
    }

    handleIconChange = icon => {
        this.setState({ icon });
    }

    handleLinkChange = (fieldName, fieldValue) => {
        this.setState({
            [fieldName]: fieldValue
        });
    }

    handleCancelRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });
    }

    handleApplyRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });

        setTimeout(() => {
            this.props.onRemove && this.props.onRemove();
        }, 300);
    }

    get classes() {
        return classnames(classes.footerMenuSocialLinkEditor, this.props.className);
    }

    get shouldRenderRemoveButton() {
        return this.props.socialLink;
    }

    renderRemoveWarningDialog() {
        let { removeWarningDialogShown } = this.state;

        let buttons = [
            <FlatButton label="No" onClick={this.handleCancelRemoveButtonClick} />,
            <FlatButton label="Yes" secondary={true} onClick={this.handleApplyRemoveButtonClick} />
        ];

        return (
            <Dialog title="Remove social link" modal={true} open={removeWarningDialogShown} actions={buttons}>
                Are you sure you want to remove social link?
            </Dialog>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === false)
            return;

        this.refs.title.focus();

        if (nextProps.socialLink) {
            this.setState({
                ...nextProps.socialLink
            });

            return;
        }

        this.setState({
            ...initialSocialLinkState
        });
    }

    renderHeader() {
        let { socialLink } = this.props;

        return (
            <div className={classes.footerMenuSocialLinkEditorHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <BackIcon />
                </IconButton>

                <h2 className={classes.footerMenuSocialLinkEditorTitle}>
                    { socialLink ? socialLink.title : "Add social link" }
                </h2>

                {
                    this.shouldRenderRemoveButton &&
                    <IconButton onClick={this.handleRemoveButtonClick}>
                        <DeleteIcon />
                    </IconButton>
                }
            </div>
        );
    }

    render() {
        let { open, pages, uploadIconAction, showMessageAction } = this.props;
        let { title, icon } = this.state;

        let { baseUrl, maxFileSize } = pageBuilder.imageSelector;

        return (
            <Drawer open={open} width={460} containerClassName={classes.footerMenuSocialLinkEditorContainer}>
                { this.renderHeader() }

                <Scrollbars autoHide>
                    <form className={this.classes} onSubmit={this.handleFormSubmit} onKeyDown={this.handleKeyDown} tabIndex="0">
                        <TextField
                            ref="title"
                            value={title}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Title"
                            className={classes.footerMenuSocialLinkEditorTextField}
                            onChange={this.handleTitleChange}
                        />

                        <ImageSelector
                            imageUrl={icon}
                            imageBaseUrl={baseUrl}
                            maxImageSize={maxFileSize}
                            uploadAction={uploadIconAction}
                            messageAction={showMessageAction}
                            onImageChange={this.handleIconChange}
                            className={classes.footerMenuSocialLinkEditorIconSelector}
                        />

                        <MenuLinkEditor
                            link={this.state}
                            pages={pages}
                            className={classes.footerMenuSocialLinkEditorLinkEditor}
                            onChange={this.handleLinkChange}
                        />

                        <RaisedButton type="submit" label="Apply" primary={true} fullWidth={true} />

                        { this.renderRemoveWarningDialog() }
                    </form>
                </Scrollbars>
            </Drawer>
        );
    }
}