import React from "react";
import classnames from "classnames";
import Collapsible from "react-collapsible";
import { bool, string, shape, number, func, oneOf, arrayOf } from "prop-types";
import { TextField, SelectField, MenuItem, Toggle, RadioButtonGroup, RadioButton } from "material-ui";

import classes from "./MenuLinkEditor.less";


export default class MenuLinkEditor extends React.PureComponent {
    static propTypes = {
        pages: arrayOf(shape({
            id: number,
            name: string
        })),
        link: shape({
            isLink: bool,
            linkType: oneOf(["page", "external-link"]),
            linkHref: string,
            linkPageId: number,
            linkTarget: oneOf(["_blank", "_self", "_parent", "_top"])
        }),
        className: string,
        onChange: func.isRequired
    }

    static defaultProps = {
        pages: [],
        link: {
            isLink: false,
            linkType: "page",
            linkHref: "",
            linkPageId: null,
            linkTarget: "_blank"
        }
    }

    handleLinkChange = fieldName => (event, fieldValue) => {
        this.props.onChange(fieldName, fieldValue);
    }

    handleLinkPageChange = (event, key, linkPageId) => {
        this.props.onChange("linkPageId", linkPageId);
    }

    handleLinkTargetChange = (event, key, linkTarget) => {
        this.props.onChange("linkTarget", linkTarget);
    }

    get classes() {
        return classnames(classes.menuLinkEditor, this.props.className);
    }

    get toggleStyle() {
        return {
            width: "52px"
        };
    }

    get radioButtonLabelStyle() {
        return {
            marginLeft: "-8px",
            fontSize: "14px",
            fontWeight: "500",
            color: "rgba(0, 0, 0, 0.87)"
        };
    }

    render() {
        let { pages } = this.props;
        let { isLink, linkType, linkHref, linkPageId, linkTarget } = this.props.link;

        return (
            <div className={this.classes}>
                <div className={classes.menuLinkEditorHeader}>
                    <label className={classes.menuLinkEditorTitle}>
                        Link
                    </label>

                    <Toggle toggled={isLink} style={this.toggleStyle} onToggle={this.handleLinkChange("isLink")} />
                </div>

                <Collapsible open={isLink} easing="ease-in-out" transitionTime={150}>
                    <RadioButtonGroup
                        name="link-type"
                        valueSelected={linkType}
                        className={classes.menuLinkEditorRadioGroup}
                        onChange={this.handleLinkChange("linkType")}>

                        <RadioButton
                            value="external-link"
                            label="External link"
                            labelStyle={this.radioButtonLabelStyle}
                            className={classes.menuLinkEditorRadioButton}
                        />
                        
                        <RadioButton
                            value="page"
                            label="Page"
                            labelStyle={this.radioButtonLabelStyle}
                            className={classes.menuLinkEditorRadioButton}
                        />
                    </RadioButtonGroup>

                    {
                        linkType === "page" &&
                        <SelectField
                            value={linkPageId}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Page"
                            menuItemStyle={{fontSize: "14px"}}
                            className={classes.menuLinkEditorSelectField}
                            onChange={this.handleLinkPageChange}>

                            {
                                pages.map(page => (
                                    <MenuItem key={page.id} value={page.id} primaryText={page.version.name} />
                                ))
                            }
                        </SelectField>
                    }

                    {
                        linkType === "external-link" &&
                        <TextField
                            value={linkHref}
                            fullWidth={true}
                            hintText="https://"
                            floatingLabelFixed={true}
                            floatingLabelText="URL"
                            className={classes.menuLinkEditorTextField}
                            onChange={this.handleLinkChange("linkHref")}
                        />
                    }

                    <SelectField
                        value={linkTarget}
                        fullWidth={true}
                        floatingLabelFixed={true}
                        floatingLabelText="Target"
                        menuItemStyle={{fontSize: "14px"}}
                        className={classes.menuLinkEditorSelectField}
                        onChange={this.handleLinkTargetChange}>

                        <MenuItem value="_blank" primaryText="Blank" />
                        <MenuItem value="_self" primaryText="Self" />
                        <MenuItem value="_parent" primaryText="Parent" />
                        <MenuItem value="_top" primaryText="Top" />
                    </SelectField>
                </Collapsible>
            </div>
        );
    }
}