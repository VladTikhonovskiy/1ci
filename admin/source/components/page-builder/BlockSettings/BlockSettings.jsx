import React from "react";
import classnames from "classnames";
import { bool, string, object, func } from "prop-types";
import { Scrollbars } from "react-custom-scrollbars";
import { Drawer, IconButton } from "material-ui";

import * as editors from "components/page-builder/editors";
import { CloseIcon, CheckIcon } from "images/icons/svg-icons";

import classes from "./BlockSettings.less";
import schemas from "components/blocks/schemas";


const editorTypesMap = {
    link: "LinkEditor",
    list: "ListEditor",
    image: "ImageEditor",
    title: "TitleEditor",
    string: "TextEditor",
    button: "ButtonEditor",
    textbox: "TextboxEditor",
    dropdown: "DropdownEditor",
    description: "DescriptionEditor",
    backgroundColor: "ColorEditor",
    html: "HtmlEditor",
    number: "NumberEditor"
};

export default class BlockSettings extends React.PureComponent {
    static propTypes = {
        open: bool,
        block: object,
        className: string,
        onClose: func,
        onApply: func,
        onSettingsChange: func
    }

    static defaultProps ={
        open: false
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleApplyButtonClick = () => {
        this.props.onApply && this.props.onApply();
    }

    handleFieldValueChange = (fieldName, fieldValue) => {
        let { block, onSettingsChange } = this.props;

        let settings = {
            ...block.settings,
            [fieldName]: fieldValue
        };

        onSettingsChange && onSettingsChange(settings);
    }

    get classes() {
        return classnames(classes.blockSettings, this.props.className);
    }

    get shouldRenderContent() {
        return this.props.block;
    }

    getFieldEditorElement(fieldName, fieldSchema, block) {
        let { type } = fieldSchema;

        let key = fieldName;
        let fieldValue = block.settings[fieldName];

        let props = {
            key,
            fieldName,
            fieldValue,
            fieldSchema,
            onChange: this.handleFieldValueChange 
        };

        let editorComponentName = editorTypesMap[type];
        let Editor = editors[editorComponentName];

        return <Editor {...props} />;
    }

    renderHeader() {
        return (
            <div className={classes.blockSettingsHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <CloseIcon />
                </IconButton>

                <h2 className={classes.blockSettingsTitle}>
                    Section edit
                </h2>

                <IconButton onClick={this.handleApplyButtonClick}>
                    <CheckIcon />
                </IconButton>
            </div>
        );
    }

    renderContent() {
        let { block } = this.props;

        let schema = schemas[block.name];
        let elements = [];

        for (let fieldName in schema) {
            let fieldSchema = schema[fieldName];
            let fieldElement = this.getFieldEditorElement(fieldName, fieldSchema, block);
            
            elements.push(fieldElement);
        }

        return (
            <div className={classes.blockSettingsContent}>
                <p className={classes.blockSettingsBlockName}> { block.name } </p>

                { elements }
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes}>
                <Drawer width={460} containerClassName={classes.blockSettingsContainer} open={this.props.open}>
                    { this.renderHeader() }

                    <Scrollbars autoHide>
                        { this.shouldRenderContent && this.renderContent() }
                    </Scrollbars>
                </Drawer>
            </div>
        );
    }
}