import React from "react";
import classnames from "classnames";
import { string, func, object, arrayOf } from "prop-types";

import { HeaderMenu, FooterMenu } from "components/blocks";

import classes from "./PageTemplate.less";


export default class PageTemplate extends React.PureComponent {
    static props = {
        selectedSection: string,
        headerMenu: arrayOf(object),
        footerMenu: arrayOf(object),
        footerSocialLinks: arrayOf(object),
        footerSecondaryLinks: arrayOf(object),
        className: string,
        onSectionSelect: func
    }

    handleSectionMouseDown = sectionName => () => {
        this.props.onSectionSelect(sectionName);
    }

    get classes() {
        return classnames(classes.pageTemplate, this.props.className);
    }

    render() {
        let { selectedSection, headerMenu, footerMenu, footerSocialLinks, footerSecondaryLinks } = this.props;

        let headerMenuClasses = classnames(classes.pageTemplateHeaderMenu, {
            [classes.pageTemplateHeaderMenuSelected]: selectedSection === "header"
        });

        let footerMenuClasses = classnames(classes.pageTemplateFooterMenu, {
            [classes.pageTemplateFooterMenuSelected]: selectedSection === "footer"
        });

        return (
            <div className={this.classes}>
                <div className={classes.headerMenuWrapper} onMouseDown={this.handleSectionMouseDown("header")}>
                    <HeaderMenu
                        menu={headerMenu}
                        className={headerMenuClasses}
                    />
                </div>


                <div className={classes.pageTemplateContent}>
                    <span className={classes.pageTemplateContentTitle}> Content area </span>
                </div>

                <div className={classes.footerMenuWrapper} onMouseDown={this.handleSectionMouseDown("footer")}>
                    <FooterMenu
                        menu={footerMenu}
                        socialLinks={footerSocialLinks}
                        secondaryLinks={footerSecondaryLinks}
                        className={footerMenuClasses}
                    />
                </div>
            </div>
        );
    }
}