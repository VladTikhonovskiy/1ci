import React from "react";
import { array } from "prop-types";

import blocks from "components/blocks";


export default class PureBlocks extends React.PureComponent {
    static propTypes = {
        blocks: array.isRequired
    }

    render() {
        return this.props.blocks.map(({ id, name, settings }) => {
            let Block = blocks[name];

            return <Block key={id} {...settings} />;
        });
    }
}