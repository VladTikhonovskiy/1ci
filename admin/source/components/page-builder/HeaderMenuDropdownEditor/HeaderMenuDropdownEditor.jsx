import React from "react";
import classnames from "classnames";
import Collapsible from "react-collapsible";
import { bool, string, shape, number, func, arrayOf } from "prop-types";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import { TextField, SelectField, MenuItem, Toggle } from "material-ui";

import { Icon } from "components/common";

import classes from "./HeaderMenuDropdownEditor.less";


const columns = [1, 2];

export default class HeaderMenuDropdownEditor extends React.PureComponent {
    static propTypes = {
        pages: arrayOf(shape({
            id: number,
            name: string
        })),
        menuItem: shape({
            dropdown: bool,
            columnCount: number,
            columnTitles: arrayOf(string)
        }),
        className: string,
        onChange: func.isRequired,
        onFadil: func
    }

    static defaultProps = {
        menuItem: {
            dropdown: false,
            columnCount: 2,
            columnTitles: ["", ""],
        }
    }

    handleMenuItemChange = fieldName => (event, fieldValue) => {
        this.props.onChange(fieldName, fieldValue);
    }

    handleColumnCountChange = (event, key, columnCount) => {
        this.props.onChange("columnCount", columnCount);
    }

    handleFadil = () => {
        this.props.onFadil();
    }

    get classes() {
        return classnames(classes.headerMenuDropdownEditor, this.props.className);
    }

    get toggleStyle() {
        return {
            width: "52px"
        };
    }

    render() {
        let { menuItem } = this.props;
        let { dropdown, columnCount, columnTitles } = menuItem;

        let SortableItem = SortableElement(item => item.value);

        let SortableList = SortableContainer(({ items }) => {
            return (
                <div>
                    {
                        items.map((item, index) => (
                            <SortableItem key={`item-${index}`} index={index} value={item} />
                        ))
                    }
                </div>
            );
        });

        let columnElements = columns.map((column, columnIndex) => (
            <div key={column} className={classes.footerMenuSettingsItemList}>
                <label className={classes.footerMenuSettingsColumnLabel}>
                    { column } column
                </label>

                <TextField
                    ref="title"
                    value={columnTitles[columnIndex]}
                    fullWidth={true}
                    floatingLabelFixed={true}
                    floatingLabelText="Title"
                    className={classes.headerMenuItemEditorTextField}
                    onChange={this.handleTitleChange}
                />

                <SortableList
                    items={menuItem.children || []}
                    useDragHandle={true}
                    helperClass={classes.footerMenuSettingsSortableHelper}
                    onSortEnd={this.handleMenuItemSortEnd}
                />
            </div>
        ));

        return (
            <div className={this.classes}>
                <div className={classes.headerMenuDropdownEditorHeader}>
                    <label className={classes.headerMenuDropdownEditorTitle}>
                        Dropdown menu
                    </label>

                    <Toggle toggled={dropdown} style={this.toggleStyle} onToggle={this.handleMenuItemChange("dropdown")} />
                </div>

                <Collapsible open={dropdown} easing="ease-in-out" transitionTime={150}>
                    <SelectField
                        value={columnCount}
                        fullWidth={true}
                        floatingLabelFixed={true}
                        floatingLabelText="Columns"
                        menuItemStyle={{fontSize: "14px"}}
                        className={classes.headerMenuDropdownEditorSelectField}
                        onChange={this.handleColumnCountChange}>

                        <MenuItem value={1} primaryText="1 column" />
                        <MenuItem value={2} primaryText="2 columns" />
                    </SelectField>

                    <button type="button" className={classes.headerMenuDropdownEditorAddItemButton} onClick={this.handleFadil}>
                        <Icon name="plus" className={classes.headerMenuDropdownEditorAddItemButtonIcon} />
                        Add menu item
                    </button>

                    { columnElements }
                </Collapsible>
            </div>
        );
    }
}