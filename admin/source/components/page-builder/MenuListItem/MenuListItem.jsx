import React from "react";
import classnames from "classnames";
import { string, shape } from "prop-types";
import { SortableHandle } from "react-sortable-hoc";

import { Icon } from "components/common";
import { PageIcon } from "images/icons/svg-icons";

import classes from "./MenuListItem.less";


export default class MenuListItem extends React.PureComponent {
    static propTypes = {
        item: shape({
            id: string,
            title: string
        }),
        className: string
    }

    get classes() {
        return classnames(classes.menuListItem, this.props.className);
    }

    render() {
        let { item, className, ...props } = this.props;

        let DragHandle = SortableHandle(() => (
            <Icon name="drag" className={classes.menuListItemPositionIcon} />
        ));

        return (
            <button className={this.classes} {...props}>
                <DragHandle />
                <PageIcon className={classes.menuListItemIcon} />

                <span className={classes.menuListItemTitle}>
                    { item.title }
                </span>
            </button>
        );
    }
}