import React from "react";
import classnames from "classnames";
import { string, number, func } from "prop-types";
import { FloatingActionButton, TextField } from "material-ui";

import { Spinner } from "components/common";
import { FileUploadIcon } from "images/icons/svg-icons";

import classes from "./ImageSelector.less";


const errorMessages = {
    maxFileSize: "Selected image exceeds max files size of 2MB",
    uploadError: "An error occured during image upload. Please try again"
};

const uploadPreloaderTimeout = 200;

export default class ImageSelector extends React.PureComponent {
    static propTypes = {
        imageUrl: string,
        imageBaseUrl: string,
        maxImageSize: number,
        className: string,
        uploadAction: func,
        messageAction: func,
        onImageChange: func
    }

    state = {
        uploadedImage: null,
        uploadPending: false
    }

    handleImageUrlChange = event => {
        this.props.onImageChange(event.target.value);
    }

    handleFileUploadButtonClick = () => {
        this.refs.uploader.click();
    }

    handleFileUploaderChange = event => {
        let selectedImage = event.target.files[0];

        if (selectedImage.size >= this.props.maxImageSize) {
            this.props.messageAction(errorMessages.maxFileSize);
            return;
        }

        this.uploadPreloaderTimer = setTimeout(() => {
            this.setState({
                uploadPending: true
            });
        }, uploadPreloaderTimeout);

        this.uploadImage(selectedImage);
    }

    uploadImage(imageToUpload) {
        this.props.uploadAction(imageToUpload)
            .then(uploadedImage => {
                this.uploadImageSuccess(uploadedImage);
            })
            .catch(response => {
                this.uploadImageFailure(response);
            });
    }

    uploadImageSuccess(uploadedImage) {
        clearTimeout(this.uploadPreloaderTimer);

        this.setState({
            uploadedImage,
            uploadPending: false
        }, () => {
            this.props.onImageChange(this.uploadedImageFullPath);
        });
    }

    uploadImageFailure() {
        clearTimeout(this.uploadPreloaderTimer);

        this.setState({
            uploadPending: false
        });

        this.props.messageAction(errorMessages.uploadError);
    }

    get uploadedImageFullPath() {
        let { imageBaseUrl } = this.props;
        let { uploadedImage } = this.state;

        return imageBaseUrl + uploadedImage.path;
    }

    get isImageSet() {
        return !!this.props.imageUrl;
    }

    get classes() {
        return classnames(classes.imageSelector, this.props.className);
    }

    get previewClasses() {
        let shouldRenderEmptyBackground = this.isImageSet === false;

        return classnames(classes.imageSelectorPreview, {
            [classes.imageSelectorPreviewEmpty]: shouldRenderEmptyBackground
        });
    }

    get previewStyle() {
        let result = {};

        let { imageUrl } = this.props;
        let { uploadedImage } = this.state;

        let backgroundImageUrl = uploadedImage ? this.uploadedImageFullPath : imageUrl;
        if (backgroundImageUrl) {
            result.backgroundImage = `url(${backgroundImageUrl})`
        }

        return result;
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            uploadedImage: null
        });
    }

    render() {
        let { imageUrl } = this.props;
        let { uploadPending } = this.state;

        return (
            <div className={this.classes}>
                <div className={this.previewClasses} style={this.previewStyle}>
                    <FloatingActionButton className={classes.imageSelectorUploadButton} onClick={this.handleFileUploadButtonClick}>
                        <FileUploadIcon />
                    </FloatingActionButton>

                    {
                        uploadPending &&
                        <Spinner className={classes.imageSelectorPreloader} />
                    }
                </div>

                <TextField
                    fullWidth={true}
                    value={imageUrl}
                    hintText="http://"
                    floatingLabelText="Image URL"
                    floatingLabelFixed={true}
                    onChange={this.handleImageUrlChange}
                />

                <input
                    type="file"
                    ref="uploader"
                    tabIndex="0"
                    accept=".png, .jpeg, .jpg, .gif, .svg"
                    className={classes.imageSelectorUploaderInput}
                    onChange={this.handleFileUploaderChange}
                />
            </div>
        );
    };
}