import React from "react";
import { string, object, arrayOf, func } from "prop-types";
import classnames from "classnames";
import { IconButton } from "material-ui";

import blocks from "components/blocks";
import classes from "./PagePreview.less";
import { CloseIcon, DeviceDesktopIcon, DeviceTabletIcon, DevicePhoneIcon } from "images/icons/svg-icons";


export default class PagePreview extends React.PureComponent {
    static propTypes = {
        blocks: arrayOf(object),
        className: string,
        onClose: func
    }

    state = {
        visible: false,
        selectedDevice: "desktop"
    }

    handleDeviceButtonClick = deviceType => () => {
        this.setState({
            selectedDevice: deviceType
        });
    }

    handleCloseButtonClick = () => {
        this.setState({
            visible: false
        });

        setTimeout(() => {
            this.props.onClose && this.props.onClose();
        }, 200);
    }

    get classes() {
        return classnames(classes.pagePreview, this.props.className, {
            [classes.pagePreviewVisible]: this.state.visible
        });
    }

    componentDidMount() {
        requestAnimationFrame(() => {
            this.setState({
                visible: true
            });
        });
    }

    getDeviceButtonClasses(deviceType) {
        return classnames(classes.pagePreviewDeviceButton, {
            [classes.pagePreviewDeviceButtonSelected]: this.state.selectedDevice === deviceType
        });
    }

    renderHeader() {
        return (
            <div className={classes.pagePreviewHeader}>
                <IconButton className={this.getDeviceButtonClasses("desktop")} onClick={this.handleDeviceButtonClick("desktop")}>
                    <DeviceDesktopIcon />
                </IconButton>

                <IconButton className={this.getDeviceButtonClasses("phone")} onClick={this.handleDeviceButtonClick("phone")}>
                    <DevicePhoneIcon />
                </IconButton>

                <IconButton className={this.getDeviceButtonClasses("tablet")} onClick={this.handleDeviceButtonClick("tablet")}>
                    <DeviceTabletIcon />
                </IconButton>

                <IconButton className={classes.pagePreviewCloseButton} onClick={this.handleCloseButtonClick}>
                    <CloseIcon />
                </IconButton>
            </div>
        );
    }

    renderContent() {
        return (
            <div className={classes.pagePreviewContent}>
                {
                    this.props.blocks.map(block => {
                        let Block = blocks[block.name];

                        return <Block key={block.id} {...block.settings} />;
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <div className={this.classes}>
                { this.renderHeader() }
                { this.renderContent() }
            </div>
        );
    }
}