export const initialSecondaryLinkState = {
    title: "",
    isLink: false,
    linkType: "page",
    linkHref: "",
    linkPageId: null,
    linkTarget: "_blank"
};