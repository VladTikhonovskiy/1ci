import React from "react";
import classnames from "classnames";
import { Scrollbars } from "react-custom-scrollbars";
import { bool, shape, string, number, oneOf, arrayOf, func } from "prop-types";
import { Dialog, TextField, IconButton, Drawer, FlatButton, RaisedButton } from "material-ui";

import { DeleteIcon, BackspaceIcon as BackIcon } from "images/icons/svg-icons";
import { MenuLinkEditor } from "components/page-builder";

import classes from "./FooterMenuSecondaryLinkEditor.less";
import { initialSecondaryLinkState } from "./FooterMenuSecondaryLinkEditor.settings";


const keys = {
    escape: 27
};

export default class FooterMenuSecondaryLinkEditor extends React.PureComponent {
    static propTypes = {
        open: bool,
        pages: arrayOf(shape({
            id: number,
            name: string
        })),
        secondaryLink: shape({
            id: string,
            title: string,
            isLink: bool,
            linkType: oneOf(["page", "external-link"]),
            linkHref: string,
            linkPageId: number,
            linkTarget: oneOf(["_blank", "_self", "_parent", "_top"])
        }),
        className: string,
        onApply: func,
        onClose: func,
        onRemove: func
    }

    static defaultProps = {
        open: false
    }

    state = {
        ...initialSecondaryLinkState,
        removeWarningDialogShown: false
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleKeyDown = event => {
        if (event.keyCode === keys.escape) {
            this.props.onClose && this.props.onClose();
        }
    }

    handleFormSubmit = event => {
        event.preventDefault();

        this.props.onApply && this.props.onApply(this.state);
    }

    handleRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: true
        });
    }

    handleTitleChange = (event, value) => {
        this.setState({
            title: value
        });
    }

    handleLinkChange = (fieldName, fieldValue) => {
        this.setState({
            [fieldName]: fieldValue
        });
    }

    handleCancelRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });
    }

    handleApplyRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });

        setTimeout(() => {
            this.props.onRemove && this.props.onRemove();
        }, 300);
    }

    get classes() {
        return classnames(classes.footerMenuSecondaryLinkEditor, this.props.className);
    }

    get shouldRenderRemoveButton() {
        return this.props.secondaryLink;
    }

    renderRemoveWarningDialog() {
        let { removeWarningDialogShown } = this.state;

        let buttons = [
            <FlatButton label="No" onClick={this.handleCancelRemoveButtonClick} />,
            <FlatButton label="Yes" secondary={true} onClick={this.handleApplyRemoveButtonClick} />
        ];

        return (
            <Dialog title="Remove secondary link" modal={true} open={removeWarningDialogShown} actions={buttons}>
                Are you sure you want to remove secondary link?
            </Dialog>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === false)
            return;

        this.refs.title.focus();

        if (nextProps.secondaryLink) {
            this.setState({
                ...nextProps.secondaryLink
            });

            return;
        }

        this.setState({
            ...initialSecondaryLinkState
        });
    }

    renderHeader() {
        let { secondaryLink } = this.props;

        return (
            <div className={classes.footerMenuSecondaryLinkEditorHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <BackIcon />
                </IconButton>

                <h2 className={classes.footerMenuSecondaryLinkEditorTitle}>
                    { secondaryLink ? secondaryLink.title : "Add secondary link" }
                </h2>

                {
                    this.shouldRenderRemoveButton &&
                    <IconButton onClick={this.handleRemoveButtonClick}>
                        <DeleteIcon />
                    </IconButton>
                }
            </div>
        );
    }

    render() {
        let { open, pages } = this.props;
        let { title } = this.state;

        return (
            <Drawer open={open} width={460} containerClassName={classes.footerMenuSecondaryLinkEditorContainer}>
                { this.renderHeader() }

                <Scrollbars autoHide>
                    <form className={this.classes} onSubmit={this.handleFormSubmit} onKeyDown={this.handleKeyDown} tabIndex="0">
                        <TextField
                            ref="title"
                            value={title}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Title"
                            className={classes.footerMenuSecondaryLinkEditorTextField}
                            onChange={this.handleTitleChange}
                        />

                        <MenuLinkEditor
                            link={this.state}
                            pages={pages}
                            className={classes.footerMenuSecondaryLinkEditorLinkEditor}
                            onChange={this.handleLinkChange}
                        />

                        <RaisedButton type="submit" label="Apply" primary={true} fullWidth={true} />

                        { this.renderRemoveWarningDialog() }
                    </form>
                </Scrollbars>
            </Drawer>
        );
    }
}