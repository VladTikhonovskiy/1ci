import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { shape, string, object, func } from "prop-types";
import { TextField } from "material-ui";

import classes from "./TextboxEditor.less";


export default class TextboxEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: shape({
            label: string,
            placeholder: string
        }).isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    constructor(props) {
        super(props);

        this.id = shortid.generate();
    }

    handleLabelTextChange = event => {
        this.sendChangeEvent("label", event.target.value);
    }

    handlePlaceholderTextChange = event => {
        this.sendChangeEvent("placeholder", event.target.value);
    }

    sendChangeEvent(name, value) {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            [name]: value
        });
    }

    get classes() {
        return classnames(classes.textboxEditor, this.props.className);
    }

    render() {
        let { fieldValue: { label, placeholder }, fieldSchema: { caption } } = this.props;

        return (
            <div className={this.classes}>
                <TextField
                    name={this.id}
                    value={label}
                    fullWidth={true}
                    floatingLabelFixed={true}
                    floatingLabelText={caption}
                    onChange={this.handleLabelTextChange}
                />

                <TextField
                    name={this.id + "__placeholder"}
                    value={placeholder}
                    fullWidth={true}
                    floatingLabelFixed={true}
                    floatingLabelText="Placeholder"
                    onChange={this.handlePlaceholderTextChange}
                />
            </div>
        );
    };
}
