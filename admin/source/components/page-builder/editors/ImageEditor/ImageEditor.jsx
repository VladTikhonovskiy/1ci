import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import { shape, string, object, func } from "prop-types";

import { pageBuilder } from "config";
import { ImageSelector } from "components/page-builder";

import classes from "./ImageEditor.less";
import * as applicationActions from "modules/app/app.actions";
import * as pageBuilderActions from "modules/page-builder/page-builder.actions";


class ImageEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: shape({
            url: string
        }).isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    handleImageChange = imageUrl => {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            url: imageUrl
        });
    }

    get classes() {
        return classnames(classes.imageEditor, this.props.className);
    }

    render() {
        let { fieldValue, fieldSchema, uploadImage, showMessage } = this.props;
        let { baseUrl, maxFileSize } = pageBuilder.imageSelector;

        return (
            <div className={this.classes}>
                <label className={classes.imageEditorTitle}>
                    { fieldSchema.caption }
                </label>

                <ImageSelector
                    imageUrl={fieldValue.url}
                    imageBaseUrl={baseUrl}
                    maxImageSize={maxFileSize}
                    uploadAction={uploadImage}
                    messageAction={showMessage}
                    onImageChange={this.handleImageChange}
                />
            </div>
        );
    }
}


export default connect(null, { ...applicationActions, ...pageBuilderActions })(ImageEditor);
