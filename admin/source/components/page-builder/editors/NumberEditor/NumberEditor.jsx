import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { string, object, func } from "prop-types";
import { TextField } from "material-ui";

import classes from "./NumberEditor.less";


export default class NumberEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: string.isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    constructor(props) {
        super(props);

        this.id = shortid.generate();
    }

    handleInputChange = event => {
        let value = event.target.value;
        this.sendChangeEvent(value.replace(/[^0-9-]/, ""));
    }

    sendChangeEvent(value) {
        let { fieldName, onChange } = this.props;

        onChange && onChange(fieldName, value);
    }

    get classes() {
        return classnames(classes.numberEditor, this.props.className);
    }

    render() {
        let { fieldValue, fieldSchema: { caption } } = this.props;

        return (
            <div className={this.classes}>
                <TextField
                    name={this.id +  + `__${caption}`}
                    value={fieldValue}
                    floatingLabelFixed={true}
                    floatingLabelText={caption}
                    onChange={this.handleInputChange}
                    className={classes.numberEditorInput}
                />
            </div>
        );
    }
}