import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { connect } from "react-redux";
import { shape, string, object, func } from "prop-types";
import { TextField, AutoComplete } from "material-ui";

import classes from "./LinkEditor.less";


class LinkEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: shape({
            url: string,
            label: string
        }).isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    constructor(props) {
        super(props);

        this.id = shortid.generate();
    }

    handleUrlChange = url => {
        this.sendChangeEvent("url", url);
    }

    handleLabelTextChange = event => {
        this.sendChangeEvent("label", event.target.value);
    }

    sendChangeEvent(name, value) {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            [name]: value
        });
    }

    get classes() {
        return classnames(classes.linkEditor, this.props.className);
    }

    renderLabelEditor() {
        let name = `label-editor__${this.id}`;
        let label = this.props.fieldValue.label;

        return (
            <TextField
                name={name}
                value={label}
                fullWidth={true}
                onChange={this.handleLabelTextChange}
            />
        );
    }

    renderUrlEditor() {
        let { fieldValue: { url }, urlList } = this.props;

        return (
            <AutoComplete
                fullWidth={true}
                searchText={url}
                hintText="http://"
                dataSource={urlList}
                onUpdateInput={this.handleUrlChange}
            />
        );
    }

    render() {
        return (
            <div className={this.classes}>
                <label className={classes.linkEditorTitle}>
                    { this.props.fieldSchema.caption }
                </label>

                { this.renderLabelEditor() }
                { this.renderUrlEditor() }
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        urlList: pageBuilder.urlList
    };
}

export default connect(mapStateToProps)(LinkEditor);