import React from "react";


export const CustomToolbar = () => (
    <div id="toolbar">
        <span className="ql-formats">
            <select className="ql-font">
                <option value="source_sans_pro">Source Sans Pro</option>
                <option value="rubik">Rubik</option>
            </select>
            <select className="ql-size"></select>
        </span>
        <span className="ql-formats">
            <button className="ql-bold"></button>
            <button className="ql-italic"></button>
            <button className="ql-underline"></button>
            <button className="ql-strike"></button>
        </span>
        <span className="ql-formats">
            <select className="ql-color"></select>
            <select className="ql-background"></select>
        </span>
        <span className="ql-formats">
            <button className="ql-blockquote"></button>
            <button className="ql-link"></button>
        </span>
        <span className="ql-formats">
            <button className="ql-list" value="ordered"></button>
            <button className="ql-list" value="bullet"></button>
            <button className="ql-indent" value="-1"></button>
            <button className="ql-indent" value="+1"></button>
        </span>
        <span className="ql-formats">
            <button className="ql-direction" value="rtl"></button>
            <select className="ql-align"></select>
        </span>
        <span className="ql-formats">
            <button className="ql-clean"></button>
        </span>
    </div>
);

export const editorSettings =  {
    modules : {
        toolbar: {
            container: "#toolbar"
        }
    },
    formats : [
        "header", "font",
        "bold", "italic", "underline", "strike", "blockquote",
        "list", "bullet", "indent",
        "link", "align",  "color", "size", "background"
    ]
}