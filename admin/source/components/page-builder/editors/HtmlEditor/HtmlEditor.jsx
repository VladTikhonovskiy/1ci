import React from "react";
import { string, object, func } from "prop-types";
import ReactQuill, { Quill } from "react-quill";

import { CustomToolbar, editorSettings } from "./HtmlEditor.settings"

const Font = Quill.import("formats/font");
Font.whitelist = ["source_sans_pro", "rubik"];
Quill.register(Font, true);


export default class HtmlEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: object,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    sendChangeEvent(name, value) {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            [name]: value
        });
    }

    handleChange = html => {
        this.sendChangeEvent("text", html);
    }

    render() {
        let { fieldValue } = this.props;

        return (
            <div>
                <CustomToolbar />
                <ReactQuill theme="snow"
                            value={fieldValue.text}
                            onChange={this.handleChange}
                            modules={editorSettings.modules}
                            formats={editorSettings.formats}>
                </ReactQuill>
            </div>
        );
    }
}