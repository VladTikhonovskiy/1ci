import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { string, object, func } from "prop-types";
import { TextField } from "material-ui";

import classes from "./TextEditor.less";


export default class TextEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: string.isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    constructor(props) {
        super(props);

        this.id = shortid.generate();
    }

    handleChange = event => {
        let { fieldName, onChange } = this.props;

        onChange && onChange(fieldName, event.target.value);
    }

    get classes() {
        return classnames(classes.textEditor, this.props.className);
    }

    get style() {
        let { fieldSchema: { width } } = this.props;

        if (width) {
            return {
                display: "inline-block",
                width: `${width}%`,
                marginRight: "5px"
            };
        }
    }

    render() {
        let { fieldValue, fieldSchema: { caption, multiLine } } = this.props;

        let props = {
            name: this.id,
            value: fieldValue,
            fullWidth: true,
            floatingLabelText: caption,
            floatingLabelFixed: true,
            onChange: this.handleChange
        };

        if (multiLine) {
            props.rows = 1;
            props.rowsMax = 2;
            props.multiLine = true;
        }

        return (
            <div className={this.classes} style={this.style}>
                <TextField {...props} />
            </div>
        );
    }
}