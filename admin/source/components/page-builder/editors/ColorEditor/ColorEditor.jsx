import React from "react";
import shortid from "shortid";
import { string, object, func } from "prop-types";
import { TextField, Popover } from "material-ui";
import { SketchPicker } from "react-color";


export default class ColorEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: object,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    state = {
        opened: false,
        anchorElement: null
    }

    constructor(props)   {
        super(props);

        this.id = shortid.generate();
    }

    handleColorChange = color => {
        this.sendChangeEvent("backgroundColor", color.rgb);
    };

    handleTextFieldFocus = event => {
        this.setState({
            opened: true,
            anchorElement: event.currentTarget
        });
    }

    handleRequestClose = () => {
        this.setState({
            opened: false
        });
    };

    sendChangeEvent(name, value) {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            ...value
        });
    }

    render() {
        let { fieldValue: { r, g, b, a }, fieldValue, fieldSchema, className } = this.props;
        let { opened, anchorElement } = this.state;

        let rgbaColorString = `rgba(${r}, ${g}, ${b}, ${a})`;

        return (
            <div className={className}>
                <TextField
                    name={this.id}
                    value={rgbaColorString}
                    fullWidth={true}
                    floatingLabelFixed={true}
                    floatingLabelText={fieldSchema.caption}
                    onFocus={this.handleTextFieldFocus}
                />

                <Popover
                    open={opened}
                    anchorEl={anchorElement}
                    anchorOrigin={{horizontal: "left", vertical: "bottom"}}
                    onRequestClose={this.handleRequestClose}>

                    <SketchPicker color={fieldValue} onChangeComplete={this.handleColorChange} />
                </Popover>
            </div>
        );
    }
}