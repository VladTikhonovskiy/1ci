import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { bool, shape, string, object, func, oneOf } from "prop-types";
import { IconButton, TextField, FlatButton } from "material-ui";

import * as icons from "images/icons/svg-icons";

import classes from "./TitleEditor.less";


const tags = ["h1", "h2", "h3", "h4", "h5", "h6"];
const aligns = ["Left", "Center", "Right"];

export default class TitleEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: shape({
            tag: oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]),
            text: string,
            visible: bool,
            position: oneOf(["Left", "Center", "Right"])
        }).isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    constructor(props)   {
        super(props);

        this.id = shortid.generate();
    }

    handleTextChange = event => {
        this.sendChangeEvent("text", event.target.value);
    }

    handleTagButtonClick = tag => event => {
        this.sendChangeEvent("tag", tag);
    }

    handleAlignButtonClick = align => event => {
        this.sendChangeEvent("position", align);
    }

    handleToggleVisibilityButtonClick = () => {
        this.sendChangeEvent("visible", !this.props.fieldValue.visible);
    }

    sendChangeEvent(name, value) {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            [name]: value
        });
    }

    get classes() {
        return classnames(classes.titleEditor, this.props.className);
    }

    renderTextEditor() {
        let { fieldValue: { text }, fieldSchema: { caption } } = this.props;

        return (
            <TextField
                name={this.id}
                value={text}
                rows={1}
                rowsMax={2}
                multiLine={true}
                fullWidth={true}
                floatingLabelText={caption}
                floatingLabelFixed={true}
                onChange={this.handleTextChange}
            />
        );
    }

    renderTagButton(tag) {
        let style = {
            width: 40,
            minWidth: 40
        };

        let labelStyle = {
            paddingLeft: 0,
            paddingRight: 0,
            textTransform: "uppercase"
        };

        if (this.props.fieldValue.tag === tag) {
            style.backgroundColor = "#172b4d";
            labelStyle.color = "white";
        }

        return (
            <FlatButton
                key={tag}
                label={tag}
                style={style}
                labelStyle={labelStyle}
                onClick={this.handleTagButtonClick(tag)}
            />
        );
    }

    renderAlignButton(align) {
        let IconTagName = icons[`Align${align}Icon`];

        let style = {};
        let iconColor = "black";

        if (this.props.fieldValue.position === align) {
            style.backgroundColor = "#172b4d";
            iconColor = "white";
        }

        return (
            <IconButton key={align} style={style} onClick={this.handleAlignButtonClick(align)}>
                <IconTagName color={iconColor} />
            </IconButton>
        );
    }

    render() {
        let { fieldValue: { visible, position, tag } } = this.props;

        return (
            <div className={this.classes}>
                <div style={{display: "flex"}}>
                    { this.renderTextEditor() }

                    <IconButton
                        className={classes.titleEditorVisibility}
                        onClick={this.handleToggleVisibilityButtonClick}>

                        { visible ? <icons.VisibilityIcon /> : <icons.VisibilityOffIcon /> }
                    </IconButton>
                </div>

                <div className={classes.titleEditorSecondRow}>
                    {
                        tag &&
                        <div className={classes.titleEditorTagSelector}>
                            { tags.map(tag => this.renderTagButton(tag)) }
                        </div>
                    }

                    {
                        position &&
                        <div className={classes.titleEditorAlignSelector}>
                            { aligns.map(align => this.renderAlignButton(align)) }
                        </div>
                    }
                </div>
            </div>
        );
    }
}