import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { shape, string, object, func } from "prop-types";
import { TextField } from "material-ui";

import classes from "./DescriptionEditor.less";


export default class DescriptionEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: shape({
            text: string
        }).isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    constructor(props) {
        super(props);

        this.id = shortid.generate();
    }

    handleChange = event => {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            text: event.target.value
        });
    }

    get classes() {
        return classnames(classes.descriptionEditor, this.props.className);
    }

    render() {
        let { fieldValue, fieldSchema } = this.props;

        return (
            <div className={this.classes}>
                <TextField
                    name={this.id}
                    value={fieldValue.text}
                    floatingLabelFixed={true}
                    floatingLabelText={fieldSchema.caption}
                    rows={2}
                    rowsMax={4}
                    multiLine={true}
                    fullWidth={true}
                    onChange={this.handleChange}
                />
            </div>
        );
    }
}