import React from "react";
import shortid from "shortid";
import classnames from "classnames";
import { connect } from "react-redux";
import { shape, string, object, } from "prop-types";
import { TextField, AutoComplete } from "material-ui";

import classes from "./ButtonEditor.less";


class ButtonEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: shape({
            link: string,
            label: string
        }).isRequired,
        fieldSchema: object.isRequired,
        className: string
    }

    constructor(props) {
        super(props);

        this.id = shortid.generate();
    }

    handleLinkTextChange = link => {
        this.sendChangeEvent("link", link);
    }

    handleLabelTextChange = event => {
        this.sendChangeEvent("label", event.target.value);
    }

    sendChangeEvent(name, value) {
        let { fieldName, fieldValue, onChange } = this.props;

        onChange && onChange(fieldName, {
            ...fieldValue,
            [name]: value
        });
    }

    get classes() {
        return classnames(classes.buttonEditor, this.props.className);
    }

    renderLabelEditor() {
        let name = `label-editor__${this.id}`;
        let label = this.props.fieldValue.label;

        return (
            <TextField
                name={name}
                value={label}
                fullWidth={true}
                onChange={this.handleLabelTextChange}
            />
        );
    }

    renderLinkEditor() {
        let { fieldValue: { link }, urlList } = this.props;

        return (
            <AutoComplete
                fullWidth={true}
                searchText={link}
                hintText="http://"
                dataSource={urlList}
                onUpdateInput={this.handleLinkTextChange}
            />
        );
    }

    render() {
        return (
            <div className={this.classes}>
                <label className={classes.buttonEditorTitle}>
                    { this.props.fieldSchema.caption }
                </label>

                { this.renderLabelEditor() }
                { this.renderLinkEditor() }
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        urlList: pageBuilder.urlList
    };
}

export default connect(mapStateToProps)(ButtonEditor);
