import React from "react";
import classnames from "classnames";
import { array, string, object, func } from "prop-types";
import { IconButton, RaisedButton } from "material-ui";

import { CloseIcon } from "images/icons/svg-icons";

import classes from "./ListEditor.less";
import * as editors from "components/page-builder/editors";


const editorTypesMap = {
    link: "LinkEditor",
    image: "ImageEditor",
    title: "TitleEditor",
    string: "TextEditor",
    button: "ButtonEditor",
    textbox: "TextboxEditor",
    dropdown: "DropdownEditor",
    description: "DescriptionEditor",
    number: "NumberEditor"
};

const defaultAddElementButtonCaption = "Add card";

export default class ListEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: array.isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    state = {
        listShow: false
    }

    handleAddListItemButtonClick = () => {
        let { fieldName, fieldValue, fieldSchema, onChange } = this.props;

        let updatedFieldValue = [...fieldValue, fieldSchema.newElement];

        onChange && onChange(fieldName, updatedFieldValue);
    }

    handleRemoveListItemButtonClick = listItemIndex => () => {
        let { fieldName, fieldValue, onChange } = this.props;

        let updatedFieldValue = fieldValue.filter((listItem, index) => {
            return listItemIndex !== index;
        });

        onChange && onChange(fieldName, updatedFieldValue);
    }

    handleFieldValueChange = listItemIndex => (listItemFieldName, listItemFieldValue) => {
        let { fieldName, fieldValue, onChange } = this.props;

        let updatedFieldValue = fieldValue.map((listItem, index) => {
            if (index === listItemIndex)
                return {
                    ...listItem,
                    [listItemFieldName]: listItemFieldValue
                };

            return listItem;
        });

        onChange && onChange(fieldName, updatedFieldValue);
    }

    handleShowPopoverButtonClick = () => {
        this.setState({
            listShow: !this.state.listShow
        });
    }

    getEditorComponent(fieldName, fieldSchema, fieldValue, listItemIndex) {
        let props = {
            key: fieldName,
            fieldName,
            fieldValue,
            fieldSchema,
            onChange: this.handleFieldValueChange(listItemIndex)
        };

        let editorComponentName = editorTypesMap[fieldSchema.type];
        let Editor = editors[editorComponentName];

        return <Editor {...props} />;
    }

    get classes() {
        return classnames(classes.listEditor, this.props.className);
    }

    renderListItemEditor(listItemValue, listItemIndex) {
        let editorComponents = [];
        let listItemSchema = this.props.fieldSchema.element;

        for (let schemaName in listItemSchema) {
            let listItemFieldValue = listItemValue[schemaName];
            let listItemFieldSchema = listItemSchema[schemaName];

            let editorComponent = this.getEditorComponent(schemaName, listItemFieldSchema, listItemFieldValue, listItemIndex);

            editorComponents.push(editorComponent);
        }

        return (
            <div key={listItemIndex} className={classes.listEditorListItemEditor}>
                <p className={classes.listEditorListItemTitle}>
                    { this.props.fieldSchema.caption }

                    <IconButton onClick={this.handleRemoveListItemButtonClick(listItemIndex)}>
                        <CloseIcon />
                    </IconButton>
                </p>

                { editorComponents }
            </div>
        );
    }

    renderList() {
        let { fieldSchema, fieldValue } = this.props;

        let buttonLabel = fieldSchema.addElementButtonCaption || defaultAddElementButtonCaption;

        return (
            <React.Fragment>
                { fieldValue.map((listItemValue, index) => this.renderListItemEditor(listItemValue, index)) }

                <RaisedButton
                    primary={true}
                    label={buttonLabel}
                    className={classes.listEditorAddButton}
                    onClick={this.handleAddListItemButtonClick}
                />
            </React.Fragment>
        )
    }

    render() {
        let { fieldSchema } = this.props;

        let { listShow } = this.state;

        let buttonCloseClasses = classnames(classes.listEditorAddButton, {
            [classes.listEditorAddButtonClose]: listShow
        });

        return (
            <div className={this.classes}>
                {
                    fieldSchema.menuCollapse ?
                    <React.Fragment>
                        <RaisedButton
                            primary={true}
                            label={listShow ? "Close" : fieldSchema.caption}
                            className={buttonCloseClasses}
                            onClick={this.handleShowPopoverButtonClick}
                        />
                        { listShow && this.renderList() }
                    </React.Fragment>
                    :
                    this.renderList()
                }
            </div>
        );
    }
}