import React from "react";
import classnames from "classnames";
import { string, object, func } from "prop-types";
import { DropDownMenu, MenuItem } from "material-ui";

import classes from "./DropdownEditor.less";


export default class DropdownEditor extends React.PureComponent {
    static propTypes = {
        fieldName: string.isRequired,
        fieldValue: string.isRequired,
        fieldSchema: object.isRequired,
        className: string,
        onChange: func
    }

    handleChange = (event, index, value) => {
        let { fieldName, onChange } = this.props;

        onChange && onChange(fieldName, value);
    }

    get classes() {
        return classnames(classes.dropdownEditor, this.props.className);
    }

    get style() {
        return {
            style: {
                width: "100%"
            },

            labelStyle: {
                paddingLeft: "0"
            },

            underlineStyle: {
                marginLeft: "0",
                marginRight: "0"
            },

            iconStyle: {
                right: "-8px"
            }
        };
    }

    renderDropdownItems() {
        return this.props.fieldSchema.options.map(option => (
            <MenuItem key={option} value={option} primaryText={option} />
        ));
    }

    render() {
        let { fieldValue, fieldSchema } = this.props;

        return (
            <div className={this.classes}>
                <label className={classes.dropdownEditorTitle}>
                    { fieldSchema.caption }
                </label>

                <DropDownMenu
                    {...this.style}
                    value={fieldValue}
                    autoWidth={false}
                    className={classes.dropdownEditorTag}
                    onChange={this.handleChange}>
                    
                    { this.renderDropdownItems() }
                </DropDownMenu>
            </div>
        );
    }
}