export const sections = [
    "CTA",
    "Buttons And Links",
    "Hero",
    "Cards",
    "Titles",
    "Visual",
    "Common",
    "Reviews",
    "Benefits",
    "Partners",
    "Products",
    "Dividers",
    "Features",
    "Separators",
    "Statistics",
    "HtmlEditor",
    "Infographic"
];

export const blocks = [
    {
        name: "GetStarted",
        section: "CTA",
        caption: "Get Started"
    },

    {
        name: "ReadyToJoin",
        section: "CTA",
        caption: "Ready To Join"
    },

    {
        name: "LinkWithIcon",
        section: "Buttons And Links",
        caption: "Link with icon"
    },

    {
        name: "LinkWithButton",
        section: "Buttons And Links",
        caption: "Link with button"
    },

    {
        name: "HtmlEditor",
        section: "HtmlEditor",
        caption: "Html Editor"
    },

    {
        name: "Hero1",
        section: "Hero",
        caption: "Hero 1"
    },

    {
        name: "Hero2",
        section: "Hero",
        caption: "Hero 2"
    },

    {
        name: "Hero3",
        section: "Hero",
        caption: "Hero 3"
    },

    {
        name: "Hero4",
        section: "Hero",
        caption: "Hero 4"
    },

    {
        name: "Hero6",
        section: "Hero",
        caption: "Hero 6"
    },

    {
        name: "Main",
        section: "Visual",
        caption: "Main"
    },

    {
        name: "Breadcrumbs",
        section: "Common",
        caption: "Breadcrumbs"
    },

    {
        name: "Reviews",
        section: "Reviews",
        caption: "Reviews"
    },

    {
        name: "Reviews2",
        section: "Reviews",
        caption: "Reviews 2"
    },

    {
        name: "Title1",
        section: "Titles",
        caption: "Title 1"
    },

    {
        name: "Title2",
        section: "Titles",
        caption: "Title 2"
    },

    {
        name: "Title3",
        section: "Titles",
        caption: "Title 3"
    },

    {
        name: "SimpleDivider",
        section: "Dividers",
        caption: "Simple Divider",
    },

    {
        name: "CountryDivider",
        section: "Dividers",
        caption: "Country Divider"
    },

    {
        name: "BigIcons1",
        section: "Features",
        caption: "Big Icons 1"
    },

    {
        name: "BigIcons2",
        section: "Features",
        caption: "Big Icons 2"
    },

    {
        name: "BigIcons3",
        section: "Features",
        caption: "Big Icons 3"
    },

    {
        name: "Features1",
        section: "Features",
        caption: "Features 1"
    },

    {
        name: "ImageList",
        section: "Features",
        caption: "Image List 1"
    },

    {
        name: "ImageList2",
        section: "Features",
        caption: "Image List 2"
    },

    {
        name: "ListImage",
        section: "Features",
        caption: "List Image"
    },

    {
        name: "Checklist",
        section: "Features",
        caption: "Checklist"
    },

    {
        name: "Contacts",
        section: "Features",
        caption: "Contacts"
    },

    {
        name: "Brochure",
        section: "Features",
        caption: "Brochure"
    },

    {
        name: "SimpleSeparator",
        section: "Separators",
        caption: "Simple Separator"
    },

    {
        name: "Cards1",
        section: "Cards",
        caption: "Cards 1"
    },

    {
        name: "Cards2",
        section: "Cards",
        caption: "Cards 2"
    },

    {
        name: "Cards3",
        section: "Cards",
        caption: "Cards 3"
    },

    {
        name: "Benefits1",
        section: "Benefits",
        caption: "Benefits 1"
    },

    {
        name: "Benefits2",
        section: "Benefits",
        caption: "Benefits 2"
    },

    {
        name: "Benefits3",
        section: "Benefits",
        caption: "Benefits 3"
    },

    {
        name: "Benefits4",
        section: "Benefits",
        caption: "Benefits 4"
    },

    {
        name: "Persons",
        section: "Benefits",
        caption: "Persons"
    },

    {
        name: "Partners1",
        section: "Partners",
        caption: "Partners 1"
    },

    {
        name: "Partners2",
        section: "Partners",
        caption: "Partners 2"
    },

    {
        name: "Enterprise",
        section: "Products",
        caption: "Enterprise"
    },

    {
        name: "Products1",
        section: "Products",
        caption: "Products 1"
    },

    {
        name: "Products2",
        section: "Products",
        caption: "Products 2"
    },

    {
        name: "Statistics1",
        section: "Statistics",
        caption: "Statistics 1"
    },

    {
        name: "Infographic",
        section: "Infographic",
        caption: "Infographic"
    }
];
