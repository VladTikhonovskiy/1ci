import React from "react";
import classnames from "classnames";
import { string, func } from "prop-types";
import Collapsible from "react-collapsible";
import { RaisedButton } from "material-ui";

import { ToggleIcon } from "images/icons/svg-icons";

import classes from "./BlockSelector.less";
import { blocks, sections } from "./BlockSelector.settings";


export default class BlockSelector extends React.PureComponent {
    static propTypes = {
        className: string,
        onBlockSelect: func
    }

    handleBlockClick = block => event => {
        this.props.onBlockSelect && this.props.onBlockSelect(block);
    }

    getSectionProps(section, index) {
        let props = {
            key: section,
            trigger: this.getSectionTriggerElement(section),
            easing: "ease-in-out",
            transitionTime: 150
        };

        if (index === 0) {
            props.open = true;
        }

        return props;
    }

    getSectionTriggerElement(section) {
        return (
            <div className={classes.blockSelectorSectionTrigger}>
                <ToggleIcon className={classes.blockSelectorSectionTriggerIcon} />

                { section }
            </div>
        );
    }

    get classes() {
        return classnames(classes.blockSelector, this.props.className);
    }

    renderSectionBlocks(section) {
        let sectionBlocks = blocks.filter(block => block.section === section);

        return sectionBlocks.map(block => (
            <RaisedButton
                key={block.name}
                label={block.caption}
                className={classes.blockSelectorBlock}
                onClick={this.handleBlockClick(block)}
            />
        ));
    }

    renderSection(section, index) {
        let sectionProps = this.getSectionProps(section, index);
        let sectionBlocks = this.renderSectionBlocks(section);

        return (
            <Collapsible
                {...sectionProps}
                className={classes.blockSelectorSection}
                openedClassName={classes.blockSelectorSectionOpened}>

                { sectionBlocks }
            </Collapsible>
        );
    }

    render() {
        return (
            <div className={this.classes}>
                { sections.map((section, index) => this.renderSection(section, index)) }
            </div>
        );
    }
}