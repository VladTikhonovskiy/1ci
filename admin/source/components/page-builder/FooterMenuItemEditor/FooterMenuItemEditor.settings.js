export const initialMenuItemState = {
    title: "",
    type: "headline",
    column: "1",

    isLink: false,
    linkType: "page",
    linkHref: "",
    linkPageId: null,
    linkTarget: "_blank"
};