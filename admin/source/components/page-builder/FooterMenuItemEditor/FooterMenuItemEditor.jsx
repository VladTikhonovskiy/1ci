import React from "react";
import classnames from "classnames";
import { Scrollbars } from "react-custom-scrollbars";
import { bool, shape, string, number, oneOf, arrayOf, func } from "prop-types";
import { Drawer, Dialog, MenuItem, IconButton, FlatButton, RaisedButton, TextField, SelectField } from "material-ui";

import { DeleteIcon, BackspaceIcon as BackIcon } from "images/icons/svg-icons";
import { MenuLinkEditor } from "components/page-builder";

import classes from "./FooterMenuItemEditor.less";
import { initialMenuItemState } from "./FooterMenuItemEditor.settings";


const keys = {
    escape: 27
};

export default class FooterMenuItemEditor extends React.PureComponent {
    static propTypes = {
        open: bool,
        pages: arrayOf(shape({
            id: number,
            name: string
        })),
        menuItem: shape({
            id: string,
            title: string,
            type: oneOf(["headline", "regular"]),
            column: string,
            isLink: bool,
            linkType: oneOf(["page", "external-link"]),
            linkHref: string,
            linkPageId: number,
            linkTarget: oneOf(["_blank", "_self", "_parent", "_top"])
        }),
        className: string,
        onApply: func,
        onClose: func,
        onRemove: func
    }

    static defaultProps = {
        open: false
    }

    state = {
        ...initialMenuItemState,
        removeWarningDialogShown: false
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleKeyDown = event => {
        if (event.keyCode === keys.escape) {
            this.props.onClose && this.props.onClose();
        }
    }

    handleFormSubmit = event => {
        event.preventDefault();

        this.props.onApply && this.props.onApply(this.state);
    }

    handleRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: true
        });
    }

    handleTitleChange = (event, value) => {
        this.setState({
            title: value
        });
    }

    handleTypeChange = (event, key, value) => {
        this.setState({
            type: value
        });
    }

    handleColumnChange = (event, key, value) => {
        this.setState({
            column: value
        });
    }

    handleCancelRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });
    }

    handleApplyRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });

        setTimeout(() => {
            this.props.onRemove && this.props.onRemove();
        }, 300);
    }

    handleLinkChange = (fieldName, fieldValue) => {
        this.setState({
            [fieldName]: fieldValue
        });
    }

    get classes() {
        return classnames(classes.footerMenuLinkEditor, this.props.className);
    }

    get shouldRenderRemoveButton() {
        return this.props.menuItem;
    }

    renderRemoveWarningDialog() {
        let { removeWarningDialogShown } = this.state;

        let buttons = [
            <FlatButton label="No" onClick={this.handleCancelRemoveButtonClick} />,
            <FlatButton label="Yes" secondary={true} onClick={this.handleApplyRemoveButtonClick} />
        ];

        return (
            <Dialog title="Remove menu item" modal={true} open={removeWarningDialogShown} actions={buttons}>
                Are you sure you want to remove menu item?
            </Dialog>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === false)
            return;
        
        this.refs.title.focus();

        if (nextProps.menuItem) {
            this.setState({
                ...nextProps.menuItem
            });

            return;
        }

        this.setState({
            ...initialMenuItemState
        });
    }

    renderHeader() {
        let { menuItem } = this.props;

        return (
            <div className={classes.footerMenuItemEditorHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <BackIcon />
                </IconButton>

                <h2 className={classes.footerMenuItemEditorTitle}>
                    { menuItem ? menuItem.title : "Add menu item" }
                </h2>

                {
                    this.shouldRenderRemoveButton &&
                    <IconButton onClick={this.handleRemoveButtonClick}>
                        <DeleteIcon />
                    </IconButton>
                }
            </div>
        );
    }

    render() {
        let { open, pages } = this.props;
        let { title, type, column } = this.state;

        return (
            <Drawer open={open} width={460} containerClassName={classes.footerMenuItemEditorContainer}>
                { this.renderHeader() }

                <Scrollbars autoHide>
                    <form className={this.classes} onSubmit={this.handleFormSubmit} onKeyDown={this.handleKeyDown} tabIndex="0">
                        <TextField
                            ref="title"
                            value={title}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Title"
                            className={classes.footerMenuItemEditorTextField}
                            onChange={this.handleTitleChange}
                        />

                        <SelectField
                            value={type}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Type"
                            menuItemStyle={{fontSize: "14px"}}
                            className={classes.footerMenuItemEditorSelectField}
                            onChange={this.handleTypeChange}>

                            <MenuItem value="headline" primaryText="Headline" />
                            <MenuItem value="regular" primaryText="Regular" />
                        </SelectField>

                        <SelectField
                            value={column}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Column"
                            menuItemStyle={{fontSize: "14px"}}
                            className={classes.footerMenuItemEditorSelectField}
                            onChange={this.handleColumnChange}>

                            <MenuItem value="1" primaryText="1 column" />
                            <MenuItem value="2" primaryText="2 column" />
                            <MenuItem value="3" primaryText="3 column" />
                        </SelectField>

                        <MenuLinkEditor
                            link={this.state}
                            pages={pages}
                            className={classes.footerMenuItemEditorLinkEditor}
                            onChange={this.handleLinkChange}
                        />

                        <RaisedButton type="submit" label="Apply" primary={true} fullWidth={true} />

                        { this.renderRemoveWarningDialog() }
                    </form>
                </Scrollbars>
            </Drawer>
        );
    }
}