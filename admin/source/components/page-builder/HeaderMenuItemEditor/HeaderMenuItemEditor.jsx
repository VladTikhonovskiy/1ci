import React from "react";
import classnames from "classnames";
import { Scrollbars } from "react-custom-scrollbars";
import { bool, shape, string, number, oneOf, arrayOf, func } from "prop-types";
import { Drawer, Dialog, IconButton, FlatButton, RaisedButton, TextField } from "material-ui";

import { DeleteIcon, BackspaceIcon as BackIcon } from "images/icons/svg-icons";
import { MenuLinkEditor, HeaderMenuDropdownEditor } from "components/page-builder";

import classes from "./HeaderMenuItemEditor.less";
import { initialMenuItemState } from "./HeaderMenuItemEditor.settings";


const keys = {
    escape: 27
};

export default class HeaderMenuItemEditor extends React.PureComponent {
    static propTypes = {
        open: bool,
        showDropdownEditor: bool,
        pages: arrayOf(shape({
            id: number,
            name: string
        })),
        menuItem: shape({
            id: string,
            title: string,
            dropdown: bool,
            columnCount: number,
            columnTitles: arrayOf(string),
            parentMenuItemId: number,
            isLink: bool,
            linkType: oneOf(["page", "external-link"]),
            linkHref: string,
            linkPageId: number,
            linkTarget: oneOf(["_blank", "_self", "_parent", "_top"])
        }),
        className: string,
        onApply: func,
        onClose: func,
        onRemove: func
    }

    static defaultProps = {
        open: false,
        showDropdownEditor: true
    }

    state = {
        ...initialMenuItemState,
        removeWarningDialogShown: false,

        editableMenuItem: null,
        editMenuItemMode: false
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleKeyDown = event => {
        if (event.keyCode === keys.escape) {
            this.props.onClose && this.props.onClose();
        }
    }

    handleFormSubmit = event => {
        event.preventDefault();

        this.props.onApply && this.props.onApply(this.state);
    }

    handleRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: true
        });
    }

    handleTitleChange = (event, value) => {
        this.setState({
            title: value
        });
    }

    handleCancelRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });
    }

    handleApplyRemoveButtonClick = () => {
        this.setState({
            removeWarningDialogShown: false
        });

        setTimeout(() => {
            this.props.onRemove && this.props.onRemove();
        }, 300);
    }

    handleLinkChange = (fieldName, fieldValue) => {
        this.setState({
            [fieldName]: fieldValue
        });
    }

    handleDropdownChange = (fieldName, fieldValue) => {
        this.setState({
            [fieldName]: fieldValue
        });
    }

    handleFadil = () => {
        this.setState({
            editMenuItemMode: true
        });
    }

    get classes() {
        return classnames(classes.headerMenuLinkEditor, this.props.className);
    }

    get shouldRenderRemoveButton() {
        return this.props.menuItem;
    }

    renderRemoveWarningDialog() {
        let { removeWarningDialogShown } = this.state;

        let buttons = [
            <FlatButton label="No" onClick={this.handleCancelRemoveButtonClick} />,
            <FlatButton label="Yes" secondary={true} onClick={this.handleApplyRemoveButtonClick} />
        ];

        return (
            <Dialog title="Remove menu item" modal={true} open={removeWarningDialogShown} actions={buttons}>
                Are you sure you want to remove menu item?
            </Dialog>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open === false)
            return;

        this.refs.title.focus();

        if (nextProps.menuItem) {
            this.setState({
                ...nextProps.menuItem
            });

            return;
        }

        this.setState({
            ...initialMenuItemState
        });
    }

    renderHeader() {
        let { menuItem } = this.props;

        return (
            <div className={classes.headerMenuItemEditorHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <BackIcon />
                </IconButton>

                <h2 className={classes.headerMenuItemEditorTitle}>
                    { menuItem ? menuItem.title : "Add menu item" }
                </h2>

                {
                    this.shouldRenderRemoveButton &&
                    <IconButton onClick={this.handleRemoveButtonClick}>
                        <DeleteIcon />
                    </IconButton>
                }
            </div>
        );
    }

    render() {
        let { open, pages } = this.props;
        let { title } = this.state;

        let { editableMenuItem, editMenuItemMode } = this.state;

        return (
            <Drawer open={open} width={460} containerClassName={classes.headerMenuItemEditorContainer}>
                { this.renderHeader() }

                <Scrollbars autoHide>
                    <form className={this.classes} onSubmit={this.handleFormSubmit} onKeyDown={this.handleKeyDown} tabIndex="0">
                        <TextField
                            ref="title"
                            value={title}
                            fullWidth={true}
                            floatingLabelFixed={true}
                            floatingLabelText="Title"
                            className={classes.headerMenuItemEditorTextField}
                            onChange={this.handleTitleChange}
                        />

                        <MenuLinkEditor
                            link={this.state}
                            pages={pages}
                            className={classes.headerMenuItemEditorLinkEditor}
                            onChange={this.handleLinkChange}
                        />

                        {
                            this.props.showDropdownEditor &&
                            <HeaderMenuDropdownEditor
                                pages={pages}
                                menuItem={this.state}
                                onChange={this.handleDropdownChange}
                                onFadil={this.handleFadil}
                            />
                        }

                        <RaisedButton type="submit" label="Apply" primary={true} fullWidth={true} />

                        { this.renderRemoveWarningDialog() }
                    </form>
                </Scrollbars>

                {
                    this.props.showDropdownEditor &&
                    <HeaderMenuItemEditor
                        open={editMenuItemMode}
                        pages={pages}
                        menuItem={editableMenuItem}
                        showDropdownEditor={false}
                    />
                }
            </Drawer>
        );
    }
}