export const initialMenuItemState = {
    title: "",
    dropdown: false,
    columnCount: 2,
    columnTitles: ["", ""],
    parentMenuItemId: null,

    isLink: false,
    linkType: "page",
    linkHref: "",
    linkPageId: null,
    linkTarget: "_blank"
};