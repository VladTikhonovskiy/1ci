import React from "react";
import { string } from "prop-types";
import classnames from "classnames";
import { Link } from "react-router-dom";

import classes from "./BottomLine.less";
import { socialButtons } from "./BottomLine.settings";


export default class BottomLine extends React.PureComponent {
    static propTypes = {
        className: string
    }

    get classes() {
        return classnames(classes.bottomLine, this.props.className);
    }

    renderGeneralInfo() {
        return (
            <div className={classes.bottomLineGeneralInfo}>
                <span className={classes.bottomLineCopyrightText}>
                    © 2018 1C International
                </span>

                <Link to="/terms-of-use" className={classes.bottomLineLink}>
                    Terms of use
                </Link>

                <Link to="/privacy-policy" className={classes.bottomLineLink}>
                    Privacy policy
                </Link>
            </div>
        );
    }

    renderSocialButtons() {
        return (
            <div className={classes.bottomLineSocialButtons}>
                {
                    socialButtons.map(({ name, link, icon }) => (
                        <a
                            key={name}
                            href={link}
                            aria-label={name}
                            className={classes.bottomLineSocialButton}
                            style={{backgroundImage: `url('${icon}')`}}
                        >&nbsp;</a>
                    ))
                }
            </div>
        );
    }

    render() {
        let generalInfo = this.renderGeneralInfo();
        let socialButtons = this.renderSocialButtons();

        return (
            <div className={this.classes}>
                <div className={classes.bottomLineContainer}>
                    { generalInfo }
                    { socialButtons }
                </div>
            </div>
        );
    }
}