import {
    twitter,
    youtube,
    linkedin,
    facebook
} from "images/icons/social";


export const socialButtons = [
    {
        name: "linkedin",
        link: "http://linkedin.com",
        icon: linkedin,
    },
    {
        name: "youtube",
        link: "http://youtube.com",
        icon: youtube
    },
    {
        name: "facebook",
        link: "http://facebook.com",
        icon: facebook
    },
    {
        name: "twitter",
        link: "http://twitter.com",
        icon: twitter
    }
];