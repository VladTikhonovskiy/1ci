import React from "react";
import { Link } from "react-router-dom";
import { string } from "prop-types";
import classnames from "classnames";

import { Image } from "components/common";

import classes from "./Header.less";


export default class Header extends React.PureComponent {
    static propTypes = {
        className: string
    }

    get classes() {
        return classnames(classes.header, this.props.className);
    }

    renderMenu() {
        return (
            <React.Fragment>
                <Link to="/preview/applications" className={classes.headerMenuItem}>
                    Applications
                </Link>

                <Link to="/preview/company" className={classes.headerMenuItem}>
                    Company
                </Link>

                <Link to="/preview/developers" className={classes.headerMenuItem}>
                    Developers
                </Link>
            </React.Fragment>
        );
    }

    render() {
        let menu = this.renderMenu();

        return (
            <div className={this.classes}>
                <div className={classes.headerContainer}>
                    <Link to="/preview">
                        <Image name="logo" className={classes.headerLogo} />
                    </Link>

                    <div className={classes.headerMenu}>
                        { menu }
                    </div>
                </div>
            </div>
        );
    };
}