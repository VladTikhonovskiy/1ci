import Icon from "./Icon/Icon";
import Image from "./Image/Image";
import FlagIcon from "./FlagIcon/FlagIcon";
import Spinner from "./Spinner/Spinner";


export {
    Icon,
    Image,
    FlagIcon,
    Spinner
};