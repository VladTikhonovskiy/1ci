import React from "react";
import classnames from "classnames";
import { string } from "prop-types";
import { default as MaterialSpinner } from "react-spinner-material";

import classes from "./Spinner.less";


export default class Spinner extends React.PureComponent {
    static propTypes = {
        className: string
    }

    get classes() {
        return classnames(classes.spinner, this.props.className);
    }

    render() {
        return (
            <div className={this.classes}>
                <MaterialSpinner
                    size={30}
                    spinnerColor="#172b4e"
                    spinnerWidth={2}
                />
            </div>
        );
    }
}