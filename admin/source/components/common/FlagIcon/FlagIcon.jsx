import React from "react";
import classnames from "classnames";
import { string } from "prop-types";

import classes from "./FlagIcon.less";
import * as icons from "images/icons/flags";


export default class FlagIcon extends React.PureComponent {
    static propTypes = {
        country: string.isRequired,
        className: string
    }

    get classes() {
        return classnames(classes.flagIcon, this.props.className);
    }

    render() {
        let { country, ...props } = this.props;

        return (
            <i
                {...props}
                className={this.classes}
                dangerouslySetInnerHTML={{__html: icons[country]}}
            />
        );
    }
}