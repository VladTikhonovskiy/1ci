import React from "react";
import classnames from "classnames";
import { string } from "prop-types";

import classes from "./Icon.less";
import * as icons from "images/icons";


export default class Icon extends React.PureComponent {
    static propTypes = {
        name: string.isRequired,
        className: string
    }

    get classes() {
        return classnames(classes.icon, this.props.className);
    }

    render() {
        let { name, ...props } = this.props;

        return (
            <i
                {...props}
                className={this.classes}
                dangerouslySetInnerHTML={{__html: icons[name]}}
            />
        );
    }
}