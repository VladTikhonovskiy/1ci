import React from "react";
import classnames from "classnames";
import { string } from "prop-types";

import classes from "./Image.less";
import * as images from "images";


export default class Image extends React.PureComponent {
    static propTypes = {
        name: string.isRequired,
        className: string
    }

    get classes() {
        return classnames(classes.image, this.props.className);
    }

    render() {
        let { name, ...props } = this.props;

        return (
            <i
                {...props}
                className={this.classes}
                dangerouslySetInnerHTML={{__html: images[name]}}
            />
        );
    }
}