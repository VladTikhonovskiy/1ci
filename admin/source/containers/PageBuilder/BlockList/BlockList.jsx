import React from "react";
import classnames from "classnames";
import { string } from "prop-types";
import { connect } from "react-redux";
import { differenceBy } from "lodash";
import { Scrollbars } from "react-custom-scrollbars";
import { Divider } from "material-ui";

import { Block } from "components/page-builder";
import {
    EditIcon,
    CloseIcon,
    CloneIcon,
    ArrowUpIcon,
    ArrowDownIcon,
    PositionIcon,
    CloneItemOutsideIcon
} from "images/icons/svg-icons";

import classes from "./BlockList.less";
import * as actions from "modules/page-builder/page-builder.actions";
import * as selectors from "modules/page-builder/page-builder.selectors";


const positionControlsViewThreshold = 30;
const manipulationControlsViewThreshold = 50;

class BlockList extends React.Component {
    selectedBlockHeight = 0
    selectedBlockOffsetTop = 0
    selectedBlockOffsetBottom = 0

    static propTypes = {
        className: string
    }

    handleBlockMouseDown = blockID => () => {
        let blockRef = this.refs[`block-${blockID}`];

        this.selectedBlockHeight = blockRef.offsetHeight;
        this.selectedBlockOffsetTop = blockRef.offsetTop - this.refs.scrollbars.getScrollTop();

        this.props.selectBlock(blockID);
    }

    handleMoveBlockUpButtonClick = () => {
        if (this.isSelectedBlockFirst)
            return;

        this.props.moveSelectedBlock("up");
    }

    handleMoveBlockDownButtonClick = () => {
        if (this.isSelectedBlockLast)
            return;

        this.props.moveSelectedBlock("down");
    }

    handleBlockEditButtonClick = () => {
        this.props.showBlockSettings();
    }

    handleBlockCloneButtonClick = () => {
        this.props.cloneSelectedBlock();
    }

    handleBlockCloneOutsideButtonClick = () => {
        this.props.cloneSelectedBlockInfoSave();
        this.props.updatePageBlocks();
    }

    handleBlockRemoveButtonClick = () => {
        this.props.removeSelectedBlock();
    }

    handleScroll = () => {
        let blockRef = this.refs[`block-${this.props.selectedBlockID}`];

        if (blockRef) {
            this.selectedBlockHeight = blockRef.offsetHeight;
            this.selectedBlockOffsetTop = blockRef.offsetTop - this.refs.scrollbars.getScrollTop();
            this.selectedBlockOffsetBottom = this.selectedBlockOffsetTop + blockRef.offsetHeight;

            this.forceUpdate();
        }
    }

    isBlockSelected(blockID) {
        return this.props.selectedBlockID === blockID;
    }

    isBlockAdded(newSelectedPageBlocks) {
        let { selectedPageBlocks } = this.props;

        return (newSelectedPageBlocks.length - selectedPageBlocks.length === 1);
    }

    findAddedBlock(newSelectedPageBlocks) {
        let { selectedPageBlocks } = this.props;

        return differenceBy(newSelectedPageBlocks, selectedPageBlocks, block => block.id)[0];
    }

    getControlButtonsContainerStyle(offsetThreshold, heightThreshold) {
        let style = {
            top: this.selectedBlockHeight / 2 + this.selectedBlockOffsetTop
        };

        let isOverflowsTop =
            this.selectedBlockOffsetTop < offsetThreshold &&
            this.selectedBlockHeight < heightThreshold;

        let isOverflowsBottom =
            this.refs.scrollbars.getClientHeight() - this.selectedBlockOffsetBottom < offsetThreshold &&
            this.selectedBlockHeight < heightThreshold;

        if (isOverflowsTop) {
            style.top = 0;
            style.transform = "none";
        }

        if (isOverflowsBottom) {
            style.top = "auto";
            style.bottom = 0;
            style.transform = "none";
        }

        return style;
    }

    get classes() {
        return classnames(classes.blockList, this.props.className);
    }

    get shouldRenderBlockControls() {
        return this.props.selectedBlockID !== ""
    }

    get manipulationControlsStyle() {
        let threshold = manipulationControlsViewThreshold;
        return this.getControlButtonsContainerStyle(threshold, 200);
    }

    get positionControlsStyle() {
        let threshold = positionControlsViewThreshold;
        return this.getControlButtonsContainerStyle(threshold, 80);
    }

    get isSelectedBlockFirst() {
        let { selectedPageBlocks, selectedBlockID } = this.props;

        return selectedPageBlocks.findIndex(block => block.id === selectedBlockID) === 0;
    }

    get isSelectedBlockLast() {
        let { selectedPageBlocks, selectedBlockID } = this.props;

        return selectedPageBlocks.findIndex(block => block.id === selectedBlockID) === selectedPageBlocks.length - 1;
    }

    renderPositionControls() {
        return (
            <div className={classes.blockListPositionControls} style={this.positionControlsStyle}>
                <button className={classes.blockListMoveBlockButton} onClick={this.handleMoveBlockUpButtonClick}>
                    <ArrowUpIcon />
                </button>

                <PositionIcon className={classes.blockListPositionIcon} />

                <button className={classes.blockListMoveBlockButton} onClick={this.handleMoveBlockDownButtonClick}>
                    <ArrowDownIcon />
                </button>
            </div>
        );
    }

    renderManipulationControls() {
        return (
            <div className={classes.blockListManipulationControls} style={this.manipulationControlsStyle}>
                <button className={classes.blockListManipulationButton} onClick={this.handleBlockRemoveButtonClick}>
                    <CloseIcon />
                </button>

                <Divider className={classes.blockListManipulationControlsDivider} />

                <button className={classes.blockListManipulationButton} onClick={this.handleBlockCloneButtonClick}>
                    <CloneIcon />
                </button>

                <button className={classes.blockListManipulationButton} onClick={this.handleBlockEditButtonClick}>
                    <EditIcon />
                </button>

                <button className={classes.blockListManipulationButton} onClick={this.handleBlockCloneOutsideButtonClick}>
                    <CloneItemOutsideIcon />
                </button>
            </div>
        );
    }

    componentWillReceiveProps(nextProps) {
        let blockRef = this.refs[`block-${nextProps.selectedBlockID}`];

        if (blockRef) {
            requestAnimationFrame(() => {
                this.selectedBlockHeight = blockRef.offsetHeight;
                this.selectedBlockOffsetTop = blockRef.offsetTop - this.refs.scrollbars.getScrollTop();
                this.selectedBlockOffsetBottom = this.selectedBlockOffsetTop + blockRef.offsetHeight;

                this.forceUpdate();
            });
        }

        if (this.isBlockAdded(nextProps.selectedPageBlocks)) {
            let addedBlock = this.findAddedBlock(nextProps.selectedPageBlocks);

            requestAnimationFrame(() => {
                let addedBlockRef = this.refs[`block-${addedBlock.id}`];

                this.refs.scrollbars.scrollTop(addedBlockRef.offsetTop);
            });
        }
    }

    render() {
        return (
            <div className={this.classes}>
                <Scrollbars autoHide ref="scrollbars" onScroll={this.handleScroll}>
                    {
                        this.props.selectedPageBlocks.map(({ id, name, settings }) => (
                            <div ref={`block-${id}`} key={id}>
                                <Block
                                    name={name}
                                    settings={settings}
                                    selected={this.isBlockSelected(id)}
                                    onMouseDown={this.handleBlockMouseDown(id)}
                                />
                            </div>
                        ))
                    }
                </Scrollbars>

                {
                    this.shouldRenderBlockControls &&
                    <React.Fragment>
                        { this.renderPositionControls() }
                        { this.renderManipulationControls() }
                    </React.Fragment>
                }
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        selectedBlockID: pageBuilder.selectedBlockID,

        cloneBlockInfo: pageBuilder.cloneBlockInfo.selectedBlockID,

        selectedPageBlocks: selectors.selectedPageBlocks(pageBuilder)
    };
}

export default connect(mapStateToProps, { ...actions })(BlockList);