import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import ReactDOMServer from "react-dom/server";
import { Scrollbars } from "react-custom-scrollbars";
import * as pretty from "pretty";
import { Drawer } from "material-ui";
import { Tabs, Tab } from "material-ui/Tabs";

import BlockList from "./BlockList/BlockList";
import BlockListHeader from "./BlockListHeader/BlockListHeader";
import PageTemplateHeader from "./PageTemplateHeader/PageTemplateHeader";
import PageSettings from "./PageSettings/PageSettings";
import PageSelector from "./PageSelector/PageSelector";
import HeaderMenuSettings from "./HeaderMenuSettings/HeaderMenuSettings";
import FooterMenuSettings from "./FooterMenuSettings/FooterMenuSettings";
import PageCodeDialog from "./PageCodeDialog/PageCodeDialog";

import { BlockSelector, BlockSettings, PagePreview, PageTemplate, PureBlocks } from "components/page-builder";

import classes from "./PageBuilder.less";
import * as appActions from "modules/app/app.actions";
import * as pageBuilderActions from "modules/page-builder/page-builder.actions";
import * as pageBuilderSelectors from "modules/page-builder/page-builder.selectors";


class PageBuilder extends React.Component {
    handleContentMouseDown = e => {
        let { resetBlockSelection, hideBlockSettings } = this.props;

        let resetTriggerElementsClasses = [
            classes.pageBuilderBlockListHeader,
            classes.pageBuilderBlockListContainer,
            classes.pageBuilderPageTemplateHeader,
            classes.pageBuilderPageTemplateContainer
        ];

        if (resetTriggerElementsClasses.some(className => e.target.classList.contains(className))) {
            resetBlockSelection();
            hideBlockSettings();
        }
    }

    handleClick = () => {
        this.props.hideNavigation();
    }

    handleBlockSelect = block => {
        this.props.createBlock(block);
    }

    handleBlockSettingsClose = () => {
        this.props.hideBlockSettings();
    }

    handleBlockSettingsApply = () => {
        this.props.hideBlockSettings();
    }

    handlePageTemplateSettingsApply = () => {
        this.props.resetPageTemplateSectionSelection();
    }

    handlePageTemplateSettingsClose = () => {
        this.props.resetPageTemplateSectionSelection();
    }

    handleBlockSettingsChange = settings => {
        this.props.changeBlockSettings(settings);
    }

    handlePageTabClick = event => {
        let tabLabel = event.props.label;
        let { selectedTabName, selectTab } = this.props;

        selectTab(tabLabel);

        if (tabLabel === "pages" && selectedTabName === "pages") {
            this.props.deselectPage();
        }
    }

    handlePageBuilderPanelEmptySpaceClick = event => {
        if(event.target.classList.contains(classes.pageBuilderPageSelector)) {
            this.props.deselectPage();
        }
    }

    handleSectionSelect = section => {
        this.props.selectPageTemplateSection(section);
    }

    handleShowPageBuilderNavigation = () => {
        this.props.showPageBuilderNavigation();
    }

    handleHidePageBuilderNavigation = () => {
        this.props.hidePageBuilderNavigation()
    }

    componentDidMount() {
        this.props.fetchPageList();
        this.props.getUrlList();
    }

    renderBlockSettings() {
        let { selectedBlock, blockSettingsShown } = this.props;

        return (
            <BlockSettings
                block={selectedBlock}
                open={blockSettingsShown}
                onClose={this.handleBlockSettingsClose}
                onApply={this.handleBlockSettingsApply}
                onSettingsChange={this.handleBlockSettingsChange}
            />
        );
    }

    render() {
        let {
            selectedPage,
            selectedPageBlocks,
            pagePreviewShown,
            hidePagePreview,
            isPageTemplateSelected,
            selectedPageTemplateSection,
            headerMenu,
            footerMenu,
            footerSocialLinks,
            footerSecondaryLinks,
            pageBuilderNavigationShown,
            selectedTabName
        } = this.props;

        let contentContainerClasses = classnames({
            [classes.pageBuilderTabPanelContainer] : true,
            [classes.pageBuilderTabPagesActive] : selectedTabName === "pages",
            [classes.pageBuilderTabInsertActive] : selectedTabName === "insert",
        });

        let pureBlocksCode = ReactDOMServer.renderToString(<PureBlocks blocks={selectedPageBlocks} />);
        let formattedBlocksCode = pretty(pureBlocksCode.replace(/<!-- -->/g, ""));

        return (
            <div className={classes.pageBuilder} onClick={this.handleClick}>
                <div
                    className={classes.pageBuilderContent}
                    onMouseDown={this.handleContentMouseDown}
                    ref="contentContainer">
                    {
                        selectedPage &&
                        <React.Fragment>
                            <BlockListHeader className={classes.pageBuilderBlockListHeader} />

                            <div className={classes.pageBuilderBlockListContainer}>
                                <BlockList className={classes.pageBuilderBlockList} />
                            </div>
                        </React.Fragment>
                    }

                    {
                        isPageTemplateSelected &&
                        <React.Fragment>
                            <PageTemplateHeader className={classes.pageBuilderPageTemplateHeader} />

                            <div className={classes.pageBuilderPageTemplateContainer}>
                                <PageTemplate
                                    selectedSection={selectedPageTemplateSection}
                                    headerMenu={headerMenu}
                                    footerMenu={footerMenu}
                                    footerSocialLinks={footerSocialLinks}
                                    footerSecondaryLinks={footerSecondaryLinks}
                                    className={classes.pageBuilderPageTemplate}
                                    onSectionSelect={this.handleSectionSelect}
                                />
                            </div>
                        </React.Fragment>
                    }
                </div>

                <Drawer width={360} openSecondary={true} open={pageBuilderNavigationShown}>
                    <div onClick={this.handlePageBuilderPanelEmptySpaceClick} onMouseLeave={this.handleHidePageBuilderNavigation} >
                        <Tabs className={classes.pageBuilderTabPanel} contentContainerClassName={contentContainerClasses} tabTemplateStyle={{height: "100%"}}>
                            <Tab onActive={this.handlePageTabClick} className={classes.pageBuilderTabActive} label="pages">
                                <Scrollbars autoHide>
                                    <PageSelector className={classes.pageBuilderPageSelector} />
                                </Scrollbars>
                            </Tab>

                            <Tab onActive={this.handlePageTabClick} label="insert">
                                <Scrollbars autoHide>
                                    <BlockSelector onBlockSelect={this.handleBlockSelect} />
                                </Scrollbars>
                            </Tab>
                        </Tabs>
                    </div>
                </Drawer>

                <PageSettings className={classes.pageBuilderPageSettings} />

                <FooterMenuSettings
                    open={selectedPageTemplateSection === "footer"}
                    onClose={this.handlePageTemplateSettingsClose}
                    onApply={this.handlePageTemplateSettingsApply}
                />

                <HeaderMenuSettings
                    open={selectedPageTemplateSection === "header"}
                    onClose={this.handlePageTemplateSettingsClose}
                    onApply={this.handlePageTemplateSettingsApply}
                />

                { this.renderBlockSettings() }

                {
                    pagePreviewShown &&
                    <PagePreview blocks={selectedPageBlocks} onClose={hidePagePreview} />
                }

                <div className={classes.pageBuilderMenuTrigger} onMouseEnter={this.handleShowPageBuilderNavigation}></div>

                <PageCodeDialog codeText={formattedBlocksCode} />
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        pagePreviewShown: pageBuilder.pagePreviewShown,
        blockSettingsShown: pageBuilder.blockSettingsShown,
        pageBuilderNavigationShown: pageBuilder.pageBuilderNavigationShown,

        selectedTabName: pageBuilder.selectedTabName,
        isPageTemplateSelected: pageBuilder.isPageTemplateSelected,
        selectedPageTemplateSection: pageBuilder.selectedPageTemplateSection,

        headerMenu: pageBuilder.headerMenu,
        footerMenu: pageBuilder.footerMenu,
        footerSocialLinks: pageBuilder.footerSocialLinks,
        footerSecondaryLinks: pageBuilder.footerSecondaryLinks,

        selectedPage: pageBuilderSelectors.selectedPage(pageBuilder),
        selectedBlock: pageBuilderSelectors.selectedBlock(pageBuilder),
        selectedPageBlocks: pageBuilderSelectors.selectedPageBlocks(pageBuilder)
    };
}

export default connect(mapStateToProps, { ...pageBuilderActions, ...appActions })(PageBuilder);