import React from "react";
import { connect } from "react-redux";
import { string } from "prop-types";
import hl from "highlightjs";
import { Dialog, FlatButton } from "material-ui";

import * as pageBuilderActions from "modules/page-builder/page-builder.actions";


const customContentStyle = {
    width: "70%",
    maxWidth: "none"
};

class PageCodeDialog extends React.Component {
    static propTypes = {
        codeText: string
    }

    handleClose = () => {
        this.props.hidePageCodeDialog();
    };

    render() {
        let { codeText, pageCodeDialogShown } = this.props;

        let actions = [
            <FlatButton
                label="Close"
                primary={true}
                onClick={this.handleClose}
            />,
        ];

        let styledCodeText = hl.highlight("html", codeText).value;

        return (
            <Dialog
                title="Code"
                actions={actions}
                modal={false}
                open={pageCodeDialogShown}
                onRequestClose={this.handleClose}
                autoScrollBodyContent={true}
                contentStyle={customContentStyle}>

                    <pre dangerouslySetInnerHTML={{__html: styledCodeText }}></pre>
            </Dialog>
        );
    }
}

function mapStateToProps({ pageBuilder }) {
    return {
        pageCodeDialogShown: pageBuilder.pageCodeDialogShown,
    };
}

export default connect(mapStateToProps, { ...pageBuilderActions })(PageCodeDialog);