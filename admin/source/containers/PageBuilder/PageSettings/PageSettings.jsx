import React from "react";
import classnames from "classnames";
import { string } from "prop-types";
import { connect } from "react-redux";
import { Scrollbars } from "react-custom-scrollbars";
import { Drawer, Divider, TextField, Checkbox, IconButton, RaisedButton } from "material-ui";

import { pageBuilder } from "config";
import { ImageSelector } from "components/page-builder";
import { CloseIcon, CheckIcon } from "images/icons/svg-icons";

import classes from "./PageSettings.less";
import * as applicationActions from "modules/app/app.actions";
import * as pageBuilderActions from "modules/page-builder/page-builder.actions";
import * as pageBuilderSelectors from "modules/page-builder/page-builder.selectors";


class PageSettings extends React.PureComponent {
    static propTypes = {
        className: string
    }

    handleSettingsChange = fieldName => event => {
        let { selectedPage, updatePageSettings } = this.props;

        let updatedPage = {
            ...selectedPage,
            version: {
                ...selectedPage.version,
                [fieldName]: event.target.value
            }
        };

        updatePageSettings(updatedPage);
    }

    handlePageImageChange = imageUrl => {
        let { selectedPage, updatePageSettings } = this.props;

        let updatedPage = {
            ...selectedPage,
            image: imageUrl
        };

        updatePageSettings(updatedPage);
    }

    handleIndexValueChange = fieldName => (event, value) => {
        let { selectedPage, updatePageSettings } = this.props;

        let updatedPage = {
            ...selectedPage,
            [fieldName]: value
        };

        updatePageSettings(updatedPage);
    }

    handleRemovePageButtonClick = () => {
        this.props.removeSelectedPage();
    }

    get classes() {
        return classnames(classes.pageSettings, this.props.className);
    }

    renderHeader() {
        let { hidePageSettings } = this.props;

        return (
            <div className={classes.pageSettingsHeader}>
                <IconButton onClick={hidePageSettings}>
                    <CloseIcon />
                </IconButton>

                <h2 className={classes.pageSettingsTitle}>
                    Page settings
                </h2>

                <IconButton onClick={hidePageSettings}>
                    <CheckIcon />
                </IconButton>
            </div>
        );
    }

    renderMetaSettings() {
        let { selectedPage } = this.props;
        let { metaTitle, metaKeywords, metaDescription } = selectedPage.version;

        return (
            <React.Fragment>
                <p className={classes.pageSettingsSubtitle}> Meta </p>
                <Divider className={classes.pageSettingsDivider} />

                <TextField
                    fullWidth={true}
                    value={metaTitle || ""}
                    floatingLabelText="Title"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("metaTitle")}
                />

                <TextField
                    rows={3}
                    rowsMax={6}
                    multiLine={true}
                    fullWidth={true}
                    value={metaDescription || ""}
                    floatingLabelText="Description"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("metaDescription")}
                />

                <TextField
                    fullWidth={true}
                    value={metaKeywords || ""}
                    floatingLabelText="Keywords"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("metaKeywords")}
                />
            </React.Fragment>
        );
    }

    renderSocialShareSettings() {
        let { selectedPage, uploadImage, showMessage } = this.props;

        let { image } = selectedPage;
        let { baseUrl, maxFileSize } = pageBuilder.imageSelector;
        let { socialTitle, socialDescription } = selectedPage.version;

        let imageUrl = image || "";

        return (
            <React.Fragment>
                <p className={classes.pageSettingsSubtitle}> Social share link </p>
                <Divider className={classes.pageSettingsDivider} />

                <TextField
                    fullWidth={true}
                    value={socialTitle || ""}
                    floatingLabelText="Title"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("socialTitle")}
                />

                <TextField
                    rows={3}
                    rowsMax={6}
                    multiLine={true}
                    fullWidth={true}
                    value={socialDescription || ""}
                    floatingLabelText="Description"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("socialDescription")}
                />

                <ImageSelector
                    imageUrl={imageUrl}
                    imageBaseUrl={baseUrl}
                    maxImageSize={maxFileSize}
                    uploadAction={uploadImage}
                    messageAction={showMessage}
                    className={classes.pageSettingsImageSelector}
                    onImageChange={this.handlePageImageChange}
                />
            </React.Fragment>
        );
    }

    renderIndexSettings() {
        let { noindex, nofollow, hideFromSitemap } = this.props.selectedPage;

        let labelStyle = {
            width: "auto",
            marginLeft: "-8px"
        };

        return (
            <div className={classes.pageSettingsIndexSettings}>
                <Checkbox
                    label="No index"
                    checked={noindex}
                    labelStyle={labelStyle}
                    className={classes.pageSettingsIndexSettingsCheckbox}
                    onCheck={this.handleIndexValueChange("noindex")}
                />

                <Checkbox
                    label="No follow"
                    checked={nofollow}
                    labelStyle={labelStyle}
                    className={classes.pageSettingsIndexSettingsCheckbox}
                    onCheck={this.handleIndexValueChange("nofollow")}
                />

                <Checkbox
                    label="Hide from sitemap"
                    checked={hideFromSitemap}
                    labelStyle={labelStyle}
                    className={classes.pageSettingsIndexSettingsCheckbox}
                    onCheck={this.handleIndexValueChange("hideFromSitemap")}
                />
            </div>
        );
    }

    renderSettings() {
        let { selectedPage } = this.props;
        let { url, name } = selectedPage.version;

        let pageUrl = url || "";
        let hintText = "/" + name.split(" ").join("-").toLowerCase();

        return (
            <div className={classes.pageSettingsContent}>
                <TextField
                    value={name}
                    fullWidth={true}
                    floatingLabelText="Page name"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("name")}
                />

                <TextField
                    value={pageUrl}
                    fullWidth={true}
                    hintText={hintText}
                    floatingLabelText="URL"
                    floatingLabelFixed={true}
                    onChange={this.handleSettingsChange("url")}
                />

                { this.renderMetaSettings() }
                { this.renderSocialShareSettings() }
                { this.renderIndexSettings() }

                <RaisedButton
                    label="Remove page"
                    secondary={true}
                    fullWidth={true}
                    className={classes.pageSettingsRemovePageButton}
                    onClick={this.handleRemovePageButtonClick}
                />
            </div>
        );
    }

    render() {
        let { selectedPage, pageSettingsShown } = this.props;

        if (!selectedPage)
            return null;

        return (
            <div className={this.classes}>
                <Drawer width={460} containerClassName={classes.pageSettingsContainer} open={pageSettingsShown}>
                    { this.renderHeader() }

                    <Scrollbars autoHide>
                        { this.renderSettings() }
                    </Scrollbars>
                </Drawer>
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        pageSettingsShown: pageBuilder.pageSettingsShown,

        selectedPage: pageBuilderSelectors.selectedPage(pageBuilder)
    };
}

export default connect(mapStateToProps, { ...applicationActions, ...pageBuilderActions })(PageSettings);