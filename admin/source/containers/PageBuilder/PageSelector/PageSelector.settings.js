const animationDuration = 150;

export const animations = {
    toggle: ({ node }) => {
        let { toggled } = node;

        return {
            animation: {
                rotateY: toggled ? 180 : 0,
            },

            duration: animationDuration
        }
    },

    drawer: () => ({
        enter: {
            animation: "slideDown",
            duration: animationDuration
        },
        leave: {
            animation: "slideUp",
            duration: animationDuration
        }
    })
};

export const style = {
    tree: {
        base: {
            margin: 0,
            padding: 0,
            backgroundColor: "transparent",
            color: "black",
            fontSize: "15px",
            listStyle: "none",
        },

        node: {
            base: {
                position: "relative",
                cursor: "pointer"
            },

            link: {
                cursor: "pointer",
                position: "relative"
            },

            activeLink: {},

            toggle: {
                base: {
                    position: "relative",
                    display: "inline-block",
                    verticalAlign: "top",
                    marginLeft: "-5px",
                    height: "24px",
                    width: "24px"
                },
                wrapper: {
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    margin: "-7px 0 0 -7px",
                    height: "14px"
                },
                height: 14,
                width: 14,
                arrow: {
                    fill: "#9da5ab",
                    strokeWidth: 0
                }
            },

            header: {
                base: {
                    display: "inline-block",
                    verticalAlign: "top",
                    color: "#9da5ab"
                },
                connector: {
                    width: "2px",
                    height: "12px",
                    borderLeft: "solid 2px black",
                    borderBottom: "solid 2px black",
                    position: "absolute",
                    top: "0px",
                    left: "-21px"
                },
                title: {
                    lineHeight: "24px",
                    verticalAlign: "middle"
                }
            },

            subtree: {
                listStyle: "none",
                paddingLeft: "0"
            },

            loading: {
                color: "#e2c089"
            }
        }
    }
};