import React from "react";
import classnames from "classnames";
import { string } from "prop-types";
import { connect } from "react-redux";
import { Treebeard } from "react-treebeard";
import { FloatingActionButton } from "material-ui";

import { Spinner } from "components/common";
import { PageIcon, ToggleIcon, HomePageIcon, CreatePageIcon, PageTemplateIcon } from "images/icons/svg-icons";

import classes from "./PageSelector.less";
import { animations, style } from "./PageSelector.settings";
import * as actions from "modules/page-builder/page-builder.actions";
import * as selectors from "modules/page-builder/page-builder.selectors";


const defaultPageName = "Page";

class PageSelector extends React.Component {
    static propTypes = {
        className: string,
    }

    handlePageNodeClick = page => () => {
        this.props.selectPage(page);
    }

    handleToggleButtonClick = page => event => {
        event.stopPropagation();

        this.props.togglePage(page);
    }

    handleCreateNewPageButtonClick = () => {
        this.props.createPage();
    }

    handleSelectPageTemplateButtonClick = () => {
        this.props.selectPageTemplate();
    }

    isPageSelected(pageID) {
        return this.props.selectedPageID === pageID;
    }

    get classes() {
        return classnames(classes.pageSelector, this.props.className);
    }

    get decorators() {
        return {
            Toggle: props => {
                let { node: page, node: { toggled, children } } = props;

                if (children === null || children.length === 0)
                    return null;

                let iconClasses = classnames(classes.pageSelectorToggleButtonIcon, {
                    [classes.pageSelectorToggleButtonIconToggled]: toggled
                });
        
                return (
                    <div role="button" className={classes.pageSelectorToggleButton} onClick={this.handleToggleButtonClick(page)}>
                        <ToggleIcon className={iconClasses} />
                    </div>
                );
            },
        
            Header: props => {
                let { isHome: isHomePage, version } = props.node;

                let pageName = version.name || defaultPageName;

                return (
                    <div className={classes.pageSelectorPageTitle}>
                        {
                            isHomePage ?
                            <HomePageIcon className={classes.pageSelectorPageTitleIcon} /> :
                            <PageIcon className={classes.pageSelectorPageTitleIcon}/>
                        }
        
                        { pageName }
                    </div>
                );
            },
        
            Container: props => {
                let { node: page, node: { id, level, children } } = props;

                let style = {
                    paddingLeft: level * 28
                };

                if (children === null || children.length === 0) {
                    style.paddingLeft += 28;
                }

                let containerClasses = classnames(classes.pageSelectorPage, {
                    [classes.pageSelectorPageSelected]: this.isPageSelected(id)
                });
        
                return (
                    <button className={containerClasses} style={style} onClick={this.handlePageNodeClick(page)}>
                        <props.decorators.Toggle {...props} />
                        <props.decorators.Header {...props} />
                    </button>
                );
            }
        };
    }

    renderPageTemplateButton() {
        let { isPageTemplateSelected } = this.props;

        let pageTemplateButtonClasses = classnames(classes.pageSelectorPageTemplateButton, {
            [classes.pageSelectorPageTemplateButtonSelected]: isPageTemplateSelected
        });

        return (
            <button className={pageTemplateButtonClasses} onClick={this.handleSelectPageTemplateButtonClick}>
                <PageTemplateIcon className={classes.pageSelectorPageTemplateButtonIcon} />

                Template
            </button>
        );
    }

    render() {
        let { pageListTreeView, isPageListFetching } = this.props;

        return (
            <div className={this.classes}>
                { this.renderPageTemplateButton() }

                <Treebeard
                    style={style}
                    data={pageListTreeView}
                    animations={animations}
                    decorators={this.decorators}
                />

                <FloatingActionButton className={classes.pageSelectorCreatePageButton} onClick={this.handleCreateNewPageButtonClick}>
                    <CreatePageIcon />
                </FloatingActionButton>

                {
                    isPageListFetching &&
                    <Spinner className={classes.pageSelectorPreloader}/>
                }
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        selectedPageID: pageBuilder.selectedPageID,
        isPageTemplateSelected: pageBuilder.isPageTemplateSelected,

        pageListTreeView: selectors.pageListTreeView(pageBuilder),
        isPageListFetching: selectors.isPageListFetching(pageBuilder)
    };
}

export default connect(mapStateToProps, { ...actions })(PageSelector);