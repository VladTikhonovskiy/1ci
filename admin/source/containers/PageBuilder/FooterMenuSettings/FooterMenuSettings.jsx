import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import { bool, string, func } from "prop-types";
import { Scrollbars } from "react-custom-scrollbars";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import { Drawer, IconButton } from "material-ui";

import { Icon } from "components/common";
import { CloseIcon, CheckIcon } from "images/icons/svg-icons";
import { MenuListItem, FooterMenuItemEditor, FooterMenuSocialLinkEditor, FooterMenuSecondaryLinkEditor } from "components/page-builder";

import classes from "./FooterMenuSettings.less";
import * as applicationActions from "modules/app/app.actions";
import * as pageBuilderActions from "modules/page-builder/page-builder.actions";


const columns = [1, 2, 3];

class FooterMenuSettings extends React.Component {
    static propTypes = {
        open: bool,
        className: string,
        onClose: func,
        onApply: func
    }

    static defaultProps ={
        open: false
    }

    state = {
        editableMenuItem: null,
        editMenuItemMode: false,
        editableSocialLink: null,
        editSocialLinkMode: false,
        editableSecondaryLink: null,
        editSecondaryLinkMode: false,
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleApplyButtonClick = () => {
        this.props.onApply && this.props.onApply();
    }

    handleAddMenuItemButtonClick = () => {
        this.setState({
            editMenuItemMode: true
        });
    }

    handleAddSocialLinkButtonClick = () => {
        this.setState({
            editSocialLinkMode: true
        });
    }

    handleAddSecondaryLinkButtonClick = () => {
        this.setState({
            editSecondaryLinkMode: true
        });
    }

    handleMenuListItemClick = menuItem => () => {
        this.setState({
            editableMenuItem: menuItem,
            editMenuItemMode: true
        });
    }

    handleSocialLinkListItemClick = socialLink => () => {
        this.setState({
            editableSocialLink: socialLink,
            editSocialLinkMode: true
        });
    }

    handleSecondaryLinkListItemClick = secondaryLink => () => {
        this.setState({
            editableSecondaryLink: secondaryLink,
            editSecondaryLinkMode: true
        });
    }

    handleMenuItemAddEdit = menuItem => {
        let { addFooterMenuItem, editFooterMenuItem } = this.props;

        this.state.editableMenuItem ? 
            editFooterMenuItem(menuItem) :
            addFooterMenuItem(menuItem);

        this.setState({
            editMenuItemMode: false
        });
    }

    handleSocialLinkAddEdit = socialLink => {
        let { addFooterSocialLink, editFooterSocialLink } = this.props;

        this.state.editableSocialLink ?
            editFooterSocialLink(socialLink) :
            addFooterSocialLink(socialLink);

        this.setState({
            editSocialLinkMode: false
        });
    }

    handleSecondaryLinkAddEdit = secondaryLink => {
        let { addFooterSecondaryLink, editFooterSecondaryLink } = this.props;

        this.state.editableSecondaryLink ?
            editFooterSecondaryLink(secondaryLink) :
            addFooterSecondaryLink(secondaryLink);

        this.setState({
            editSecondaryLinkMode: false
        });
    }

    handleMenuItemRemove = () => {
        this.props.removeFooterMenuItem(this.state.editableMenuItem);

        this.setState({
            editableMenuItem: null,
            editMenuItemMode: false
        });
    }

    handleSocialLinkRemove = () => {
        this.props.removeFooterSocialLink(this.state.editableSocialLink);

        this.setState({
            editableSocialLink: null,
            editSocialLinkMode: false
        });
    }

    handleSecondaryLinkRemove = () => {
        this.props.removeFooterSecondaryLink(this.state.editableSecondaryLink);

        this.setState({
            editableSecondaryLink: null,
            editSecondaryLinkMode: false
        });
    }

    handleMenuItemEditorClose = () => {
        this.setState({
            editableMenuItem: null,
            editMenuItemMode: false
        });
    }

    handleSocialLinkEditorClose = () => {
        this.setState({
            editableSocialLink: null,
            editSocialLinkMode: false
        });
    }

    handleSecondaryLinkEditorClose = () => {
        this.setState({
            editableSecondaryLink: null,
            editSecondaryLinkMode: false
        });
    }

    handleMenuItemSortEnd = column => info => {
        this.props.moveFooterMenuItem({
            column,
            ...info
        });
    }

    handleSocialLinkSortEnd = info => {
        this.props.moveFooterSocialLink(info);
    }

    handleSecondaryLinkSortEnd = info => {
        this.props.moveFooterSecondaryLink(info);
    }

    get classes() {
        return classnames(classes.footerMenuSettings, this.props.className);
    }

    renderSectionTitle(title) {
        return (
            <label className={classes.footerMenuSettingsSectionTitle}>
                { title }
            </label>
        );
    }

    renderSectionAddButton(label, handler) {
        return (
            <button className={classes.footerMenuSettingsAddItemButton} onClick={handler}>
                <Icon name="plus" className={classes.footerMenuSettingsAddItemButtonIcon} />
                { label }
            </button>
        );
    }

    renderMenuListItems(column) {
        let columnMenuItems = this.props.footerMenu.filter(menuItem => menuItem.column === column.toString());

        return columnMenuItems.map(menuItem => (
            <MenuListItem
                key={menuItem.id}
                item={menuItem}
                className={classes.footerMenuSettingsMenuListItem}
                onClick={this.handleMenuListItemClick(menuItem)}
            />
        ));
    }

    renderSocialLinkItems() {
        return this.props.footerSocialLinks.map(socialLink => (
            <MenuListItem
                key={socialLink.id}
                item={socialLink}
                className={classes.footerMenuSettingsMenuListItem}
                onClick={this.handleSocialLinkListItemClick(socialLink)}
            />
        ));
    }

    renderSecondaryLinkItems() {
        return this.props.footerSecondaryLinks.map(secondaryLink => (
            <MenuListItem
                key={secondaryLink.id}
                item={secondaryLink}
                className={classes.footerMenuSettingsMenuListItem}
                onClick={this.handleSecondaryLinkListItemClick(secondaryLink)}
            />
        ));
    }

    renderHeader() {
        return (
            <div className={classes.footerMenuSettingsHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <CloseIcon />
                </IconButton>

                <h2 className={classes.footerMenuSettingsTitle}>
                    Footer navigation edit
                </h2>

                <IconButton onClick={this.handleApplyButtonClick}>
                    <CheckIcon />
                </IconButton>
            </div>
        );
    }

    renderContent() {
        let SortableItem = SortableElement(item => item.value);

        let SortableList = SortableContainer(({ items }) => {
            return (
                <div>
                    {
                        items.map((item, index) => (
                            <SortableItem key={`item-${index}`} index={index} value={item} />
                        ))
                    }
                </div>
            );
        });

        let columnElements = columns.map(column => (
            <div key={column} className={classes.footerMenuSettingsItemList}>
                <label className={classes.footerMenuSettingsColumnLabel}>
                    { column } column
                </label>

                <SortableList
                    items={this.renderMenuListItems(column)}
                    useDragHandle={true}
                    helperClass={classes.footerMenuSettingsSortableHelper}
                    onSortEnd={this.handleMenuItemSortEnd(column)}
                />
            </div>
        ));

        return (
            <div className={classes.footerMenuSettingsContent}>
                { this.renderSectionTitle("Main navigation") }
                { this.renderSectionAddButton("Add menu item", this.handleAddMenuItemButtonClick) }
                { columnElements }

                { this.renderSectionTitle("Social links") }
                { this.renderSectionAddButton("Add social link", this.handleAddSocialLinkButtonClick) }

                <div className={classes.footerMenuSettingsItemList}>
                    <SortableList
                        items={this.renderSocialLinkItems()}
                        useDragHandle={true}
                        helperClass={classes.footerMenuSettingsSortableHelper}
                        onSortEnd={this.handleSocialLinkSortEnd}
                    />
                </div>

                { this.renderSectionTitle("Secondary links") }
                { this.renderSectionAddButton("Add secondary link", this.handleAddSecondaryLinkButtonClick) }

                <div className={classes.footerMenuSettingsItemList}>
                    <SortableList
                        items={this.renderSecondaryLinkItems()}
                        useDragHandle={true}
                        helperClass={classes.footerMenuSettingsSortableHelper}
                        onSortEnd={this.handleSecondaryLinkSortEnd}
                    />
                </div>
            </div>
        );
    }

    shouldComponentUpdate(nextProps) {
        if (this.props.open === false && nextProps.open === false)
            return false;

        return true;
    }

    render() {
        let { pageList, uploadImage, showMessage } = this.props;

        let {
            editableMenuItem, editMenuItemMode,
            editableSocialLink, editSocialLinkMode,
            editableSecondaryLink, editSecondaryLinkMode
        } = this.state;

        return (
            <div className={this.classes}>
                <Drawer width={460} containerClassName={classes.footerMenuSettingsContainer} open={this.props.open}>
                    { this.renderHeader() }

                    <Scrollbars autoHide>
                        { this.renderContent() }
                    </Scrollbars>
                </Drawer>

                <FooterMenuItemEditor
                    open={editMenuItemMode}
                    pages={pageList}
                    menuItem={editableMenuItem}
                    className={classes.footerMenuSettingsItemEditor}
                    onApply={this.handleMenuItemAddEdit}
                    onClose={this.handleMenuItemEditorClose}
                    onRemove={this.handleMenuItemRemove}
                />

                <FooterMenuSocialLinkEditor
                    open={editSocialLinkMode}
                    pages={pageList}
                    socialLink={editableSocialLink}
                    className={classes.footerMenuSettingsSocialLinkEditor}
                    onApply={this.handleSocialLinkAddEdit}
                    onClose={this.handleSocialLinkEditorClose}
                    onRemove={this.handleSocialLinkRemove}
                    uploadIconAction={uploadImage}
                    showMessageAction={showMessage}
                />

                <FooterMenuSecondaryLinkEditor
                    open={editSecondaryLinkMode}
                    pages={pageList}
                    secondaryLink={editableSecondaryLink}
                    className={classes.footerMenuSettingsSecondaryLinkEditor}
                    onApply={this.handleSecondaryLinkAddEdit}
                    onClose={this.handleSecondaryLinkEditorClose}
                    onRemove={this.handleSecondaryLinkRemove}
                />
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        pageList: pageBuilder.pageList,
        footerMenu: pageBuilder.footerMenu,
        footerSocialLinks: pageBuilder.footerSocialLinks,
        footerSecondaryLinks: pageBuilder.footerSecondaryLinks
    };
}

export default connect(mapStateToProps, { ...applicationActions, ...pageBuilderActions })(FooterMenuSettings);