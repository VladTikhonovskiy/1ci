import React from "react";
import classnames from "classnames";
import { string } from "prop-types";
import { connect } from "react-redux";
import { IconButton, FlatButton, RaisedButton } from "material-ui";

import { BackspaceIcon, SettingsIcon, VisibilityIcon, CodeIcon, CloneItemPasteIcon, CloseIcon, UndoIcon } from "images/icons/svg-icons";

import classes from "./BlockListHeader.less";
import * as applicationActions from "modules/app/app.actions";
import * as pageBuilderActions from "modules/page-builder/page-builder.actions";
import * as pageBuilderSelectors from "modules/page-builder/page-builder.selectors";


const defaultPageName = "Default page";

class BlockListHeader extends React.Component {
    static propTypes = {
        className: string
    }

    handleShowNavigationButtonClick = event => {
        event.stopPropagation();

        this.props.showNavigation();
    }

    handlePageSettingsButtonClick = () => {
        let { pageSettingsShown, showPageSettings, hidePageSettings } = this.props;

        if (pageSettingsShown) {
            hidePageSettings()
        } else {
            showPageSettings();
        }
    }

    handlePagePreviewButtonClick = () => {
        this.props.showPagePreview();
    }

    handlePublishPageButtonClick = () => {
        this.props.publishSelectedPage();
    }

    handleShowPageCodeButtonClick = () => {
        this.props.showPageCodeDialog();
    }

    handleButtonCloneInsertClick = () => {
        this.props.cloneBlockInsertInPage();
        this.props.updatePageBlocks();
    }

    handleButtonCloneDisabledClick = () => {
        this.props.cloneSelectedBlockInfoClean();
    }

    handleUndoButtonClick = () => {
        this.props.undo();
    }

    get classes() {
        return classnames(classes.blockListHeader, this.props.className);
    }

    get pageTitle() {
        let { selectedPage } = this.props;

        return selectedPage.version.name || defaultPageName;
    }

    renderActionButtons() {
        let actionButtonLabelStyle = {
            textTransform: "none",
            color: "rgba(0, 0, 0, 0.87)"
        };

        let icons = {
            settings: <SettingsIcon />,
            preview: <VisibilityIcon />,
            code: <CodeIcon />,
            paste: <CloneItemPasteIcon />
        };

        return (
            <div className={classes.blockListHeaderActionButtons}>
                {
                    this.props.cloneBlockInfo !== "" &&
                    <div className={classes.blockListHeaderManipulationButtonWrapper}>
                        <button onClick={this.handleButtonCloneDisabledClick} className={classes.blockListHeaderManipulationButton} >
                            <CloseIcon />
                        </button>
                        <FlatButton
                            icon={icons.paste}
                            label="Paste  cloned block"
                            labelStyle={actionButtonLabelStyle}
                            className={classes.blockListHeaderActionButton}
                            onClick={this.handleButtonCloneInsertClick}
                        />
                    </div>
                }

                <FlatButton
                    icon={icons.code}
                    label="Watch code"
                    labelStyle={actionButtonLabelStyle}
                    className={classes.blockListHeaderActionButton}
                    onClick={this.handleShowPageCodeButtonClick}
                />
                <FlatButton
                    icon={icons.settings}
                    label="Page settings"
                    labelStyle={actionButtonLabelStyle}
                    className={classes.blockListHeaderActionButton}
                    onClick={this.handlePageSettingsButtonClick}
                />

                <FlatButton
                    icon={icons.preview}
                    label="Page preview"
                    labelStyle={actionButtonLabelStyle}
                    className={classes.blockListHeaderActionButton}
                    onClick={this.handlePagePreviewButtonClick}
                />

                <RaisedButton
                    label="Publish"
                    primary={true}
                    className={classes.blockListHeaderPublishButton}
                    onClick={this.handlePublishPageButtonClick}
                />
            </div>
        );
    }

    render() {
        let { lastUndoableAction } = this.props;

        let actionButtons = this.renderActionButtons();

        return (
            <div className={this.classes}>
                <IconButton
                    className={classes.blockListHeaderShowNavigationButton}
                    onClick={this.handleShowNavigationButtonClick}>

                    <BackspaceIcon />
                </IconButton>

                <h2 className={classes.blockListHeaderPageName}>
                    { this.pageTitle }
                </h2>

                {
                    lastUndoableAction &&
                    <IconButton className={classes.blockListHeaderUndoButton} onClick={this.handleUndoButtonClick}>
                        <UndoIcon />
                    </IconButton>
                }

                { actionButtons }
            </div>
        );
    }
}


function mapStateToProps({ app, pageBuilder }) {
    return {
        lastUndoableAction: app.lastUndoableAction,
        pageSettingsShown: pageBuilder.pageSettingsShown,
        cloneBlockInfo: pageBuilder.cloneBlockInfo.selectedBlockID,

        selectedPage: pageBuilderSelectors.selectedPage(pageBuilder),
    };
}

export default connect(mapStateToProps, { ...applicationActions, ...pageBuilderActions })(BlockListHeader);