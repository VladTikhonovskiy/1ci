import React from "react";
import classnames from "classnames";
import { string } from "prop-types";
import { connect } from "react-redux";
import { IconButton } from "material-ui";

import { BackspaceIcon } from "images/icons/svg-icons";

import classes from "./PageTemplateHeader.less";
import * as applicationActions from "modules/app/app.actions";


class PageTemplateHeader extends React.PureComponent {
    static propTypes = {
        className: string
    }

    handleShowNavigationButtonClick = event => {
        event.stopPropagation();

        this.props.showNavigation();
    }

    get classes() {
        return classnames(classes.pageTemplateHeader, this.props.className);
    }

    render() {
        return (
            <div className={this.classes}>
                <IconButton
                    className={classes.pageTemplateHeaderShowNavigationButton}
                    onClick={this.handleShowNavigationButtonClick}>
                    
                    <BackspaceIcon />
                </IconButton>

                <h2 className={classes.pageTemplateHeaderTitle}>
                    Page template
                </h2>
            </div>
        );
    }
}


export default connect(null, { ...applicationActions })(PageTemplateHeader);