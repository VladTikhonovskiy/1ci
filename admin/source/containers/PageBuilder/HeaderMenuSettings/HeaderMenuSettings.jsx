import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import { bool, string, func } from "prop-types";
import { Scrollbars } from "react-custom-scrollbars";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import { Drawer, IconButton } from "material-ui";

import { Icon } from "components/common";
import { CloseIcon, CheckIcon } from "images/icons/svg-icons";
import { MenuListItem, HeaderMenuItemEditor } from "components/page-builder";

import classes from "./HeaderMenuSettings.less";
import * as pageBuilderActions from "modules/page-builder/page-builder.actions";


class HeaderMenuSettings extends React.Component {
    static propTypes = {
        open: bool,
        className: string,
        onClose: func,
        onApply: func
    }

    static defaultProps ={
        open: false
    }

    state = {
        editableMenuItem: null,
        editMenuItemMode: false
    }

    handleCloseButtonClick = () => {
        this.props.onClose && this.props.onClose();
    }

    handleApplyButtonClick = () => {
        this.props.onApply && this.props.onApply();
    }

    handleAddMenuItemButtonClick = () => {
        this.setState({
            editMenuItemMode: true
        });
    }

    handleMenuListItemClick = menuItem => () => {
        this.setState({
            editableMenuItem: menuItem,
            editMenuItemMode: true
        });
    }

    handleMenuItemAddEdit = menuItem => {
        let { addHeaderMenuItem, editHeaderMenuItem } = this.props;

        this.state.editableMenuItem ? 
            editHeaderMenuItem(menuItem) :
            addHeaderMenuItem(menuItem);

        this.setState({
            editMenuItemMode: false
        });
    }

    handleMenuItemRemove = () => {
        this.props.removeHeaderMenuItem(this.state.editableMenuItem);

        this.setState({
            editableMenuItem: null,
            editMenuItemMode: false
        });
    }

    handleMenuItemEditorClose = () => {
        this.setState({
            editableMenuItem: null,
            editMenuItemMode: false
        });
    }

    handleMenuItemSortEnd = info => {
        this.props.moveHeaderMenuItem(info);
    }

    get classes() {
        return classnames(classes.headerMenuSettings, this.props.className);
    }

    renderSectionTitle(title) {
        return (
            <label className={classes.headerMenuSettingsSectionTitle}>
                { title }
            </label>
        );
    }

    renderSectionAddButton(label, handler) {
        return (
            <button className={classes.headerMenuSettingsAddItemButton} onClick={handler}>
                <Icon name="plus" className={classes.headerMenuSettingsAddItemButtonIcon} />
                { label }
            </button>
        );
    }

    renderMenuListItems() {
        return this.props.headerMenu.map(menuItem => (
            <MenuListItem
                key={menuItem.id}
                item={menuItem}
                className={classes.headerMenuSettingsMenuListItem}
                onClick={this.handleMenuListItemClick(menuItem)}
            />
        ));
    }

    renderHeader() {
        return (
            <div className={classes.headerMenuSettingsHeader}>
                <IconButton onClick={this.handleCloseButtonClick}>
                    <CloseIcon />
                </IconButton>

                <h2 className={classes.headerMenuSettingsTitle}>
                    Header navigation edit
                </h2>

                <IconButton onClick={this.handleApplyButtonClick}>
                    <CheckIcon />
                </IconButton>
            </div>
        );
    }

    renderContent() {
        let SortableItem = SortableElement(item => item.value);

        let SortableList = SortableContainer(({ items }) => {
            return (
                <div>
                    {
                        items.map((item, index) => (
                            <SortableItem key={`item-${index}`} index={index} value={item} />
                        ))
                    }
                </div>
            );
        });

        return (
            <div className={classes.footerMenuSettingsContent}>
                <label className={classes.headerMenuSettingsSectionTitle}>
                    Menu items
                </label>

                <button className={classes.headerMenuSettingsAddItemButton} onClick={this.handleAddMenuItemButtonClick}>
                    <Icon name="plus" className={classes.headerMenuSettingsAddItemButtonIcon} />
                    Add menu item
                </button>

                <div className={classes.headerMenuSettingsItemList}>
                    <SortableList
                        items={this.renderMenuListItems()}
                        useDragHandle={true}
                        helperClass={classes.headerMenuSettingsSortableHelper}
                        onSortEnd={this.handleMenuItemSortEnd}
                    />
                </div>
            </div>
        );
    }

    renderHeaderMenuItemEditor() {
        let { pageList } = this.props;
        let { editableMenuItem, editMenuItemMode } = this.state;

        return (
            <HeaderMenuItemEditor
                open={editMenuItemMode}
                pages={pageList}
                menuItem={editableMenuItem}
                className={classes.headerMenuSettingsItemEditor}
                onApply={this.handleMenuItemAddEdit}
                onClose={this.handleMenuItemEditorClose}
                onRemove={this.handleMenuItemRemove}
            />
        );
    }

    shouldComponentUpdate(nextProps) {
        if (this.props.open === false && nextProps.open === false)
            return false;

        return true;
    }

    render() {
        return (
            <div className={this.classes}>
                <Drawer width={460} containerClassName={classes.headerMenuSettingsContainer} open={this.props.open}>
                    { this.renderHeader() }

                    <Scrollbars autoHide>
                        { this.renderContent() }
                    </Scrollbars>
                </Drawer>

                { this.renderHeaderMenuItemEditor() }
            </div>
        );
    }
}


function mapStateToProps({ pageBuilder }) {
    return {
        pageList: pageBuilder.pageList,
        headerMenu: pageBuilder.headerMenu
    };
}

export default connect(mapStateToProps, { ...pageBuilderActions })(HeaderMenuSettings);