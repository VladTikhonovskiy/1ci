export default [
    {
        name: "Breadcrumbs",
        settings: {
            path: "Home/Applications/AccountingSuite"
        }
    },
    {
        name: "Hero1",
        settings: {
            title: {
                tag: "h2",
                text: "AccountingSuite",
                visible: true
            },
            description: {
                text: "All-in-one cloud accounting to boost the practice of accounting professionals and empower SMBs with an enterprise-level solution."
            }
        }
    },
    {
        name: "CountryDivider"
    },
    {
        name: "Partners1",
        settings: {
            title: {
                tag: "h2",
                text: "Professional Accountants that trust AccoutingSuite",
                visible: true
            },
            banners: [
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } }
            ]
        }
    },
    {
        name: "ReadyToJoin",
        settings: {
            button: {
                link: "/preview",
                label: "Free Sign Up"
            }
        }
    }
];