export default [
    {
        name: "Breadcrumbs",
        settings: {
            path: "Home/Developers"
        }
    },
    {
        name: "Benefits2",
        settings: {
            banners: [
                {
                    image: { url: "" },
                    caption: "Fast development",
                    description: {
                        text: "Times faster than you are typically used to."
                    }
                },
                {
                    image: { url: "" },
                    caption: "Cross-platform",
                    description: {
                        text: "Create Desktop, Cloud and Mobile apps."
                    }
                },
                {
                    image: { url: "" },
                    caption: "Apps created",
                    description: {
                        text: "Apps created on 1C:Enterprise easily customizable."
                    }
                }
            ]
        }
    },
    {
        name: "Statistics1"
    },
    {
        name: "Cards1"
    },
    {
        name: "GetStarted"
    }
];