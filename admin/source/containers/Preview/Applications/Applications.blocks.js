export default [
    {
        name: "Hero1",
        settings: {
            title: {
                tag: "h2",
                text: "Skyrocket Your Business",
                visible: true
            },
            description: {
                text: "Customized ERP solutions for companies of all sizes and types of business on the higly flexible, developer-friendly business automation platform."
            }
        }
    },
    {
        name: "SimpleSeparator",
        settings: {
            size: 24
        }
    },
    {
        name: "Title1",
        settings: {
            title: {
                tag: "h2",
                text: "1Ci Core Applications",
                visible: true
            }
        }
    },
    {
        name: "Cards3",
        settings: {
            cards: [
                {
                    icon: {
                        url: ""
                    },
                    caption: "1C:ERP 2",
                    description: {
                        text: "Innovative solution for building corporate management systems based on the best practice in automating large and medium-size businesses."
                    },
                    button: {
                        link: "",
                        label: "Request a Demo"
                    },
                    link: {
                        url: "/preview/erp2",
                        label: "Learn more"
                    }
                },
                {
                    icon: {
                        url: ""
                    },
                    caption: "1C:Drive",
                    description: {
                        text: "Complete yet clear and straightforward solution for automating key workflows in SME, including Purchases, Sales, Production, Customer Service and more."
                    },
                    button: {
                        link: "",
                        label: "Request a Demo"
                    },
                    link: {
                        url: "/preview/drive",
                        label: "Learn more"
                    }
                },
                {
                    icon: {
                        url: ""
                    },
                    caption: "AccountingSuite",
                    description: {
                        text: "All-in-one cloud accounting addressing the needs of product-based businesses and easily customizable for existing workflows."
                    },
                    button: {
                        link: "",
                        label: "Request a Demo"
                    },
                    link: {
                        url: "/preview/accounting-suite",
                        label: "Learn more"
                    }
                }
            ]
        }
    },
    {
        name: "Title3",
        settings: {
            title: {
                tag: "h2",
                text: "Partner Products",
                visible: true
            },
            description: {
                text: "More than 300 000 developers all over the world have created more than 1000 of amazingapplications based on 1C:Enterprise platform. And XXX companies trust these products to run critical operations of their factories, warehouses, shops and service companies. Check 1Ci Marketplace to see which solution fits best to boost your company efficiency and drive exceptional growth for your business."
            }
        }
    },
    {
        name: "Cards3",
        settings: {
            cards: [
                {
                    icon: {
                        url: ""
                    },
                    caption: "1C:Contabilitate",
                    description: {
                        text: "Integrated software for accounting professionals and business managers that want transparence of their company performance."
                    },
                    button: {
                        link: "",
                        label: "Request a Demo"
                    },
                    link: {
                        url: "/preview/contabilitate",
                        label: "Learn more"
                    }
                }
            ]
        }
    },
    {
        name: "Hero4",
        settings: {
            title: {
                tag: "h2",
                text: "Software Developers",
                visible: true
            },
            description: {
                text: "If you are a VAR or developer, turn your business ideas into reality with 1Ci powerful development platform. Quickly deploy your industry-specific solutions, cloud apps, knowledge-based applications for projects of any type and scale: accounting, finance, manufacturing, inventory, sales, orders, HR and many more."
            },
            button: {
                label: "Learn more"
            },
            backgroundImage: {
                url: "https://cdn.techinasia.com/wp-content/uploads/2017/04/66096349_l.jpg"
            }
        }
    }
];