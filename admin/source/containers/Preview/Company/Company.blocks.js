export default [
    {
        name: "Breadcrumbs",
        settings: {
            path: "Home/Company"
        }
    },
    {
        name: "Hero3"
    },
    {
        name: "Title3",
        settings: {
            title: {
                tag: "h2",
                text: "",
                visible: false
            },
            description: {
                text: "1Ci (1C International) is a growing international division of a leading software developer in Eastern Europe that provides business digitization solutions to over 1 million companies and organizations in the area. Founded in February 2017, 1Ci has a mission to bring that technological expertise to the global market, expand the partner ecosystem, and facilitate digital transformation in businesses of all sizes."
            }
        }
    },
    {
        name: "SimpleSeparator",
        settings: {
            size: 32
        }
    },
    {
        name: "Title3",
        settings: {
            title: {
                tag: "h2",
                text: "Core Technology",
                visible: true
            },
            description: {
                text: "Underlying 1Ci solutions is a comprehensive 1C:Enterprise platform that includes a set of technologies and development tools for digitizing business processes."
            }
        }
    },
    {
        name: "Statistics1"
    }
];