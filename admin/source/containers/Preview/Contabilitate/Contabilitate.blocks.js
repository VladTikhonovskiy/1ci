export default [
    {
        name: "Breadcrumbs",
        settings: {
            path: "Home/Applications/1C:Contabilitate"
        }
    },
    {
        name: "Hero6",
        settings: {
            title: {
                tag: "h2",
                text: "1C:Contabilitate",
                visible: true
            },
            description: {
                text: "Integrated software for professional accountants and business managers that want to stay on top of their company performance."
            }
        }
    },
    {
        name: "CountryDivider",
        settings: {
            title: {
                tag: "h2",
                text: "Limited for use in Romania",
                visible: true
            },
            country: "Romania"
        }
    },
    {
        name: "Title1",
        settings: {
            title: {
                tag: "h2",
                text: "Why 1C:Contabilitate?",
                visible: true
            }
        }
    },
    {
        name: "Benefits1",
        settings: {
            banners: [
                {
                    image: { url: "" },
                    description: {
                        text: "Quick customization according to the specific needs of your business."
                    }
                },
                {
                    image: { url: "" },
                    description: {
                        text: "Trusted technical support - we are always here to help."
                    }
                },
                {
                    image: { url: "" },
                    description: {
                        text: "Seamless integration with other software used in your company."
                    }
                }
            ]
        }
    },
    {
        name: "SimpleDivider"
    },
    {
        name: "Partners1",
        settings: {
            title: {
                tag: "h2",
                text: "Success in all sizes",
                visible: true
            },
            banners: [
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } },
                { image: { url: "" } }
            ]
        }
    }
];