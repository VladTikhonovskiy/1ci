import manufacturing from "./manufacturing.png";
import transportation from "./transportation.png";
import smartEnterprises from "./smart-enterprises.png";
import textilesAndApparel from "./textiles-and-apparel.png";
import smallAndMediumBusiness from "./small-and-medium-business.png";
import accountingProfessionals from "./accounting-professionals.png";


export default {
    manufacturing,
    transportation,
    smartEnterprises,
    textilesAndApparel,
    smallAndMediumBusiness,
    accountingProfessionals
};