import images from "./images";


export default [
    {
        name: "Hero1",
        settings: {
            title: {
                tag: "h2",
                text: "Skyrocket Your Business",
                visible: true
            },
            description: {
                text: "Customized ERP solutions for companies of all sizes and types of business on the higly flexible, developer-friendly business automation platform."
            }
        }
    },
    {
        name: "SimpleSeparator",
        settings: {
            size: 40
        }
    },
    {
        name: "Title3",
        settings: {
            title: {
                tag: "h2",
                text: "Tools for teams, from startup to enterprise",
                visible: true
            },
            description: {
                text: "It was a humorously perilous business for both of us. For, before we proceed further, it must be said that the monkey-rope was fast at both ends."
            }
        }
    },
    {
        name: "Cards2",
        settings: {
            cards: [
                {
                    icon: {
                        url: images.smallAndMediumBusiness
                    },
                    caption: "Small and Medium Business",
                    description: {
                        text: "Add trasnparency to your business operations - identify bottlenecks and redundancies to keep full control."
                    },
                    link: {
                        url: "/preview/drive",
                        label: "Learn about 1C:Drive"
                    }
                },
                {
                    icon: {
                        url: images.accountingProfessionals
                    },
                    caption: "Accounting Professionals",
                    description: {
                        text: "Grow your professional value by mastering advanced accounting tools (e.g. inventory, project management, and financial reporting)."
                    },
                    link: {
                        url: "/preview/accounting-suite",
                        label: "Learn about Accounting Suite"
                    }
                },
                {
                    icon: {
                        url: images.smartEnterprises
                    },
                    caption: "Smart Enterprises",
                    description: {
                        text: "Improve the flow of data across your organization and adapt quickly to the dynamics of digital transformation."
                    },
                    link: {
                        url: "/preview/erp2",
                        label: "Learn about 1C:ERP 2"
                    }
                },
                {
                    icon: {
                        url: images.textilesAndApparel
                    },
                    caption: "Textiles and Apparel",
                    description: {
                        text: "Automate all critical steps of the textile manufacturing including raw material production, finance, accounting."
                    },
                    link: {
                        url: "",
                        label: "Learn more"
                    }
                },
                {
                    icon: {
                        url: images.transportation
                    },
                    caption: "Transportation",
                    description: {
                        text: "Feel a difference with transparent, streamlined delivery and supply processes."
                    },
                    link: {
                        url: "",
                        label: "Learn more"
                    }
                },
                {
                    icon: {
                        url: images.manufacturing
                    },
                    caption: "Manufacturing",
                    description: {
                        text: "Automate and optimize all stages of the production cycle, starting from inventory management to real-time monitoring, reporting, and analytics."
                    },
                    link: {
                        url: "",
                        label: "Learn more"
                    }
                }
            ]
        }
    },
    {
        name: "Hero4",
        settings: {
            title: {
                tag: "h2",
                text: "Software Developers",
                visible: true
            },
            description: {
                text: "If you are a VAR or developer, turn your business ideas into reality with 1Ci powerful development platform. Quickly deploy your industry-specific solutions, cloud apps, knowledge-based applications for projects of any type and scale: accounting, finance, manufacturing, inventory, sales, orders, HR and many more."
            },
            button: {
                label: "Learn more"
            },
            backgroundImage: {
                url: "https://cdn.techinasia.com/wp-content/uploads/2017/04/66096349_l.jpg"
            }
        }
    },
    {
        name: "Partners1",
        settings: {
            title: {
                tag: "h2",
                text: "With 1Ci you are in good company",
                visible: true
            },
            banners: [
                { image: {} },
                { image: {} },
                { image: {} },
                { image: {} },
                { image: {} },
                { image: {} }
            ]
        }
    }
];