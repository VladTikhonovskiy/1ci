export default [
    {
        name: "Breadcrumbs",
        settings: {
            path: "Home/Applications/1C:Drive"
        }
    },
    {
        name: "Hero1",
        settings: {
            title: {
                tag: "h2",
                text: "1C:Drive",
                visible: true
            },
            description: {
                text: "Complete solution to streamline and manage key processes in SMB, bringing transparency to operations and revealing new opportunities for business growth."
            }
        }
    },
    {
        name: "Benefits2",
        settings: {
            banners: [
                {
                    image: {
                        url: ""
                    },
                    caption: "Take informed decisions",
                    description: {
                        text: "Monitor and retrieve data on your company performance for taking business decisions."
                    }
                },
                {
                    image: {
                        url: ""
                    },
                    caption: "Forget about tons of spreadsheets",
                    description: {
                        text: "Consolidate the detailed customer, order, stock and sales information in one straightforward system."
                    }
                },
                {
                    image: {
                        url: ""
                    },
                    caption: "Adapt to your requirements",
                    description: {
                        text: "Easily customize the software instead of adapting your unique processes to it."
                    }
                }
            ]
        }
    },
    {
        name: "Title1",
        settings: {
            title: {
                tag: "h2",
                text: "Understand how successful your business",
                visible: true
            }
        }
    }
];