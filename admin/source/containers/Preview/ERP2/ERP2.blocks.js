export default [
    {
        name: "Breadcrumbs",
        settings: {
            path: "Home/Applications/1C:ERP 2"
        }
    },
    {
        name: "Hero1",
        settings: {
            title: {
                tag: "h2",
                text: "1C:ERP 2",
                visible: true
            },
            description: {
                text: "Advanced ERP solution for midsize and large businesses that connects individual subsidiaries into one integrated system for seamless information flow and greater efficiencies."
            }
        }
    },
    {
        name: "Title1",
        settings: {
            title: {
                tag: "h2",
                text: "Understand how successful your business",
                visible: true
            }
        }
    },
    {
        name: "SimpleDivider"
    },
    {
        name: "Partners1",
        settings: {
            title: {
                tag: "h2",
                text: "Professional Accountants that trust 1C:Drive",
                visible: true
            }
        }
    }
];