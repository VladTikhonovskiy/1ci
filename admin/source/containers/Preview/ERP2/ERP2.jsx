import React from "react";

import blocks from "components/blocks";
import pageBlocks from "./ERP2.blocks";


export default class ERP2 extends React.PureComponent {
    render() {
        return (
            <React.Fragment>
                {
                    pageBlocks.map((block, index) => {
                        let BlockTagName = blocks[block.name];

                        return <BlockTagName key={index} {...block.settings}/>;
                    })
                }
            </React.Fragment>
        );
    }
}