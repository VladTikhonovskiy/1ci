import React from "react";
import { Route, Switch } from "react-router-dom";

import { Header, BottomLine } from "components/preview";
import Home from "./Home/Home";
import ERP2 from "./ERP2/ERP2";
import Drive from "./Drive/Drive";
import Company from "./Company/Company";
import Developers from "./Developers/Developers";
import Applications from "./Applications/Applications";
import Contabilitate from "./Contabilitate/Contabilitate";
import AccountingSuite from "./AccountingSuite/AccountingSuite";

import routes from "constants/preview-routes";
import classes from "./Preview.less";


export default class Preview extends React.PureComponent {
    render() {
        return (
            <div className={classes.preview}>
                <Header />

                <Switch>
                    <Route path={routes.accountingSuite} component={AccountingSuite} />
                    <Route path={routes.applications} component={Applications} />
                    <Route path={routes.developers} component={Developers} />
                    <Route path={routes.contabilitate} component={Contabilitate} />
                    <Route path={routes.drive} component={Drive} />
                    <Route path={routes.erp2} component={ERP2} />
                    <Route path={routes.company} component={Company} />
                    <Route path={routes.home} component={Home} />
                </Switch>

                <BottomLine />
            </div>
        );
    }
}