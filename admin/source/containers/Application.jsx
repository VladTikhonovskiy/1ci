import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

import { Snackbar } from "material-ui";

import Partners from "./Partners/Partners";
import PageBuilder from "./PageBuilder/PageBuilder.jsx";
import SystemUsers from "./SystemUsers/SystemUsers";
import WebsiteUsers from "./WebsiteUsers/WebsiteUsers.jsx";
import Localization from "./Localization/Localization.jsx";
import Navigation from "components/app/Navigation/Navigation.jsx";

import classes from "./Application.less";
import routes from "constants/routes";
import * as appActions from "modules/app/app.actions";
import * as authActions from "modules/auth/auth.actions";


class Application extends React.Component {
    handleMenuItemClick = () => {
        this.props.hideNavigation();
    }

    handleLogoutButtonClick = () => {
        this.props.logout();
    }

    componentDidMount() {
        if (this.shouldShowNavigation(this.props)) {
            this.props.showNavigation();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.shouldShowNavigation(nextProps)) {
            this.props.showNavigation();
        }
    }

    shouldShowNavigation(props) {
        return props.location.pathname === "/";
    }

    renderMessage() {
        let { messageText, messageShown, hideMessage } = this.props;

        return (
            <Snackbar
                open={messageShown}
                message={messageText}
                autoHideDuration={5000}
                onRequestClose={hideMessage}
            />
        );
    }

    render() {
        let { user, location, navigationShown } = this.props;

        if (user === null) {
            return <Redirect to={routes.auth} />;
        }

        return (
            <div className={classes.application}>
                <Route path={routes.partners} component={Partners} />
                <Route path={routes.pageBuilder} component={PageBuilder} />
                <Route path={routes.systemUsers} component={SystemUsers} />
                <Route path={routes.websiteUsers} component={WebsiteUsers} />
                <Route path={routes.localization} component={Localization} />

                <Navigation
                    open={navigationShown}
                    location={location.pathname}
                    className={classes.applicationNavigation}
                    onMenuItemClick={this.handleMenuItemClick}
                    onLogoutButtonClick={this.handleLogoutButtonClick}
                />

                { this.renderMessage() }
            </div>
        );
    }
}


function mapStateToProps({ app, auth }) {
    return {
        user: auth.user,
        messageText: app.messageText,
        messageShown: app.messageShown,
        navigationShown: app.navigationShown
    };
}

export default connect(mapStateToProps, { ...appActions, ...authActions })(Application);
