import React from "react";
import validator from "validator";
import classnames from "classnames";
import { string } from "prop-types";
import { connect } from "react-redux";
import { TextField, RaisedButton } from "material-ui";

import { Image } from "components/common";

import classes from "./AuthForm.less";
import * as actions from "modules/auth/auth.actions";
import * as selectors from "modules/auth/auth.selectors";


class AuthForm extends React.Component {
    static propTypes = {
        className: string
    }

    state = {
        email: "",
        password: "",

        emailErrorMessage: "",
        passwordErrorMessage: ""
    }

    shouldShowErrorMessages = false

    handleFormSubmit = e => {
        e.preventDefault();

        if (this.isFormValid === false) {
            this.shouldShowErrorMessages = true;
            this.setValidationState();
            return;
        }

        if (this.props.isLoginPending)
            return;

        let { email, password } = this.state;
        this.props.login(email, password);
    }

    handleEmailChange = e => {
        let email = e.target.value.trim();

        let { isValid, errorMessage } = this.validateEmail(email);
            
        this.setState({
            email,
            emailErrorMessage: isValid ? "" : errorMessage
        });
    }

    handlePasswordChange = e => {
        let password = e.target.value;

        let { isValid, errorMessage } = this.validatePassword(password);

        this.setState({
            password,
            passwordErrorMessage: isValid ? "" : errorMessage
        });
    }

    setValidationState() {
        let { errorMessage: emailErrorMessage } = this.validateEmail(this.state.email);
        let { errorMessage: passwordErrorMessage } = this.validatePassword(this.state.password);

        this.setState({
            emailErrorMessage,
            passwordErrorMessage
        });
    }

    validateEmail(value) {
        if (value === "") {
            return { isValid: false, errorMessage: "Enter e-mail" };
        }

        if (validator.isEmail(value) === false) {
            return { isValid: false, errorMessage: "E-mail doesn't have a valid format" };
        }

        return { isValid: true, errorMessage: "" };
    }

    validatePassword(value) {
        if (value === "") {
            return { isValid: false, errorMessage: "Enter password" };
        }

        return { isValid: true, errorMessage: "" };
    }

    get classes() {
        return classnames(classes.authForm, this.props.className);
    }

    get isFormValid() {
        let { email, password } = this.state;
        let validationResults = [this.validateEmail(email), this.validatePassword(password)];

        return validationResults.every(validationResult => validationResult.isValid);
    }

    renderEmailField() {
        let props = {
            fullWidth: true,
            floatingLabelText: "User e-mail"
        };

        if (this.shouldShowErrorMessages) {
            props.errorText = this.state.emailErrorMessage;
        }

        return <TextField {...props} onChange={this.handleEmailChange} />;
    }

    renderPasswordField() {
        let props = {
            type: "password",
            fullWidth: true,
            floatingLabelText: "Password"
        };

        if (this.shouldShowErrorMessages) {
            props.errorText = this.state.passwordErrorMessage;
        }

        return <TextField {...props} onChange={this.handlePasswordChange} />;
    }

    render() {
        let emailField = this.renderEmailField();
        let passwordField = this.renderPasswordField();

        return (
            <form className={this.classes} onSubmit={this.handleFormSubmit}>
                <div className={classes.authFormHeader}>
                    <Image name="logo" className={classes.authFormLogo} />
                </div>

                <div className={classes.authFormContent}>
                    { emailField }
                    { passwordField }

                    <p className={classes.authFormErrorMessage}>
                        { this.props.errorMessage }
                    </p>

                    <RaisedButton
                        type="submit"
                        label="Sign in"
                        primary={true}
                        className={classes.authFormLoginButton}
                    />
                </div>
            </form>
        );
    }
}


function mapStateToProps({ auth }) {
    return {
        errorMessage: auth.errorMessage,
        isLoginPending: selectors.isLoginPending(auth)
    };
}

export default connect(mapStateToProps, { ...actions })(AuthForm);