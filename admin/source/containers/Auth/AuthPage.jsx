import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import AuthForm from "./AuthForm";

import classes from "./AuthPage.less"
import routes from "constants/routes";


class AuthPage extends React.Component {
    render() {
        if (this.props.user) {
            return <Redirect to={routes.app} />;
        }

        return (
            <div className={classes.authPage}>
                <AuthForm />
            </div>
        );
    }
}


function mapStateToProps({ auth }) {
    return {
        user: auth.user
    };
}

export default connect(mapStateToProps)(AuthPage);