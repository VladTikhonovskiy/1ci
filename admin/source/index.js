import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import RegisterServiceWorker from "./registerServiceWorker";

import "styles/index.less";
import "styles/blocks.less";
import store from "./modules/store";
import routes from "constants/routes";

import Application from "containers/Application";
import AuthPage from "./containers/Auth/AuthPage";
import Preview from "./containers/Preview/Preview";


ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider>
            <BrowserRouter>
                <Switch>
                    <Route path={routes.auth} component={AuthPage} />
                    <Route path={routes.preview} component={Preview} />
                    <Route path={routes.app} component={Application} />
                </Switch>
            </BrowserRouter>
        </MuiThemeProvider>
    </Provider>,
    
    document.getElementById("root")
);


RegisterServiceWorker();