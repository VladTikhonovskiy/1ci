const { api, pageBuilder } = require(`config/${process.env.NODE_ENV}.json`);


export {
    api,
    pageBuilder
};