import { api } from "config";
import { pick } from "lodash";
import { http } from "./http.api";
import { buildURL } from "./helpers.api";



export default class PageBuilderApi {
    get urls() {
        return api.urls.pageBuilder;
    }

    async fetchPage(pageID) {
        let url = buildURL(this.urls.fetchPage, {
            id: pageID
        });

        let response = await http.get(url);
        return response.data;
    }

    async fetchPageList() {
        let response = await http.get(this.urls.fetchPages);
        return response.data;
    }

    async fetchUrlList(urlName) {
        let response = await http.get(this.urls.getUrls, {
            params: {
                url: urlName
            }
        });

        return response.data;
    }

    async createPage(parentPageID) {
        let data = {
            name: "New page",
            parentId: parentPageID === -1 ? null : parentPageID
        };

        let response = await http.post(this.urls.createPage, data);
        return response.data;
    }

    removePage(pageID) {
        let url = buildURL(this.urls.removePage, {
            id: pageID
        });

        return http.delete(url);
    }

    updatePage(page) {
        let { version } = page;

        let url = buildURL(this.urls.updatePage, {
            id: page.id
        });

        let data = {
            ...pick(page, [
                "image",
                "noindex",
                "nofollow",
                "hideFromSitemap"
            ]),

            ...pick(version, [
                "url",
                "name",
                "metaTitle",
                "metaKeywords",
                "metaDescription",
                "socialTitle",
                "socialDescription"
            ])
        };

        return http.put(url, data);
    }

    updatePageBlocks(page) {
        let url = buildURL(this.urls.updatePage, {
            id: page.id
        });

        return http.put(url, {
            blocks: page.version.blocks
        });
    }

    publishPage(page) {
        let url = buildURL(this.urls.publishPage, {
            id: page.id
        });

        return http.put(url);
    }

    uploadImage(image) {
        let formData = new FormData();
        formData.append("file", image);

        return http.post(this.urls.uploadImage, formData)
            .then(response => response.data);
    }
}