import AuthApi from "./auth.api";
import PageBuilderApi from "./page-builder.api";


export default {
    auth: new AuthApi(),
    pageBuilder: new PageBuilderApi()
};