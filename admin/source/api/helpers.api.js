import { each } from "lodash";


export function buildURL(url, params) {
    let result = url;

    each(params, (value, name) => {
        result = result.replace(`:${name}`, value);
    }, url);

    return result;
}