import axios from "axios";
import { api } from "config";


export const http = axios.create({
    baseURL: api.baseURL,
    timeout: api.timeout,
    withCredentials: api.withCredentials
});