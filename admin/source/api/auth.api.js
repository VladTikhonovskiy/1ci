import { api } from "config";
import { http } from "./http.api";


export default class AuthApi {
    get urls() {
        return api.urls.auth;
    }

    login(login, password) {
        return http.post(this.urls.login, { login, password });
    }

    logout() {
        return http.post(this.urls.logout);
    }
}