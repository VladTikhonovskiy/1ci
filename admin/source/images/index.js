import logo from "./logo.inline.svg";
import logoWhite from "./logo-white.inline.svg";
import heroBackground from "./hero-background.inline.svg";
import Earth from "./earth.svg";
import testBackgroundImage from "./test-background-image.jpg";
import notFoundPage from "./not-found-page.jpg";
import bicycleImage from "./bicycle.svg";
import dogImage from "./dog.svg";


export {
    logo,
    logoWhite,
    heroBackground,
    Earth,
    testBackgroundImage,
    notFoundPage,
    bicycleImage,
    dogImage
};