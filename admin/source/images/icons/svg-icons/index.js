import EditIcon          from "material-ui/svg-icons/editor/mode-edit";
import PageIcon          from "material-ui/svg-icons/editor/insert-drive-file";
import CheckIcon         from "material-ui/svg-icons/navigation/check";
import CloseIcon         from "material-ui/svg-icons/content/clear";
import CloneIcon         from "material-ui/svg-icons/content/content-copy";
import DeleteIcon        from "material-ui/svg-icons/action/delete";
import LogoutIcon        from "material-ui/svg-icons/action/exit-to-app";
import SettingsIcon      from "material-ui/svg-icons/action/settings";
import ToggleIcon        from "material-ui/svg-icons/navigation/arrow-drop-up";
import PositionIcon      from "material-ui/svg-icons/action/view-module";
import HomePageIcon      from "material-ui/svg-icons/action/home";
import UndoIcon          from "material-ui/svg-icons/content/undo";
import RedoIcon          from "material-ui/svg-icons/content/redo";
import CreatePageIcon    from "material-ui/svg-icons/content/add";
import ArrowUpIcon       from "material-ui/svg-icons/hardware/keyboard-arrow-up";
import ArrowRightIcon    from "material-ui/svg-icons/hardware/keyboard-arrow-right";
import ArrowDownIcon     from "material-ui/svg-icons/hardware/keyboard-arrow-down";
import BackspaceIcon     from "material-ui/svg-icons/hardware/keyboard-backspace";
import VisibilityIcon    from "material-ui/svg-icons/action/visibility";
import VisibilityOffIcon from "material-ui/svg-icons/action/visibility-off";
import FileUploadIcon    from "material-ui/svg-icons/file/file-upload";
import DevicePhoneIcon   from "material-ui/svg-icons/hardware/phone-iphone";
import DeviceTabletIcon  from "material-ui/svg-icons/hardware/tablet";
import DeviceDesktopIcon from "material-ui/svg-icons/hardware/computer";
import PageTemplateIcon  from "material-ui/svg-icons/av/web";
import CodeIcon          from "material-ui/svg-icons/action/code";
import AlignLeftIcon     from "material-ui/svg-icons/editor/format-align-left";
import AlignRightIcon    from "material-ui/svg-icons/editor/format-align-right";
import AlignCenterIcon   from "material-ui/svg-icons/editor/format-align-center";
import CloneItemOutsideIcon  from "material-ui/svg-icons/content/archive";
import CloneItemPasteIcon  from "material-ui/svg-icons/content/move-to-inbox";


export {
    EditIcon,
    PageIcon,
    CheckIcon,
    CloseIcon,
    CloneIcon,
    DeleteIcon,
    LogoutIcon,
    SettingsIcon,
    ToggleIcon,
    PositionIcon,
    HomePageIcon,
    UndoIcon,
    RedoIcon,
    CreatePageIcon,
    ArrowUpIcon,
    ArrowDownIcon,
    ArrowRightIcon,
    BackspaceIcon,
    VisibilityIcon,
    VisibilityOffIcon,
    FileUploadIcon,
    DevicePhoneIcon,
    DeviceTabletIcon,
    DeviceDesktopIcon,
    PageTemplateIcon,
    CodeIcon,
    AlignLeftIcon,
    AlignRightIcon,
    AlignCenterIcon,
    CloneItemOutsideIcon,
    CloneItemPasteIcon
};