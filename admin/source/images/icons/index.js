import link from "./link.inline.svg";
import plus from "./plus.inline.svg";
import drag from "./drag.inline.svg";
import arrowDown from "./arrow-down.inline.svg";
import arrowRight from "./arrow-right.inline.svg";
import arrowLeft from "./arrow-left.inline.svg";
import filePdf from "./file--pdf.inline.svg";
import point from "./point.png";
import quotes from "./quotes.png";
import bubble from "./bubble.png";


export {
    link,
    plus,
    drag,
    arrowDown,
    arrowRight,
    arrowLeft,
    filePdf,
    point,
    quotes,
    bubble
};