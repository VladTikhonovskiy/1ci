import twitter from "./twitter.svg";
import youtube from "./youtube.svg";
import linkedin from "./linkedin.svg";
import facebook from "./facebook.svg";


export {
    twitter,
    youtube,
    linkedin,
    facebook
};