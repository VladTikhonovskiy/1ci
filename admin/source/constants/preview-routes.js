export default {
    home: "/preview",
    erp2: "/preview/erp2",
    drive: "/preview/drive",
    company: "/preview/company",
    developers: "/preview/developers",
    applications: "/preview/applications",
    contabilitate: "/preview/contabilitate",
    accountingSuite: "/preview/accounting-suite"
}