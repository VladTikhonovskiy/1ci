export default {
    app: "/",
    auth: "/auth",
    preview: "/preview",
    pageBuilder: "/page-builder",
    partners: "/partners",
    systemUsers: "/system-users",
    websiteUsers: "/website-users",
    localization: "/localization"
}