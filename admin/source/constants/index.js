import routes from "./routes";
import previewRoutes from "./preview-routes";


export {
    routes,
    previewRoutes
};