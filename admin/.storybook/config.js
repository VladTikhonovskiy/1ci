import { configure } from "@storybook/react";


configure(loadStories, module);


function loadStories() {
    require("../stories/controls/link.js");
    require("../stories/controls/button.js");
    require("../stories/controls/circle-button.js");
    require("../stories/controls/textbox.js");
    require("../stories/controls/checkbox.js");
    require("../stories/blocks/products/enterprise.js");
    require("../stories/blocks/products/products1.js");
    require("../stories/blocks/products/products2.js");
    require("../stories/blocks/features/features-1.js");
    require("../stories/blocks/features/big-icons-1.js");
    require("../stories/blocks/features/big-icons-2.js");
    require("../stories/blocks/features/big-icons-3.js");
    require("../stories/blocks/features/image-list.js");
    require("../stories/blocks/features/image-list-2.js");
    require("../stories/blocks/features/list-image.js");
    require("../stories/blocks/reviews/reviews-2.js");
}