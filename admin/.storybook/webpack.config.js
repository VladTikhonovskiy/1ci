const path = require("path");
const paths = require("../config/paths");
const autoprefixer = require("autoprefixer");

let resolvePath = path.resolve(__dirname, "../");


module.exports = {
    module: {
        rules: [
            {
                oneOf: [
                    {
                        test: /\.inline.svg$/,
                        loader: require.resolve('svg-inline-loader'),
                        include: resolvePath
                    },
                    {
                        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
                        loader: require.resolve('url-loader'),
                        options: {
                            limit: 10000,
                            name: 'static/media/[name].[hash:8].[ext]',
                        },
                        include: resolvePath
                    },
                    {
                        test: /\.(js|jsx|mjs)$/,
                        include: paths.appSrc,
                        loader: require.resolve('babel-loader'),
                        options: {
                            cacheDirectory: true,
                        }
                    },
                    {
                        test: /\.less$/,
                        include: resolvePath,
                        use: [
                            require.resolve('style-loader'),
                            {
                                loader: require.resolve('css-loader'),
                                options: {
                                    modules: true,
                                    camelCase: 'only',
                                    importLoaders: 2,
                                    localIdentName: '[local]--[hash:base64:5]'
                                },
                            },
                            {
                                loader: require.resolve('postcss-loader'),
                                options: {
                                    ident: 'postcss',
                                    plugins: () => [
                                        require('postcss-flexbugs-fixes'),
                                        autoprefixer({
                                            browsers: [
                                                '>1%',
                                                'last 4 versions',
                                                'Firefox ESR',
                                                'not ie < 9', // React doesn't support IE8 anyway
                                            ],
                                            flexbox: 'no-2009',
                                        }),
                                    ],
                                },
                            },
                            {
                                loader: require.resolve('less-loader')
                            }
                        ],
                    }
                ]
            }
        ]
    }
}