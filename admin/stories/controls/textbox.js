import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { Textbox } from "../../source/components/controls";


const events = {
    onChange: action("changed")
};


storiesOf("Textbox/Light", module)
    .addDecorator(story => {
        let style = {
            width: 500
        };

        return (
            <div style={style}>
                { story() }
            </div>
        );
    })
    .add("simple textbox", () => (
        <Textbox {...events} />
    ))
    .add("textbox with label", () => (
        <Textbox label="Textbox label" {...events} />
    ))
    .add("textbox with value", () => (
        <Textbox label="Textbox label" value="Textbox value" {...events} />
    ))
    .add("textbox with valid state", () => (
        <Textbox label="Textbox label" value="Textbox value" state="valid" {...events} />
    ))
    .add("textbox with placeholder ", () => (
      <Textbox label="Textbox label" placeholder="Placeholder Example" {...events} />
    ))
    .add("textbox with invalid state", () => (
        <Textbox label="Textbox label" value="Textbox value" state="invalid" {...events} />
    ))
    .add("textbox with error text", () => (
        <Textbox label="Textbox label" value="Textbox value" error="Textbox error text" state="invalid" {...events} />
    ))
    .add("disabled textbox", () => (
        <Textbox label="Textbox label" value="Textbox value" disabled={true} {...events} />
    ));


storiesOf("Textbox/Dark", module)
    .addDecorator(story => {
        let style = {
            width: 500,
            padding: "40px 30px",
            backgroundColor: "#158aff"
        };

        return (
            <div style={style}>
                { story() }
            </div>
        );
    })
    .add("simple textbox", () => (
        <Textbox theme="dark" {...events} />
    ))
    .add("textbox with label", () => (
        <Textbox theme="dark" label="Textbox label" {...events} />
    ))
    .add("textbox with value", () => (
        <Textbox theme="dark" label="Textbox label" value="Textbox value" {...events} />
    ))
    .add("textbox with valid state", () => (
        <Textbox theme="dark" label="Textbox label" value="Textbox value" state="valid" {...events} />
    ))
    .add("textbox with placeholder ", () => (
      <Textbox theme="dark" label="Textbox label" placeholder="Placeholder Example" {...events} />
    ))
    .add("textbox with invalid state", () => (
        <Textbox theme="dark" label="Textbox label" value="Textbox value" state="invalid" {...events} />
    ))
    .add("textbox with error text", () => (
        <Textbox theme="dark" label="Textbox label" value="Textbox value" error="Textbox error text" state="invalid" {...events} />
    ))
    .add("disabled textbox", () => (
        <Textbox theme="dark" label="Textbox label" value="Textbox value" disabled={true} {...events} />
    ));