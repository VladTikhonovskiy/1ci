import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { Checkbox } from "../../source/components/controls";


const events = {
    onChange: action("changed")
};


storiesOf("Checkbox/Light", module)
    .addDecorator(story => {
        let style = {
          width: 200
        };

        return (
            <div style={style}>
                { story() }
            </div>
        );
    })
    .add("checkbox unchecked", () => (
        <Checkbox text="By submitting this form, I consent to the use of my personal information for the purposes described in the Privacy Notice." {...events} />
    ))
    .add("checbox checked", () => (
        <Checkbox isChecked={true} text="By submitting this form, I consent to the use of my personal information for the purposes described in the Privacy Notice." {...events} />
    ));


storiesOf("Checkbox/Dark", module)
    .addDecorator(story => {
        let style = {
            width: 1000,
            padding: "40px 30px",
            backgroundColor: "#158aff"
        };

        return (
            <div style={style}>
                { story() }
            </div>
        );
    })
    .add("checkbox unchecked", () => (
        <Checkbox theme="dark"  text="By submitting this form, I consent to the use of my personal information for the purposes described in the Privacy Notice." {...events} />
    ))
    .add("checkbox checked", () => (
        <Checkbox theme="dark" isChecked={true} text="By submitting this form, I consent to the use of my personal information for the purposes described in the Privacy Notice."{...events} />
    ));