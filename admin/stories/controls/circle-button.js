import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { CircleButton } from "../../source/components/controls";

import * as icons from "../../source/images/icons";


const events = {
    onClick: action("clicked")
};

storiesOf("Circle button", module)
    .add("button with icon", () => (
        <CircleButton icon={icons.arrowRight} {...events} />
    ))
    .add("button without icon", () => (
        <CircleButton {...events} />
    ));
    