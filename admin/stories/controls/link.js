import React from "react";
import { storiesOf } from "@storybook/react";
import { Link } from "../../source/components/controls";


storiesOf("Link", module)
    .add("simple link", () => (
        <Link url="http://example.com" label="Simple link" />
    ))
    .add("link with arrow", () => (
        <Link url="http://example.com" label="Link with arrow" showArrow={true} />
    ));