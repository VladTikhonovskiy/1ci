import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { Button } from "../../source/components/controls";


const events = {
    onClick: action("clicked")
};

const simpleButton = type => () => (
    <Button type={type} label="Simple button" {...events} />
);

const disabledButton = type => () => (
    <Button type={type} label="Disabled button" disabled={true} {...events} />
);

const buttonAsLink = type => () => (
    <Button type={type} label="Link button" link="http://example.com" {...events} />
);

const buttonAsLinkDisabled = type => () => (
    <Button type={type} label="Link button disabled" link="http://example.com" disabled={true} {...events} />
);


storiesOf("Button/Primary", module)
    .add("simple button", simpleButton("primary"))
    .add("disabled button", disabledButton("primary"))
    .add("button as link", buttonAsLink("primary"))
    .add("button as link disabled", buttonAsLinkDisabled("primary"));


storiesOf("Button/Secondary", module)
    .add("simple button", simpleButton("secondary"))
    .add("disabled button", disabledButton("secondary"))
    .add("button as link", buttonAsLink("secondary"))
    .add("button as link disabled", buttonAsLinkDisabled("secondary"));


storiesOf("Button/Alternate", module)
    .addDecorator(story => {
        let style = {
            position: "fixed",
            width: "100%",
            height: "100%",
            padding: "20px",
            backgroundColor: "#6c798f"
        };

        return (
            <div style={style}>
                { story() }
            </div>
        );
    })
    .add("simple button", simpleButton("alternate"))
    .add("disabled button", disabledButton("alternate"))
    .add("button as link", buttonAsLink("alternate"))
    .add("button as link disabled", buttonAsLinkDisabled("alternate"));