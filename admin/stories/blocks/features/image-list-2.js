import React from "react";
import { storiesOf } from "@storybook/react";
import ImageList2 from "../../../source/components/blocks/features/ImageList2/ImageList2";

storiesOf("Blocks/Features/ImageList2", module)
    .add("with default parameters", () => (
        <ImageList2 />
    ));