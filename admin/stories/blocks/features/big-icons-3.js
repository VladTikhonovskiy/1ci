import React from "react";
import { storiesOf } from "@storybook/react";
import BigIcons3 from "../../../source/components/blocks/features/BigIcons3/BigIcons3";

storiesOf("Blocks/Features/BigIcons3", module)
    .add("with default parameters", () => (
        <BigIcons3 />
    ));