import React from "react";
import { storiesOf } from "@storybook/react";
import ListImage from "../../../source/components/blocks/features/ListImage/ListImage";

storiesOf("Blocks/Features/ListImage", module)
    .add("with default parameters", () => (
        <ListImage />
    ));