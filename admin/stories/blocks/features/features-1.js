import React from "react";
import { storiesOf } from "@storybook/react";
import Features1 from "../../../source/components/blocks/features/Features1/Features1";

storiesOf("Blocks/Features/Features1", module)
    .add("with default parameters", () => (
        <Features1 />
    ));