import React from "react";
import { storiesOf } from "@storybook/react";
import ImageList from "../../../source/components/blocks/features/ImageList/ImageList";

storiesOf("Blocks/Features/ImageList", module)
    .add("with default parameters", () => (
        <ImageList />
    ));