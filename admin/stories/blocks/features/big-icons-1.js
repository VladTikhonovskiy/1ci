import React from "react";
import { storiesOf } from "@storybook/react";
import BigIcons1 from "../../../source/components/blocks/features/BigIcons1/BigIcons1";

storiesOf("Blocks/Features/BigIcons1", module)
    .add("with default parameters", () => (
        <BigIcons1 />
    ));