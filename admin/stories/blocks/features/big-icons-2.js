import React from "react";
import { storiesOf } from "@storybook/react";
import BigIcons2 from "../../../source/components/blocks/features/BigIcons2/BigIcons2";

storiesOf("Blocks/Features/BigIcons2", module)
    .add("with default parameters", () => (
        <BigIcons2 />
    ));