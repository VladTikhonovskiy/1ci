import React from "react";
import { storiesOf } from "@storybook/react";
import Products1 from "../../../source/components/blocks/products/Products1/Products1";

import { testBackgroundImage } from "../../../source/images";


storiesOf("Blocks/Products/Products1", module)
    .add("with default parameters", () => (
        <Products1 />
    ))
    .add("with custom title", () => {
        let title = {
            tag: "h3",
            text: "Custom title text",
            visible: true
        };

        return <Products1 title={title} />;
    })
    .add("with very long title", () => {
        let title = {
            tag: "h6",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            visible: true
        };

        return <Products1 title={title} />;
    })
    .add("without title", () => {
        let title = {
            tag: "h1",
            text: "Invisible title",
            visible: false
        };

        return <Products1 title={title} />;
    })
    .add("with custom description", () => {
        let description = {
            text: "Custom description text"
        };

        return <Products1 description={description} />
    })
    .add("with custom link text", () => {
        let link = {
            url: "http://example.com",
            label: "Custom link text"
        };

        return <Products1 link={link} />;
    })
    .add("with custom image", () => {
        let image = {
            url: testBackgroundImage
        };

        return <Products1 image={image} />;
    });