import React from "react";
import { storiesOf } from "@storybook/react";
import Products2 from "../../../source/components/blocks/products/Products2/Products2";

import { testBackgroundImage } from "../../../source/images";


storiesOf("Blocks/Products/Products2", module)
    .add("with default parameters", () => (
        <Products2 />
    ))
    .add("with custom title", () => {
        let title = {
            tag: "h4",
            text: "Custom title text",
            visible: true
        };

        return <Products2
            leftBannerTitle={title}
            rightBannerTitle={title}
        />;
    })
    .add("with very long title", () => {
        let title = {
            tag: "h6",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            visible: true
        };

        return <Products2
            leftBannerTitle={title}
            rightBannerTitle={title}
        />;
    })
    .add("without title", () => {
        let title = {
            tag: "h1",
            text: "Invisible title",
            visible: false
        };

        return <Products2
            leftBannerTitle={title}
            rightBannerTitle={title}
        />;
    })
    .add("with custom description", () => {
        let description = {
            text: "Custom description text"
        };

        return <Products2
            leftBannerDescription={description}
            rightBannerDescription={description}
        />;
    })
    .add("with custom link text", () => {
        let link = {
            url: "http://example.com",
            label: "Custom link text"
        };

        return <Products2
            leftBannerLink={link}
            rightBannerLink={link}
        />;
    })
    .add("with custom image", () => {
        let backgroundImage = {
            url: testBackgroundImage
        };

        return <Products2
            leftBannerBackgroundImage={backgroundImage}
            rightBannerBackgroundImage={backgroundImage}
        />;
    });