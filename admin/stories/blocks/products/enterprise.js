import React from "react";
import { storiesOf } from "@storybook/react";
import Enterprise from "../../../source/components/blocks/products/Enterprise/Enterprise";

import { testBackgroundImage } from "../../../source/images";


storiesOf("Blocks/Products/Enterprise", module)
    .add("with default parameters", () => (
        <Enterprise />
    ))
    .add("with custom title", () => {
        let title = {
            tag: "h3",
            text: "Custom title text",
            visible: true
        };

        return <Enterprise title={title} />;
    })
    .add("with very long title", () => {
        let title = {
            tag: "h6",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            visible: true
        };

        return <Enterprise title={title} />;
    })
    .add("without title", () => {
        let title = {
            tag: "h1",
            text: "Invisible title",
            visible: false
        };

        return <Enterprise title={title} />;
    })
    .add("with custom description", () => {
        let description = {
            text: "Custom description text"
        };

        return <Enterprise description={description} />
    })
    .add("with custom link text", () => {
        let link = {
            url: "http://example.com",
            label: "Custom link text"
        };

        return <Enterprise link={link} />;
    })
    .add("with custom background image", () => {
        let backgroundImage = {
            url: testBackgroundImage
        };

        return <Enterprise backgroundImage={backgroundImage} />;
    });